# -*- coding: utf-8 -*-
# 
# Copyright (C) 2014  Nicolas Grandjean <nicolas@microniko.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.shortcuts import render

# Create your views here.
from django.http import Http404
from django.shortcuts import render, get_object_or_404
from django.views.generic import ListView, DetailView


from models import Pages
from ekz.views import Alphabet

# Fonction qui liste les pages
# Calcul de dates
import datetime
def ListeDesPages():
    return Pages.objects.filter(date_debut__lte = datetime.datetime.now(), date_fin__gte = datetime.datetime.now())


# Afficher une page
class VoirPage(DetailView):
    """ Vue d'affichage d'une page en particulier. """
    model = Pages
    context_object_name = "page"
    template_name = "voir_page.html"
    def get_context_data(self, **kwargs):
        context = super(VoirPage, self).get_context_data(**kwargs)
        context['pages'] = ListeDesPages()
        context['alphabet'] = Alphabet()
        return context
