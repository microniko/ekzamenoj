# -*- coding: utf-8 -*-
# 
# Copyright (C) 2014  Nicolas Grandjean <nicolas@microniko.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.



from django.contrib import admin

# Register your models here.
from models import Pages, News
from django_summernote.admin import SummernoteModelAdmin


class PagesAdmin(SummernoteModelAdmin):
    """Administration des pages"""
    list_display = ('titre','titre_court','date_debut','date_fin')
    list_filter  = ()

class NewsAdmin(SummernoteModelAdmin):
    """Administration des news"""
    list_display = ('titre','date_debut', 'date_fin')
    list_filter  = ()

admin.site.register(Pages, PagesAdmin)
admin.site.register(News, NewsAdmin)

