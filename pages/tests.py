# -*- coding: utf-8 -*-
# 
# Copyright (C) 2014  Nicolas Grandjean <nicolas@microniko.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.test import TestCase

# Create your tests here.

from models import *

import datetime

class TestPages(TestCase):
    """ Classe de test pour les pages """

    def test_page_create(self):
        """ Test pour la création d'une page. """
        pg = Pages(titre=u"Titre long d'une page pour tester",
                   titre_court=u"Titre court",
                   contenu=u"Bla bla bla",
                   date_debut=datetime.datetime(2014, 2, 15),
                   date_fin=datetime.datetime(2014,2, 17)
                  )
        pg.save()
        # Vérification
        page = Pages.objects.get(titre_court=u"Titre court")
        self.assertEqual(page.titre,u"Titre long d'une page pour tester")
        date_delta = page.date_debut+datetime.timedelta(days=2)
        self.assertEqual(date_delta,page.date_fin)



class TestNews(TestCase):
    """ Classe de tests pour les nouvelles. """
    def test_news_create(self):
        """ Test pour la création d'une nouvelle """
        new = News(titre=u"Titre de la nouveauté",
                   contenu=u"Bla bla bla",
                   date_debut=datetime.datetime(2014, 2, 15),
                   date_fin=datetime.datetime(2014,2, 27)
                  )
        new.save()
        # Vérification
        n = News.objects.get(id=1)
        self.assertEqual(n.titre,u"Titre de la nouveauté")
        date_delta = n.date_debut+datetime.timedelta(days=12)
        self.assertEqual(date_delta,n.date_fin)
