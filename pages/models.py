# -*- coding: utf-8 -*-
# 
# Copyright (C) 2014  Nicolas Grandjean <nicolas@microniko.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from django.db import models

import datetime
from django.utils import timezone
# Create your models here.

def dans_un_an():
    """ Dans un an…"""
    return timezone.now() + timezone.timedelta(days=365)

def dans_deux_semaines():
    """ Dans deux semaines…"""
    return timezone.now() + timezone.timedelta(days=14)

class Pages(models.Model):
    """ Les pages permettent de référencer des informations statiques (c-à-d qui changent peu souvent).
    Un lien est automatiquement ajouté dans le menu principal lorsque la date courrante est située entre les dates
    de début de de fin."""
    titre = models.CharField(max_length=250, unique=True, null=False, verbose_name=u'Titre')
    titre_court = models.CharField(max_length=50, unique=True, null=False, verbose_name=u'Titre court (apparaît dans le menu)')
    contenu = models.TextField(verbose_name=u'Contenu')
    date_debut = models.DateTimeField(verbose_name=u"Afficher dans le menu à partir du", default=timezone.now)
    date_fin = models.DateTimeField(verbose_name=u"Afficher dans le menu jusqu'au",default=dans_un_an)
    def get_absolute_url(self):
        return "/page%i" % self.id
    def __str__ (self):
       return self.titre
    class Meta:
       verbose_name=u"Page"
       verbose_name_plural=u"Pages"

class News(models.Model):
     """ Les news s'affichent sur la page d'acceuil et permettent de proposer une information."""
     titre = models.CharField(max_length=250, unique=True, null=False, verbose_name=u'Titre')
     contenu = models.TextField(verbose_name=u'Contenu')
     ordre = models.IntegerField(verbose_name="Ordre d'affichage", default=0, null=False)
     date_debut = models.DateTimeField(verbose_name=u"Afficher à partir du", default=timezone.now)
     date_fin = models.DateTimeField(verbose_name=u"Afficher jusqu'au",default=dans_deux_semaines)
     def __str__ (self):
        return self.titre
     class Meta:
        verbose_name=u"Nouvelle"
        verbose_name_plural=u"Nouvelles"
        ordering = ['ordre', 'titre']
