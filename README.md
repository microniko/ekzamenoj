Ekzamenoj
=========

Qu'est-ce ?
-----------
Manuel libre des examens de bioogie médicale avec aide à l'optimisation des tubes pour le prélèvement.
Il est distribué avec l'espoir qu'il pourrait être utile (et amélioré) mais sans aucune garantie (cf. Licence - plus bas).
Vous pouvez regarder [une présentation](http://ekzamenoj.microniko.net).
Une version de démonstration est disponible à l'adresse <http://demo-ekzamenoj.microniko.net>.

Documentations
--------------
Une partie de la doc est dans le code :

* doc/HOWTO.md    → Alimenter la base manuellement.
* doc/INFOS.md    → Quelques infos peu utiles.
* doc/TODO.md     → Ce qu'il reste à faire dans le développement.
* doc/importation → Importer des données au format CSV (voir le README.md)
* doc/mrd.png     → Image du modèle relationnel des données.

Licence
-------
Ekzamenoj est un Logiciel Libre. Il est distribué sous licence AGPL3 (voir le fichier LICENCE).

Il est fournit tel quel, *sans aucune garantie*. L'auteur ne saurait être tenu responsable de l'utilisation qui en sera faite. Ainsi, vous utilisez Ekzamenoj à vos propres risques.
Vous êtes cordialement invité à proposer des améliorations. Les sources sont disponibles sur [FramaGit](https://git.framasoft.org/microniko/ekzamenoj) où vous trouverez le moyen de suggérer vos améliorations.

Le logo utilise le microscope sous licence CC By-Sa 3.0 dont l'auteur est Con-struct (https://commons.wikimedia.org/wiki/User:Con-struct). Lui-même est distribué sous la même licence.

Auteur
------
* N. Grandjean - 2014-2016

Tester
------
### Créer un environnement virtuel :

    mkvirtualenv --python=/usr/bin/python3 -a Documents/django/ekzamenoj ekzamenoj

### Travailler dedans :

    workon ekzamenoj

### Instaler ce qu'il faut :

    pip install -r requirements.txt

### Initialiser la base de données (répondez gentillement aux questions posées)

#### Vous pouvez alimenter la base de données avec des données par défaut du fichier doc/default_data.json :

    python manage.py migrate --noinput

puis

    python manage.py loaddata doc/default_data.json

Les utilisateurs par défaut sont :

- login : admin ; mot de passe : admin   -- Administrateur
-         ide                    ide     -- Préleveur
-         labo                   labo    -- Laboratoire
-         bio                    bio     -- Paramétreur
-         med                    med     -- Prescripteur

Il faudra aussi décompresser l'archive doc/default_data_fichiers.tar.bz2 dans et placer son contenu à la racine de l'application.

#### Si vous souhaitez utiliser une base vide

    python manage.py migrate


### Il ne reste plus qu'à lancer le serveur

    python manage.py runserver

Et voilà !


Installation en production
--------------------------

Récupérer les sources :

    git clone https://git.framasoft.org/microniko/ekzamenoj.git

Créer un environnement virtuel

    mkvirtualenv ekzamenoj --python=/usr/bin/python3

Travailler dans l'environnement virtuel

    workon ekzamenoj

Installer les paquets nécessaires

    pip install -r requirements.txt

Créer un fichier de configuration

    ekzamenoj/local_settings.py

Il permet de surcharger les valeurs du fichier par défaut (`settings.py`). Les valeurs obligatoires sont les suivantes :
- Utiliser [ce site](http://www.miniwebtool.com/django-secret-key-generator/) pour générer `SECRET_KEY`.
- Mettre `DEBUG` et `TEMPLATES[0]["OPTIONS"]["debug"]` à `False`.
- Indiquer le nom d'hôte dans `ALLOWED_HOSTS = ['fqdn']` (pleinement qualifié).
- `EMAIL_HOST` : si vous avez un serveur de courriel en local, commenter cette ligne, sinon, indiquer l'adresse pleinement qualifiée du serveur SMTP.
- Chemin vers les fichiers statiques (voir plus loin), par exemple `STATIC_ROOT = '/var/www/xxx/static/'`.
- `STATICFILES_DIRS = ()` : doit être vide en production.
- La liste des administrateurs doit être indiquée dans `ADMINS`.
- Paramètres de sécurité :

`

    SECURE_SSL_REDIRECT = True # Mettre à True au déploiement !
    SECURE_HSTS_SECONDS = True # Idem
    SECURE_FRAME_DENY   = True
    SECURE_BROWSER_XSS_FILTER = True
    SESSION_COOKIE_SECURE = True # Idem
    SESSION_COOKIE_HTTPONLY = True # Idem

Créer la base de données

    python manage.py syncdb

Collecter les fichiers statiques (de l'administration) *S'assurer que `STATICFILES_DIRS` est bien vide !*

    python manage.py collectstatic

Créer un hôte virtuel en utilisant (ou pas) le fichier d'hôte virtuel fourni pour Apache `vhost_apache.conf`.

Si vous souhaitez utiliser la génération d'étiquettes de prescription, XeLaTeX doit être disponible. La commande suivante (sous Debian) devrait faire l'affaire :

    apt-get install texlive-xetex texlive-lang-french

N'oubliez pas de modifier le templates des étiquettes pour modifier le chemin vers les images.

Après avoir relancé votre serveur HTTP, Ekzamenoj est prêt à être utilisé.


Mise à jour
-----------

### Travailler dans l'environnement virtuel

    workon ekzamenoj

### Mettre à jour

    git pull

### Mettre à jour les paquets nécessaires

    pip install -r requirements.txt

### Lancer le script de mirgrations des données

    $ python manage.py migrate

### Lancer la commande suivante pour récupérer les fichiers statiques :

    $ python manage.py collectstatic

Cela aura pour effet de supprimer le contenu initial de STATICFILES_DIR.

### Modifier si besoin le fichier de configuration local

Depuis la dernière version :

    from ekzamenoj.settings import TEMPLATES

    #TEMPLATE_DEBUG = True
    TEMPLATES[0]["OPTIONS"]["debug"]=False

Traduction
----------
Non prévue pour l'instant (contributeurs bienvenus!)
