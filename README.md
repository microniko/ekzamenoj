Ekzamenoj
=========

Qu'est-ce ?
-----------
Manuel libre des examens de bioogie médicale avec aide à l'optimisation des tubes pour le prélèvement.
Il est distribué avec l'espoir qu'il pourrait être utile (et amélioré) mais sans aucune garantie (cf. Licence - plus bas).
Une version de démonstration est disponible à l'adresse <http://demo.ekzamenoj.microniko.net>.

Documentations
--------------
Une partie de la doc est dans le code

* doc/HOWTO.md    → Alimenter la base manuellement.
* doc/INFOS.md    → Quelques infos peu utiles.
* doc/TODO.md     → Ce qu'il reste à faire dans le développement.
* doc/importation → Importer des données au format CSV (voir le README.md)
* doc/mrd.png     → Image du modèle relationnel des données.

Licence
-------
Ekzamenoj est un Logiciel Libre. Il est distribué sous licence GPL3 (voir le fichier LICENCE.md).

Il est fournit tel quel, *sans aucune garantie*. L'auteur ne saurait être tenu responsable de l'utilisation qui en sera faite. Ainsi, vous utilisez Ekzamenoj à vos propres risques.
Vous êtes cordialement invité à proposer des améliorations. Les sources sont disponibles sur Gitlab <https://gitlab.com/microniko/ekzamenoj> où vous trouverez le moyen de suggérer vos améliorations.

Auteur
------
* N. Grandjean - 2014-2015

Tester
------
Créer un environnement virtuel :

  mkvirtualenv -a Documents/django/ekzamenoj ekzamenoj

Travailler dedans :

  workon ekzamenoj

Instaler ce qu'il faut :

  pip install -r requirements.txt

Initialiser la base de données (répondez gentillement aux questions posées)

Vous pouvez alimenter la base de données avec des données par défaut du fichier doc/default_data.json :

  python manage.py migrate --noinput

puis

  python manage.py loaddata doc/default_data.json

Les utilisateurs par défaut sont :

- login : utilisateur ; mot de passe : nicolas -- Administrateur
-         ide                          ide     -- Préleveur
-         labo                         labo    -- Laboratoire
-         bio                          bio     -- Paramétreur
-         med                          med     -- Prescripteur

Il faudra aussi décompresser l'archive doc/default_data_fichiers.tar.bz2 dans et placer son contenu à la racine de l'application.

Si vous souhaitez utiliser une base vide :

  python manage.py migrate


Il ne reste plus qu'à lancer le serveur :

  python manage.py runserver

Et voilà !


Installation en production
--------------------------

Récupérer les sources :
  git clone git@gitlab.com:microniko/ekzamenoj.git

Créer un environnement virtuel
  mkvirtualenv ekzamenoj --no-site-packages

Travailler dans l'environnement virtuel
  workon ekzamenoj

Installer les paquets nécessaires
  pip install -r requirements.txt 

Renommer le fichier de configuration
  mv ekzamenoj/settings-dist.py ekzamenoj/settings.py

Éditer le fichier de configuration ekzamenoj/settings.py :
- Utiliser <http://www.miniwebtool.com/django-secret-key-generator/> pour générer SECRET_KEY.
- Mettre DEBUG et TEMPLATE_DEBUG à False.
- Indiquer le nom d'hôte dans ALLOWED_HOSTS = ['fqdn'] Utile ?
- EMAIL_HOST : si vous avez un serveur de courriel en local, commenter cette ligne, sinon, indiquer l'adresse pleinement qualifiée du serveur SMTP.
- Chemin vers les fichiers statiques (voir plus loin), par exemple STATIC_ROOT = '/var/www/xxx/static/'.
- STATICFILES_DIRS = () : doit être vide en production.
- La liste des administrateurs doit être indiquée dans ADMINS.

Créer la base de données

  python manage.py syncdb

Collecter les fichiers statiques (de l'administration) *S'assurer que STATICFILES_DIRS est bien vide !*
  python manage.py collectstatic

Créer un hôte virtuel en utilisant (ou pas) le fichier d'hôte virtuel fourni pour Apache :
  vhost_apache.conf

Après avoir relancer votre serveur HTTP, Ekzamenoj est prêt à être utilisé.


Traduction
----------
Non prévue pour l'instant.
