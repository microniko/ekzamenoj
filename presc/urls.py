# -*- coding: utf-8 -*-
# 
# Copyright (C) 2015  Nicolas Grandjean <nicolas@microniko.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf.urls import patterns, include, url



from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.static import static
from django.conf import settings

from django.contrib.auth.decorators import  permission_required

from views import voir_prescription, ListePrescriptions


urlpatterns = patterns('',
       # La liste des prescriptions (tous)
       url(r'^$', permission_required('presc.view_prescriptions',raise_exception=True)(ListePrescriptions.as_view()), name='liste_prescriptions'),

       # Page d'une prescription
       url(r'^(?P<id>\d+)$', voir_prescription, name='voir_prescription'),
    )


