# -*- coding: utf-8 -*-
# 
# Copyright (C) 2015  Nicolas Grandjean <nicolas@microniko.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf.urls import patterns, include, url

from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.static import static
from django.conf import settings

from django.contrib.auth.decorators import  permission_required

from presc.views import voir, Liste, prescrire, en_cours, vider, valider, enlever_examen, associer_patient, enlever_patient, eti_presc, prescrire_bilan, prelever, envoyer, recevoir


app_name = "presc"
urlpatterns = [
       # La liste des prescriptions (tous)
       url(r'^$', permission_required('presc.view_prescriptions',raise_exception=True)(Liste.as_view()), name='liste'),

       # Page d'une prescription
       url(r'^(?P<id>\d+)$', permission_required('presc.view_prescriptions',raise_exception=True)(voir), name='voir'),

       # PDF des étiquettes prescription
       url(r'^etiquettes/(?P<pk>\d+)\.pdf$', permission_required('presc.view_prescriptions',raise_exception=True)(eti_presc), name='etiquettes'),

       # Ajouter à une prescription
       url(r'^prescrire/(?P<examen_id>\d+)$', permission_required('presc.create_prescriptions',raise_exception=True)(prescrire), name='prescrire'),

       # Ajouter un bilan à une prescription
       url(r'^prescrire/bilan/(?P<bilan_id>\d+)$', permission_required('presc.create_prescriptions',raise_exception=True)(prescrire_bilan), name='prescrire_bilan'),

       # Enlever d'une prescription
       url(r'^enlever/(?P<examen_id>\d+)$', permission_required('presc.create_prescriptions',raise_exception=True)(enlever_examen), name='enlever_examen'),

       # Enlever le patient de la prescription en cours de saisie (doit être déclaré avant l'attribution)
       url(r'^patient/enlever$', permission_required('presc.create_prescriptions',raise_exception=True)(enlever_patient), name='enlever_patient'),

       # Attribuer une prescription à un patient
       url(r'^patient/(?P<patient_id>\d+)$', permission_required('presc.create_prescriptions',raise_exception=True)(associer_patient), name='associer_patient'),

       # Détail de la prescription en cours de saisie
       url(r'^voir$', permission_required('presc.create_prescriptions',raise_exception=True)(en_cours), name='prescription'),

       # Vider la prescription en cours de saisie
       url(r'^vider$', permission_required('presc.create_prescriptions',raise_exception=True)(vider), name='vider'),

       # Valider la prescription en cours de saisie
       # Le décorateur de permission ne peut pas être ici (pourquoi ?) il est donc dans views.py
       url(r'^valider$', valider, name='valider'),
       #url(r'^valider$', permission_required('presc.create_prescriptions',raise_exception=True)(valider), name='valider'),

       # Prélever une prescription
       url(r'^prelever/(?P<prescription_id>\d+)$', permission_required('presc.prelever_prescriptions',raise_exception=True)(prelever), name='prelever'),

       # Envoyer une prescription
       url(r'^envoyer/(?P<prescription_id>\d+)$', permission_required('presc.envoyer_prescriptions',raise_exception=True)(envoyer), name='envoyer'),

       # Recevoir une prescription
       url(r'^recevoir/(?P<prescription_id>\d+)$', permission_required('presc.recevoir_prescriptions',raise_exception=True)(recevoir), name='recevoir'),

       ]

