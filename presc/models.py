# -*- coding: utf-8 -*-
# 
# Copyright (C) 2014  Nicolas Grandjean <nicolas@microniko.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from django.db import models

from ekz.models import Examens

#from patients.models import Patients

# Create your models here.

import datetime

class Prescriptions(models.Model):
   # numero = models.IntegerField(verbose_name=u'Numéro unique',)
    #examens = models.ManyToManyField(Examens, verbose_name=u"Examens", limit_choices_to={'is_actif': True})
    examens = models.ManyToManyField(Examens, verbose_name=u"Examens", limit_choices_to={'date_desactivation__gt': datetime.date.today()})
    #prescripteur = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=u"Prescripteur")
    #patient = models.ForeignKey(Patients, verbose_name=u'Patient')
    date_exec = models.DateTimeField(verbose_name=u"Date d'éxécution")
    date_crea = models.DateTimeField(verbose_name=u"Date de création", auto_now_add=True,)
    date_modif = models.DateTimeField(verbose_name=u"Date de la dernière modification", auto_now=True,)
    def __unicode__ (self):
        return str(self.id)
    class Meta:
        verbose_name=u"Prescription"
        verbose_name_plural=u"Prescriptions"
        permissions = (
              ("view_prescriptions", u"Peut voir les prescriptions"),
            )

