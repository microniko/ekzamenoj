# -*- coding: utf-8 -*-
#
# Copyright (C) 2014  Nicolas Grandjean <nicolas@microniko.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import datetime 
from django.shortcuts import render

# Create your views here.
from django.http import Http404, HttpResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic import ListView
from django.contrib import messages
from django.contrib.auth.models import User
from django.core.exceptions import FieldError

from presc.models import Prescriptions
from presc.forms import ValiderPrescriptionForm
from ekz.views import Alphabet
from pages.views import ListeDesPages
from ekz.models import Examens, UtilisationGroupePrelevement, Prelevements, Tubes
from patients.models import Patients

# Pour la génération des étiquettes (images)
from ekzamenoj.settings import MEDIA_ROOT


# Pour les calculs
from django.db.models import Sum
from math import ceil

# Pour les étiquettes
# https://blog.sevenbyte.org/2014/09/23/generating-pdfs-with-django-and-latex.html
from django.template import Context
from django.template.loader import get_template
from subprocess import Popen, PIPE
import tempfile
import os

from ekzamenoj import configlocale
from django.contrib.auth.decorators import permission_required

if configlocale.CONFIG_LOCALE['VOIR_PRESCRIPTIONS'] == True:
    class Liste(ListView):
        """Liste des prescriptions dans la base"""
        model = Prescriptions
        context_object_name= "liste_prescriptions" # ==> Ce que l'on retrouve dans le template'
        template_name = "liste_prescriptions.html"
        def get_context_data(self, **kwargs):
            context = super(Liste, self).get_context_data(**kwargs)
            context['pages'] = ListeDesPages()
            context['alphabet'] = Alphabet()
            return context


    def voir(request, id):
        """Vue perso pour afficher les détails d'une prescription
        Calcul la somme des tubes nécessaires"""
        prescription = get_object_or_404(Prescriptions, id=id) # Ne devra servir qu'à retourner une erreur 404... et savoir de quelle prescription l'on cause
        exam = prescription.examens.all()
        util=list()
        docjoints=list()
        for ex in exam:
            util.append(UtilisationGroupePrelevement.objects.values(
                    'prelevement__nom',
                    'prelevement__tube__nom',
                    'prelevement__nombre',
                    'prelevement__tube__image'
                    ).filter(examen=ex).order_by('prelevement__tube').annotate(Sum('pourcentage')))
            # On en profite pour récupérer les documents
            if ex.fichier.all():
               docjoints.append(ex.fichier.all())
        prelevement=[]
        for ut in util:
            n=ceil(ut[0]['pourcentage__sum']/100.0)*ut[0]['prelevement__nombre']
            dic={'nom_prel':ut[0]['prelevement__nom'],
                 'nbre_tube':n,
                 'nom_tube':ut[0]['prelevement__tube__nom'],
                 'img_tube':ut[0]['prelevement__tube__image'],
                 }
            prelevement.append(dic)

        pages = ListeDesPages()
        alphabet = Alphabet()
        return render(request, 'voir_prescription.html', locals())

    def prescrire(request, examen_id):
        """ Fonction permettant de prescrire un examen
        Elle place l'examen (s'il est dans un groupe de prélèvement) dans une session pour pouvoir être validé ensuite
        """
        if 'prescription' not in request.session:
            prescription = list()
        else:
            prescription = request.session['prescription']

        # On vérifie que l'examen existe dans un groupe de prélèvement…
        if not Prelevements.objects.filter(examen__id=examen_id):
            messages.add_message(request, messages.ERROR,
                    "Cet examen ne peut pas être prescrit car il n'est pas dans un groupe de prélèvement"
                    )
            return redirect(request.META.get('HTTP_REFERER'))

        if examen_id not in prescription:
            prescription.append(examen_id)  # On ajoute l'examen à la prescription
            messages.add_message(request, messages.SUCCESS,
                    'Examen ajouté à la prescription.'
                    )
        else:
            messages.add_message(request, messages.ERROR,
                    'Cet examen est déjà présent dans la prescription'
                    )

        request.session['prescription'] = prescription

        # On retrourne d'où l'on vient…
        if request.META.get('HTTP_REFERER'):
            return redirect(request.META.get('HTTP_REFERER'))
        else:
            return redirect(reverse('ekz'))

    #@permission_required('presc.create_prescriptions', raise_exception=True)
    def prescrire_bilan(request, bilan_id):
        """ Fonction permettant de prescrire les examens d'un bilan
        """
        if 'prescription' not in request.session:
            prescription = list()
        else:
            prescription = request.session['prescription']

        examens = Examens.objects.filter(bilans__id=bilan_id)

        for ex in examens:
            # On vérifie que l'examen existe dans un groupe de prélèvement…
            if not Prelevements.objects.filter(examen__id=ex.id):
                messages.add_message(request, messages.ERROR,
                        "L'examen " + ex.nom + " ne peut pas être prescrit car il n'est pas dans un groupe de prélèvement"
                        )

            if str(ex.id) not in prescription:
                prescription.append(str(ex.id))  # On ajoute l'examen à la prescription
                messages.add_message(request, messages.SUCCESS,
                    ex.nom + " a été ajouté à la prescription"
                    )
            else:
                messages.add_message(request, messages.INFO,
                    ex.nom + ' est déjà présent dans la prescription'
                    )

        request.session['prescription'] = prescription

        # On retrourne d'où l'on vient…
        if request.META.get('HTTP_REFERER'):
            return redirect(request.META.get('HTTP_REFERER'))
        else:
            return redirect(reverse('ekz'))


    def associer_patient(request, patient_id):
        """ Fonction permettant d'attribuer un patient à la prescription
        """
        if 'patient' not in request.session:
            patient = list()
            patient.append(patient_id)  # On ajoute le patient à la session 
            request.session['patient'] = patient 
            messages.add_message(request, messages.SUCCESS,
                    'Prescription attribuée au patient.'
                    )
        else:
            messages.add_message(request, messages.ERROR,
                    'Cette prescription a déjà été atribuée.'
                    )
        # On retrourne à la prescription
        return redirect('prescription')


    #@permission_required('presc.create_prescriptions', raise_exception=True)
    def enlever_patient(request):
        """
        Ôte le patient attribué à la prescription → Supprimer la valeur correspondante dans la session
        """
        if 'patient' in request.session:
            del request.session['patient']
            messages.add_message(request, messages.INFO,
                    'Patient retiré de la prescription.'
                    )
            return redirect(request.META.get('HTTP_REFERER'))
        else:
            messages.add_message(request, messages.ERROR,
                    'Ce patient n\'a pas de la prescription.'
                    )
            return redirect(request.META.get('HTTP_REFERER'))


    #@permission_required('presc.create_prescriptions', raise_exception=True)
    def enlever_examen(request, examen_id):
        """
        Ôte un des examens déjà présent dans la prescription
        """
        if 'prescription' in request.session:
            prescription = request.session['prescription']
            if examen_id in prescription:
                prescription.remove(examen_id)
                prescription = request.session['prescription']
                request.session.modified = True
                messages.add_message(request, messages.INFO,
                    'Examen retiré de la prescription.'
                    )
                return redirect(request.META.get('HTTP_REFERER'))
            else:
                messages.add_message(request, messages.ERROR,
                    'Cet examen ne fait pas partie de la prescription.'
                    )
                return redirect(request.META.get('HTTP_REFERER'))
        else:
            messages.add_message(request, messages.DEBUG,
               'Aucune prescription trouvée !'
               )
            return redirect(request.META.get('HTTP_REFERER'))


    def en_cours(request):
        """ Affiche la liste des examens présents dans la prescription en cours (stockée en session
        """
        pages = ListeDesPages()
        alphabet = Alphabet()
        if 'prescription' in request.session:
            prescription = list()
            #print(prescription)
            #print(request.session.get('prescription'))
            for examen_id in request.session.get('prescription'):
                prescription_ligne = Examens.objects.get(id=examen_id)
                list.append(prescription, prescription_ligne)
        else:
            prescription=None
        if 'patient' in request.session:
            for patient_id in request.session.get('patient'):
                prescription_patient = Patients.objects.get(id=patient_id)
        else:
            prescription_patient = None
        return render(request, 'prescription.html', locals())


    #@permission_required('presc.create_prescriptions', raise_exception=True)
    def vider(request):
        """ Supprime la prescription en cours de saisie """
        del request.session['prescription']
        messages.add_message(request, messages.INFO,
                    'La prescription a été vidée.'
                    )
        # On retrourne d'où l'on vient…
        if request.META.get('HTTP_REFERER'):
            return redirect(request.META.get('HTTP_REFERER'))
        else:
            return redirect(reverse('ekz'))

    @permission_required('presc.create_prescriptions', raise_exception=True)
    def valider(request):
        """ Valider une prescription : enregistrer en base
        Le décorateur de permission ne peut pas être dans url.py mais je ne sais pas pourquoi.
        """
        pages = ListeDesPages()
        alphabet = Alphabet()
        if 'prescription' in request.session:
            prescription = list()
            for examen_id in request.session.get('prescription'):
                prescription_ligne = Examens.objects.get(id=examen_id)
                list.append(prescription, prescription_ligne)
        else:
            prescription=None
            messages.add_message(request, messages.DEBUG,
                        "Il n'y a pas de prescription en attente !"
                        )
            return redirect('prescription')
        if 'patient' in request.session:
            prescripteur = User.objects.get(id = request.user.id)
            #print request.session.get('patient')
            patient = Patients.objects.get(id = request.session.get('patient')[0])
            prescription_patient = patient
            if request.method== 'POST':
                form = ValiderPrescriptionForm(request.POST)
                if form.is_valid():
                    if 'prescription' in request.session:
                        p = Prescriptions(
                                date_exec=form.cleaned_data['date_exec'],
                                prescripteur = prescripteur,
                                patient = patient,
                                commentaire = form.cleaned_data['commentaire']
                                )
                        p.save()
                        for examen in request.session['prescription']:
                            ex = Examens.objects.get(id=examen)
                            p.examens.add(ex)
                        del request.session['prescription']
                        del request.session['patient']
                        messages.add_message(request, messages.SUCCESS,
                            'La prescription a été validée. Elle porte le numéro → ' + str(p.id)
                            )
                        return redirect('voir', id=p.id)
                    else:
                        messages.add_message(request, messages.DEBUG,
                            'La prescription est vide !'
                            )
                        return redirect(request.META.get('HTTP_REFERER'))
            else:
                form = ValiderPrescriptionForm()
            return render(request, 'prescrire.html', locals())
        else:
            messages.add_message(request, messages.DEBUG,
                        "Il n'y a pas de prescription en attente !"
                        )
            return redirect('prescription')

    def prelever(request, prescription_id):
        """ Prélever une prescription : mettre la date/heure courante et l'utilisateur connecté comme prélveur """
        preleveur = User.objects.get(id = request.user.id)
        try:
            Prescriptions.objects.filter(id=prescription_id).update(date_prel=datetime.datetime.now(), preleveur=preleveur)
            messages.add_message(request, messages.SUCCESS,
                    'Prescription prélevée.'
                    )
        except FieldError:
            messages.add_message(request, messages.DEBUG,
                'Quelque chose s\'est mal passé. Mise à jour de la prescription impossible'
                )
            return redirect(request.META.get('HTTP_REFERER'))
        return redirect('voir', prescription_id)

    def envoyer(request, prescription_id):
        """ Envoyer une prescription : mettre la date/heure courante """
        try:
            Prescriptions.objects.filter(id=prescription_id).update(date_depart=datetime.datetime.now())
            messages.add_message(request, messages.SUCCESS,
                    'Prescription envoyée.'
                    )
        except FieldError:
            messages.add_message(request, messages.DEBUG,
                'Quelque chose s\'est mal passé. Mise à jour de la prescription impossible'
                )
            return redirect(request.META.get('HTTP_REFERER'))
        return redirect('voir', prescription_id)

    def recevoir(request, prescription_id):
        """ Recevoir une prescription : mettre la date/heure courante """
        try:
            Prescriptions.objects.filter(id=prescription_id).update(date_arrive=datetime.datetime.now())
            messages.add_message(request, messages.SUCCESS,
                    'Prescription reçue.'
                    )
        except FieldError:
            messages.add_message(request, messages.DEBUG,
                'Quelque chose s\'est mal passé. Mise à jour de la prescription impossible'
                )
            return redirect(request.META.get('HTTP_REFERER'))
        return redirect('voir', prescription_id)


    def etiquettes(request, templ, contexte):
        """ Fonction python3 qui génère le PDF des étiquettes
            Il faudra probablement la déplacer si on a besoin de
            générer des étiquettes ailleurs.
            Arguments à fournir, dans l'ordre :
            - request,
            - templ : chemin vers le modèle en LaTeX,
            - contexte : objet récupérant les données dans le modèle
                         Ex : Modele.objects.get(pk=pk)
            Utiliser le modèle avec la syntaxe habituelle. Les données sont contenues
            dans {{ content }}
            Il faut XeLaTeX car PDFLaTeX n'est pas capable de générer un PDF avec PStricks
            (utilisé pour générer un code-à-barre…)
            Très inspiré de : https://blog.sevenbyte.org/2014/09/23/generating-pdfs-with-django-and-latex.html
        """
        context = {
                    'content': contexte,
                    }
        template = get_template(templ)
        rendered_tpl = template.render(context).encode('utf-8')
        #print(rendered_tpl)
        #import time
        #time.sleep(10)
        with tempfile.TemporaryDirectory() as tempdir:
            for i in range(1):
                process = Popen(
                        ['xelatex', '-output-directory', tempdir],
                        #['pdflatex', '-output-directory', tempdir],
                        stdin=PIPE,
                        stdout=PIPE,
                        )
                process.communicate(rendered_tpl)
                #import time
                #time.sleep(10)
            with open(os.path.join(tempdir, 'texput.pdf'), 'rb') as f:   # Pour une raison inconnue le PDF généré n'est pas etiquettes.pdf…
                pdf = f.read()
            r= HttpResponse(content_type='application/pdf')
            r.write(pdf)
            return r

    def eti_presc(request, pk):
        """
        Retourne les étiquettes de la prescription
        Génère autant d'étiquettes que nécessaire.
        """
        prescription = Prescriptions.objects.get(id=pk) # La prescription
        exam = prescription.examens.all()
        util = list()
        for ex in exam:
            util.append(UtilisationGroupePrelevement.objects.values(
                'prelevement__nom',
                'prelevement__tube__nom',
                'prelevement__nombre',
                'prelevement__tube__image').filter(examen=ex).order_by('prelevement__tube').annotate(Sum('pourcentage'))
                )
        prelevement=[]
        x = 1; # pour afficher et compter l'odre des tubes
        for ut in util:
            print("UT →")
            print(ut[0])
            n=ceil(ut[0]['pourcentage__sum']/100.0)*ut[0]['prelevement__nombre']
            i=1
            while i<=n:
                dic={'nom_prel':ut[0]['prelevement__nom'],
                     'ordre_tube':x,
                     'nom_tube':ut[0]['prelevement__tube__nom'],
                     'img_tube':MEDIA_ROOT+'/'+ut[0]['prelevement__tube__image'],
                     'patient': prescription.patient,
                     'date_exec': prescription.date_exec,
                     'cb': prescription.id
                     }
                prelevement.append(dic)
                x=x+1  # On incrémente le compteur d'ordre
                i=i+1  # Le compteur du nombre de tubes par prélèvement aussi

        return etiquettes(request, "etiquettes.tex", prelevement)
