# -*- coding: utf-8 -*-
#
# Copyright (C) 2014  Nicolas Grandjean <nicolas@microniko.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from django.shortcuts import render

# Create your views here.
from django.http import Http404
from django.shortcuts import render, get_object_or_404
from django.views.generic import ListView


from presc.models import Prescriptions
from ekz.views import Alphabet
from pages.views import ListeDesPages
from ekz.models import Examens, UtilisationGroupePrelevement, Prelevements, Tubes

#from patients.models import Patients


# Pour les calculs
from django.db.models import Sum
from math import ceil

from ekzamenoj import localsettings
from django.contrib.auth.decorators import permission_required

if localsettings.CONFIG_LOCALE['VOIR_PRESCRIPTIONS'] == True:
    class ListePrescriptions(ListView):
        """Liste des prescriptions dans la base"""
        model = Prescriptions
        context_object_name= "liste_prescriptions" # ==> Ce que l'on retrouve dans le template'
        template_name = "liste_prescriptions.html"
        def get_context_data(self, **kwargs):
            context = super(ListePrescriptions, self).get_context_data(**kwargs)
            context['pages'] = ListeDesPages()
            context['alphabet'] = Alphabet()
            return context



    @permission_required('presc.view_prescriptions', raise_exception=True)
    def voir_prescription(request, id):
        """Vue perso pour afficher les détails d'une prescription
        Calcul la somme des tubes nécessaires"""
        prescription = get_object_or_404(Prescriptions, id=id) # Ne devra servir qu'à retourner une erreur 404... et savoir de quelle prescription l'on cause
        util = UtilisationGroupePrelevement.objects.values('prelevement__nom', 'prelevement__tube__nom', 'prelevement__nombre', 'prelevement__tube__image').filter(examen=prescription.examens.all).order_by('prelevement__tube').annotate(Sum('pourcentage'))
        #synonymes = Synonymes.objects.filter(examen_id=examen.id)
        pages = ListeDesPages()
        alphabet = Alphabet()
        exam  = Examens.objects.filter(id=prescription.examens.all)#.order_by('nom')
        prelevement=[]
        for ut in util:
            n=ceil(ut['pourcentage__sum']/100.0)*ut['prelevement__nombre']
            dic={'nom_prel':ut['prelevement__nom'],
                 'nbre_tube':n,
                 'nom_tube':ut['prelevement__tube__nom'],
                 'img_tube':ut['prelevement__tube__image'],
                 }
            prelevement.append(dic)

        return render(request, 'voir_prescription.html', locals())

