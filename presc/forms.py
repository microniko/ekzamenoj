# -*- coding: utf-8 -*-
# 
# Copyright (C) 2015  Nicolas Grandjean <nicolas@microniko.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime
from django import forms
from presc.models import Prescriptions
from ekz.models import Examens

class ValiderPrescriptionForm(forms.ModelForm):
    """ Formulaire de validation d'une prescription
    """
    date_exec=forms.SplitDateTimeField(initial=datetime.datetime.now, label="Date d'exécution")
    class Meta:
        model = Prescriptions
        exclude = ('date_crea', 'date_modif', 'examens', 'prescripteur', 'patient', 'date_prel', 'preleveur', 'date_depart', 'date_arrive')

