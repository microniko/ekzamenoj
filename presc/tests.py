# -*- coding: utf-8 -*-
# 
# Copyright (C) 2014  Nicolas Grandjean <nicolas@microniko.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.test import TestCase

# Create your tests here.

from presc.models import *

from ekz.models import Examens

class TestPrescriptions(TestCase):
    """ Classe de test des prescriptions. """
    def test_examen_create_dans_presc(self):
        """ Pour tester la création d'un examen basique car seulement pour tester les prescriptions"""
        exam = Examens(nom=u"Examen pour tester les prélèvements")
        exam.save()
        # Vérification
        ex = Examens.objects.get(id=1)
        self.assertEqual(ex.nom,u"Examen pour tester les prélèvements")

    def test_precription_create(self):
        """ Test de création de prescription
        FIXME → pb avec Many2Many"""
        self.test_examen_create_dans_presc()
        x = Examens.objects.get(id=1)
        presc = Prescriptions(examens=x,
                              date_exec=datetime.date.today())
        presc.save()

