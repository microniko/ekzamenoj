# -*- coding: utf-8 -*-
# 
# Copyright (C) 2015  Nicolas Grandjean <nicolas@microniko.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf.urls import patterns, include, url



from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.static import static
from django.conf import settings

#from django.contrib.auth.decorators import  permission_required


from patients.views import PatientCreate, PatientView, Liste, recherche


app_name = "patients"
urlpatterns = [
       # La liste des patients
       #url(r'^$',Liste.as_view(), name='liste_patients'),

       # Page de recherche
       url(r'^$', recherche, name='recherche'),
       #url(r'^rechercher/$', recherche, name='recherche'),

       # Créer un nouveau patient
       url(r'^creer$', PatientCreate.as_view(), name='creer_patient'),

       # Voir le détail d'un patient
       url(r'^voir/(?P<pk>\d+)$', PatientView.as_view(), name='voir_patient'),


       ]
