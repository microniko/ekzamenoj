# -*- coding: utf-8 -*-
# 
# Copyright (C) 2014  Nicolas Grandjean <nicolas@microniko.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from django.db import models
from django.core.urlresolvers import reverse

# Create your models here.

#class Tracabilites(models.Model):
#    date = models.DateTimeField(verbose_name=u"Date", auto_now_add=True,)
#    description = models.CharField(verbose_name=u'Description', max_length=255)
#    def __str__ (self):
#        return self.date
#    class Meta:
#        verbose_name=u'Traçabilité'
#        verbose_name_plural=u'Traçabilités'


class Civilites(models.Model):
    abbreviation = models.CharField(max_length=5, unique=True, null=False, verbose_name=u'Abbréviation')
    nom = models.CharField(max_length=50, unique=True, null=False, verbose_name=u'Nom complet')
    date_modif = models.DateTimeField(verbose_name=u"Date de la dernière modification", auto_now_add=True,)
    def __str__ (self):
        return self.nom
    class Meta:
        verbose_name=u"Civilité"
        verbose_name_plural=u"Civilités"

class Etablissements(models.Model):
    nom = models.CharField(max_length=255,verbose_name=u'Nom')
    description = models.CharField(max_length=255, verbose_name=u'Description')
    def __str__ (self):
        return self.nom
    class Meta:
        verbose_name=u'Établissement'
        verbose_name_plural=u'Établissements'


class Patients(models.Model):
    SEXE = (
        (0,u'Indéterminé'),
        (1,u'Féminin'),
        (2,u'Masculin'),
    )
    #civilite = models.ForeignKey(Civilites)
    nom_usage = models.CharField(max_length=255, verbose_name=u"Nom d'usage")
    nom_naissance = models.CharField(max_length=255, verbose_name=u"Nom de naissance", blank=True)
    prenom = models.CharField(max_length=255, verbose_name=u'Prenom')
    sexe = models.IntegerField(verbose_name=u'Sexe', null=False, choices=SEXE)
    ddn = models.DateField(verbose_name=u"Date de naissance")
    #adresse = models.TextField(verbose_name=u'Adresse', blank=True)
   # num_unique = models.BigIntegerField(verbose_name=u'Identifiant unique du patient (???)', null=True,blank=True)
   # tracabilite = models.OneToOneField(Tracabilites, verbose_name=u"Tracabilite", blank=True, null=True)

    def get_absolute_url(self):
        return reverse('voir_patient', kwargs={'pk': self.pk})

    def __str__ (self):
        if self.sexe == 1:
            self.ne = u"née"
        else:
            self.ne = u"né"
        if self.nom_naissance != self.nom_usage or self.nom_naissance:
            return u"{0} {1} {2} {3} le {4}".format(self.prenom,self.nom_usage, self.ne, self.nom_naissance, self.ddn)
        else:
            return u"{0} {1} {2} le {3}".format(self.prenom,self.nom_usage, self.ne, self.ddn)
    class Meta:
        verbose_name=u"Patient"
        verbose_name_plural=u"Patients"


class Services(models.Model):
    code = models.CharField(max_length=50, verbose_name=u'Code')
    nom = models.CharField(max_length=255, verbose_name=u'Nom')
    etablissement = models.ForeignKey(Etablissements, verbose_name=u'Etablissement')
    date_modif = models.DateTimeField(verbose_name=u"Date de la dernière modification", auto_now_add=True,)
    def __str__ (self):
        return u'{0} / {1} ({2})'.format(self.etablissement,self.nom, self.code)
    class Meta:
        verbose_name=u'Service'
        verbose_name_plural=u'Services'

class Sejours(models.Model):
    patient = models.ForeignKey(Patients)
    numero = models.BigIntegerField(verbose_name=u'Numéro de séjour')
    ipp = models.BigIntegerField(verbose_name=u'Identifiant permanant du patient (IPP)', blank=True)
    service = models.ForeignKey(Services, verbose_name=u"Service")
    date_modif = models.DateTimeField(verbose_name=u"Date de la dernière modification", auto_now_add=True,)

    def __str__ (self):
        return u'{0} / {1}'.format(self.patient,self.service)
    class Meta:
        verbose_name=u'Séjour'
        verbose_name_plural=u'Séjours'

