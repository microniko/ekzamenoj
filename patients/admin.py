# -*- coding: utf-8 -*-
# 
# Copyright (C) 2014  Nicolas Grandjean <nicolas@microniko.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from django.contrib import admin

# Register your ekz.models here.
from patients.models import Civilites, Patients, Sejours, Etablissements, Services

#class TracabilitesAdmin(admin.ModelAdmin):
#    """Administration de Traçabilité """
    #list_display = ('','')
    #list_filter  = ()
    #date_hierarchy = ''
    #ordering = ('',)
    #search_field = ('')
#admin.site.register(Tracabilites, TracabilitesAdmin)

class CivilitesAdmin(admin.ModelAdmin):
    """Administration de Civilités """
admin.site.register(Civilites, CivilitesAdmin)

class PatientsAdmin(admin.ModelAdmin):
    """Administration des patients  """
admin.site.register(Patients, PatientsAdmin)

class SejoursAdmin(admin.ModelAdmin):
    """Administration des séjours"""
admin.site.register(Sejours, SejoursAdmin)

class ServicesAdmin(admin.ModelAdmin):
    """Administration des services"""
admin.site.register(Services, ServicesAdmin)

class EtablissementsAdmin(admin.ModelAdmin):
    """Administration des établissements"""
admin.site.register(Etablissements, EtablissementsAdmin)
