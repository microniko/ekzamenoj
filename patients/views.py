# -*- coding: utf-8 -*-
# 
# Copyright (C) 2015  Nicolas Grandjean <nicolas@microniko.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.shortcuts import render

# Create your views here.
from django.views.generic.edit import CreateView
from django.views.generic.detail import DetailView
from django.views.generic import ListView
from patients.models import Patients
from patients.forms import RecherchePatientsForm
from ekz.views import Alphabet
from presc.models import Prescriptions
from pages.views import ListeDesPages

class Liste(ListView):
    """Liste des patients dans la base"""
    model = Patients
    context_object_name= "liste_patients" # ==> Ce que l'on retrouve dans le template'
    template_name = "liste.html"
    def get_context_data(self, **kwargs):
        context = super(Liste, self).get_context_data(**kwargs)
        context['pages'] = ListeDesPages()
        context['alphabet'] = Alphabet()
        return context


class PatientCreate(CreateView):
    """
    Classe générique de création d'un patient
    """
    model = Patients
    fields = [ 'nom_usage', 'nom_naissance', 'prenom', 'ddn','sexe' ]
    template_name = "patients_form.html"



class PatientView(DetailView):
    """
    Classe générique de visualisation d'un patient
    """
    model = Patients
    template_name = "voir_patient.html"
    def get_context_data(self, **kwargs):
        context = super(PatientView, self).get_context_data(**kwargs)
        context['pages'] = ListeDesPages()
        context['alphabet'] = Alphabet()
        context['prescriptions'] = Prescriptions.objects.filter(patient=context['patients'])
        return context


def recherche(request):
    '''Recherche de patients '''
    if len(request.POST) > 0:
        form = RecherchePatientsForm(request.POST)
        if form.is_valid():
            ddn = form.cleaned_data['ddn']
            Resultats = Patients.objects.filter(ddn=ddn).order_by('nom_usage')
    else:
        form = RecherchePatientsForm()
    return render(request, 'recherche_patients.html', locals())

