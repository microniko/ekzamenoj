# -*- coding: utf-8 -*-
# 
# Copyright (C) 2014  Nicolas Grandjean <nicolas@microniko.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


"""
Django settings for ekzamenoj project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = ''

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'csvimport.app.CSVImportConf',
    'ekz',
    'django_summernote',
    'pages',
    'presc',
    #'patients',
    'django_extensions',
    'django.contrib.admindocs',
    #'debug_toolbar',
    'djangosecure',
)

MIDDLEWARE_CLASSES = (
    'djangosecure.middleware.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'ekzamenoj.urls'

WSGI_APPLICATION = 'ekzamenoj.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'fr-FR'

TIME_ZONE = 'Europe/Paris'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)

# Endroit des images
MEDIA_ROOT = BASE_DIR + "/fichiers"
#MEDIA_ROOT = os.path.dirname(os.path.dirname(__file__))+"/images"
MEDIA_URL = "/fichiers/"

# Activer au déploiement (python manage.py collectstatic afin de récupérer les fichiers statiques de l'admin)
#STATIC_ROOT = '/var/www/xxx/static/'


# Administrateurs
ADMINS = (
    ("", ""),
)

# Gabarits
TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, 'templates'),
)

TEMPLATE_CONTEXT_PROCESSORS = (
   'django.contrib.auth.context_processors.auth',
   # Récupérer la config
   'ekz.context_processors.configuration',
)


#Ajoute un Slash à la fin de chaque URL
APPEND_SLASH = True

# Courrels
EMAIL_HOST = 'smtp.xx.fr'



# Librairie JQuery en locale
DEBUG_TOOLBAR_CONFIG = {
   'JQUERY_URL' : '/static/jquery/jquery.js',
}

# Où aller après s'être authentifié ?
LOGIN_REDIRECT_URL = '/'


# Config Summernote (https://github.com/summernote/django-summernote#options - http://summernote.org/#/features#api)
SUMMERNOTE_CONFIG = {
    'iframe':False,
    'toolbar': [
        ['style', ['style']],
        ['misc', ['undo', 'redo' ]],
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['insert', ['picture', 'link', 'table']],
        ['layout', ['ul','ol' ]],
        ['help', ['help','codeview' ]],
    ],
    'inplacewidget_external_css' : (
        '/static/bootstrap/css/bootstrap.min.css',
        '/static/font-awesome/css/font-awesome.min.css',
    ),
    'inplacewidget_external_js' : (
        '/static/jquery/jquery.js',
        '/static/bootstrap/js/bootstrap.min.js',
    ),

}

# Config Django-Secure (http://django-secure.readthedocs.org/en/latest/#usage)
# Vérifier par python manage.py checksecure

SECURE_SSL_REDIRECT = False # Mettre à True au déploiement !
SECURE_HSTS_SECONDS = False # Idem
SECURE_FRAME_DENY   = True
SECURE_BROWSER_XSS_FILTER = True
SESSION_COOKIE_SECURE = False # Idem
SESSION_COOKIE_HTTPONLY = False # Idem
