# -*- coding: utf-8 -*-
# 
# Copyright (C) 2014  Nicolas Grandjean <nicolas@microniko.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


""" Config locale
    Permet de personnaliser le comportement de l'application.

    Les valeurs du dictionnaire CONFIG_LOCALE sont disponibles dans
    les templates (grâce à ekz/context_processors.py dans les
    TEMPLATE_CONTEXT_PROCESSORS du settings.py)
    ainsi que dans l'ensemble du code python.
    """


CONFIG_LOCALE = {
    # Petit titre sur toutes les pages (1ère ligne)
    'TITRE_1' : u'Laboratoire Biolibre',
    # Grand titre sur toutes les pages (2ème ligne)
    'TITRE_2' : u'Manuel <em>Libre</em> des examens de biologie médicale',
    # Page d'accueil seulement
    'TITRE_3' : u"Bienvenue sur Ekzamenoj",
    # Chemin vers le logo
    'URL_LOGO' : "/static/logo.png",
    # Texte affiché au survol du logo (attribut title)
    'TITLE_LOGO' : u"Accueil",
    # Texte texte alternatif au logo (attribut alt)
    'ALT_LOGO' : u"Le logo n'a pas pu être chargé !",
    # Chemin vers l'icône
    'URL_FAVICON' : "/static/favicon.png",
    # Permet de lister les bons de demande d'examens avec un lien dans le menu
    'VOIR_LISTE_DES_BONS': True,
    # Active les prescriptions
    'VOIR_PRESCRIPTIONS': True,
    # Permettre de voir la liste complète des examens
    'VOIR_LISTE_EXAMENS': True,
    # Monnaie locale
    'MONNAIE_LOCALE' : u" €",
    # URL du serveur (utilisé pour l'opensearch.xml)
    'SERVER_NAME' : "localhost:8000",
    # Référentiel externe (page de recherche) on ajoute à la suite le nom de l'examen
    'REF_EXT' : "http://www.labtestonline.fr/tests/risultatoricerca/index.html?q=",
    #
    '' : "",
}
