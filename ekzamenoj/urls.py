# -*- coding: utf-8 -*-
# 
# Copyright (C) 2014  Nicolas Grandjean <nicolas@microniko.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from django.conf.urls import patterns, include, url

from ekzamenoj import localsettings


from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.static import static
from django.conf import settings

from django.views.generic.base import TemplateView

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'ekzamenoj.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    # Page d'accueil (i.e. « home »)
    url(r'^$', 'ekz.views.accueil', name='accueil'),

    url('^examens/', include('ekz.urls')),
    url('^page', include('pages.urls')),

    url(r'^summernote/', include('django_summernote.urls')),

    # Admindoc
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    url(r'^admin/', include(admin.site.urls)),

    # Login à part (provisoire ?)
    url(r'^login/$', 'django.contrib.auth.views.login', {'template_name': 'login.html'}, name='login'),

    # Déconnexion (nécessaire pour les utilisateurs n'ayant pas le statut équipe)
    url(r'^logout/$', 'django.contrib.auth.views.logout', {'next_page': 'accueil'}, name='logout'),

    # Page test presc
    #url(r'^prescription/$', prescrire, name='prescrire'),

    # Fichiers robots.txt
    url('^robots.txt$', TemplateView.as_view(template_name='robots.txt'), name='robots'),

)

urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if localsettings.CONFIG_LOCALE['VOIR_PRESCRIPTIONS'] == True:
    urlpatterns += patterns('',
        url('^prescriptions/', include('presc.urls')),

    )
