# -*- coding: utf-8 -*-
# 
# Copyright (C) 2014  Nicolas Grandjean <nicolas@microniko.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from django.conf.urls import patterns, include, url

from ekzamenoj import configlocale
from ekz.views import accueil
from django.contrib.auth.views import login, logout


from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.static import static
from django.conf import settings

from django.views.generic.base import TemplateView

from django.contrib import admin
admin.autodiscover()

urlpatterns =[
    # Examples:
    # url(r'^$', 'ekzamenoj.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    # Page d'accueil (i.e. « home »)
    url(r'^$', accueil, name='accueil'),

    url('^examens/', include('ekz.urls')),
    url('^page', include('pages.urls')),
    url('^patient/', include('patients.urls')),

    url(r'^summernote/', include('django_summernote.urls')),

    # Admindoc
    url(r'^' + settings.ADMIN_URL + '/doc/', include('django.contrib.admindocs.urls')),

    url(r'^' + settings.ADMIN_URL + '/', include(admin.site.urls)),

    # Login à part (provisoire ?)
    url(r'^login/$', login, {'template_name': 'login.html'}, name='login'),

    # Déconnexion (nécessaire pour les utilisateurs n'ayant pas le statut équipe)
    url(r'^logout/$', logout, {'next_page': 'accueil'}, name='logout'),

    # Page test presc
    #url(r'^prescription/$', prescrire, name='prescrire'),

    # Fichiers robots.txt
    url('^robots.txt$', TemplateView.as_view(template_name='robots.txt'), name='robots'),

]

urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if configlocale.CONFIG_LOCALE['VOIR_PRESCRIPTIONS'] == True:
    urlpatterns +=[ 
        url('^prescriptions/', include('presc.urls')),
        ]
