# -*- coding: utf-8 -*-
# 
# Copyright (C) 2015  Nicolas Grandjean <nicolas@microniko.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
""" Importation du CSV des Tubes
    Usage : python manage.py runscript import_tubes
    Pour forcer si la tables n'est pas vide (non conseillé) :
      python manage.py runscript import_tubes --script-args=force
    Pour mettre la table à jour :
      python manage.py runscript import_tubes --script-args=maj
    Format du fichier :
    "nom","ordre","image"
"""
from django.core.files import File
from ekz.models import Tubes

import sys,os, os.path
import csv

def run(*args):
    # Localisation du CSV
    csv_file= raw_input("Chemin vers le fichier ? ")
    if os.path.isfile(csv_file) == False:
        print "Je n'ai pas trouvé le fichier!"
        sys.exit()
    dataReader= csv.reader(open(csv_file), delimiter=",", quotechar='"')
    if "maj" in args:
        if Tubes.objects.count()>0:
            print "Mise à jour…"
            for row in dataReader:
                if row[0] != "nom":
                    tube=Tubes.objects.get(nom=row[0])
                    tube.ordre=row[1]
                    if row[2] != "":
                        fic = File(open(row[2], "rb"))
                        tube.image.save(row[2], fic, save=False)
                    tube.save()
        else:
            print "C'est ennuyeux de mettre à jour quand la table est vide…"
    elif Tubes.objects.count()>0 and not "force" in args:
        print "La table des tubes n'est pas vide ! ajoutez « --script-args=force » pour voir"
    else:
        print 'Insertions…'
        for row in dataReader:
            if row[0] != "nom":
                tube=Tubes()
                tube.nom=row[0]
                tube.ordre=row[1]
                if row[2] != "":
                     fic = File(open(row[2], "rb"))
                     tube.image.save(row[2], fic, save=False)
                tube.save()

