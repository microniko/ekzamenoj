# -*- coding: utf-8 -*-
# 
# Copyright (C) 2015  Nicolas Grandjean <nicolas@microniko.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
""" Importation du CSV des preanalytiques
    Usage : python manage.py runscript import_preanalytiques
    Utilise par défaut le format
    Pour forcer si la tables n'est pas vide (mais si des preanalytiques existent déjà, ça ne fonctionnera pas) :
      python manage.py runscript import_preanalytiques --script-args=force
    Pour mettre la table à jour :
      python manage.py runscript import_preanalytiques --script-args=maj
    Format du fichier :
    "centri_vitesse","centri_temps","stock_nom","stock_piece","stock_temperature","aliquotage","analyseur_nom"
"""
from ekz.models import Preanalytiques, Analyseurs, Temperatures, Centrifugations, Stockages

import sys,os, os.path
import csv

def run(*args):
    # Localisation du CSV
    csv_file= raw_input("Chemin vers le fichier ? ")
    if os.path.isfile(csv_file) == False:
        print "Je n'ai pas trouvé le fichier!"
        sys.exit()
    dataReader= csv.reader(open(csv_file), delimiter=",", quotechar='"')
    if "maj" in args:
        print "On ne peut pas faire de mise à jour en masse."
    elif Preanalytiques.objects.count()>0 and not "force" in args:
        print "La table des preanalytiques n'est pas vide ! ajoutez « --script-args=force » pour voir"
    else:
        print 'Insertions…'
        for row in dataReader:
            if row[0] != 'centri_vitesse':
                preanalytique=Preanalytiques()
                if row[0] !="" and row[1] != "":
                    centri=Centrifugations()
                    centri.vitesse=row[0]
                    centri.temps=row[1]
                    centri.save()
                    preanalytique.centrifugation=centri
                if row[2] !="" and row[3] != "" and row[4] != "":
                    stock=Stockages()
                    stock.nom=row[2]
                    stock.piece=row[3]
                    if row[4] != "":
                        temp= Temperatures.objects.get(nom=row[4])
                        stock.temperature =temp
                    stock.save()
                    preanalytique.stockage=stock
                preanalytique.aliquotage=row[5]
                if row[6] != "":
                    preanalytique.analyseur= Analyseurs.objects.get(nom=row[6])
                preanalytique.save()


