# -*- coding: utf-8 -*-
# 
# Copyright (C) 2015  Nicolas Grandjean <nicolas@microniko.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
""" Importation du CSV des methodes
    Usage : python manage.py runscript import_methodes
    Pour forcer si la tables n'est pas vide (non conseillé) :
      python manage.py runscript import_actes --script-args=force
    Pour mettre la table à jour :
      python manage.py runscript import_actes --script-args=maj
    Format du fichier :
    "nom","description"
"""


from ekz.models import Methodes

import sys,os, os.path
import csv



def run(*args):
    # Localisation du CSV
    csv_file= raw_input("Chemin vers le fichier ? ")
    if os.path.isfile(csv_file) == False:
        print "Je n'ai pas trouvé le fichier!"
        sys.exit()
    dataReader= csv.reader(open(csv_file), delimiter=",", quotechar='"')
    if "maj" in args:
        if Methodes.objects.count()>0:
            print "Mise à jour…"
            for row in dataReader:
                if row[0] != "nom":
                    methode=Methodes.objects.get(nom=row[0])
                    methode.description=row[1]
                    methode.save()
        else:
            print "C'est ennuyeux de mettre à jour quand la table est vide…"
    elif Methodes.objects.count()>0 and not "force" in args:
        print "La table des méthodes n'est pas vide ! ajoutez « --script-args=force » pour voir"
    else:
        print 'Insertions…'
        for row in dataReader:
            if row[0] != 'nom':
                methode=Methodes()
                methode.nom=row[0]
                methode.description=row[1]
                methode.save()

