# -*- coding: utf-8 -*-
# 
# Copyright (C) 2015  Nicolas Grandjean <nicolas@microniko.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
""" Importation du CSV des Fichiers
    Usage : python manage.py runscript import_fichiers
    Pour forcer si la tables n'est pas vide (non conseillé) :
      python manage.py runscript import_fichiers --script-args=force
    Pour mettre la table à jour :
      python manage.py runscript import_fichiers --script-args=maj
    Format du fichier :
    "nom","description","fichier","Examen"
"""
from django.core.files import File
from ekz.models import Fichiers, Examens
from django.core.exceptions import ObjectDoesNotExist

import sys,os, os.path
import csv

def run(*args):
    # Localisation du CSV
    csv_file= raw_input("Chemin vers le fichier ? ")
    if os.path.isfile(csv_file) == False:
        print "Je n'ai pas trouvé le fichier!"
        sys.exit()
    dataReader= csv.reader(open(csv_file), delimiter=",", quotechar='"')
    if "maj" in args:
        if Fichiers.objects.count()>0:
            print "Mise à jour impossible…"
        else:
            print "C'est ennuyeux de mettre à jour quand la table est vide…"
    elif Fichiers.objects.count()>0 and not "force" in args:
        print "La table des fichiers n'est pas vide ! ajoutez « --script-args=force » pour voir"
    else:
        print 'Insertions…'
        for row in dataReader:
            if row[0] != "nom":
                try:
                    fichier = Fichiers.objects.get(nom=row[0])
                except ObjectDoesNotExist:
                    # Le fichier n'existe pas déjà
                    print "Fichier : " + row[0]
                    fichier=Fichiers()
                    fichier.nom=row[0]
                    if row[2] != "":
                        fic = File(open(row[2], "rb"))
                        fichier.fichier.save(row[2], fic, save=False)
                    fichier.save()
                else:
                    print "Le fichier "+ row[0] + " existe déjà dans la base"
                print "Associé à "+ row[3]
                examen=Examens.objects.get(nom=row[3])
                examen.fichier.add(fichier)

