# -*- coding: utf-8 -*-
# 
# Copyright (C) 2015  Nicolas Grandjean <nicolas@microniko.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
""" Importation du CSV des Transporteurs
    Usage : python manage.py runscript import_transporteurs
    Pour forcer si la tables n'est pas vide (non conseillé) :
      python manage.py runscript import_transporteurs --script-args=force
    Pour mettre la table à jour :
      python manage.py runscript import_transporteurs --script-args=maj
    Format du fichier :
    "nom","description","courriel","cp","ville","telephone","url"
"""


from ekz.models import Transporteurs

import sys,os, os.path
import csv



def run(*args):
    # Localisation du CSV
    csv_file= raw_input("Chemin vers le fichier ? ")
    if os.path.isfile(csv_file) == False:
        print "Je n'ai pas trouvé le fichier!"
        sys.exit()
    dataReader= csv.reader(open(csv_file), delimiter=",", quotechar='"')
    if "maj" in args:
        if Transporteurs.objects.count()>0:
            print "Mise à jour…"
            for row in dataReader:
                if row[0] != "nom":
                    transporteur=Transporteurs.objects.get(nom=row[0])
                    transporteur.description=row[1]
                    transporteur.courriel=row[2]
                    transporteur.cp=row[3]
                    transporteur.ville=row[4]
                    transporteur.telephone=row[5]
                    transporteur.url=row[6]
                    transporteur.save()
        else:
            print "C'est ennuyeux de mettre à jour quand la table est vide…"
    elif Transporteurs.objects.count()>0 and not "force" in args:
        print "La table des transporteurs n'est pas vide ! ajoutez « --script-args=force » pour voir"
    else:
        print 'Insertions…'
        for row in dataReader:
            if row[0] != "nom":
                transporteur=Transporteurs()
                transporteur.nom=row[0]
                transporteur.description=row[1]
                transporteur.courriel=row[2]
                transporteur.cp=row[3]
                transporteur.ville=row[4]
                transporteur.telephone=row[5]
                transporteur.url=row[6]
                transporteur.save()

