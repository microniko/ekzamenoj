# -*- coding: utf-8 -*-
# 
# Copyright (C) 2015  Nicolas Grandjean <nicolas@microniko.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
""" Importation du CSV des examens
    Usage : python manage.py runscript import_examens
    Pour forcer si la table n'est pas vide (mais si des examens existent déjà, ça peut ne pas fonctionner) :
      python manage.py runscript import_examens --script-args=force
    Pour mettre la table à jour :
      python manage.py runscript import_examens --script-args=maj
    Format du fichier :
    "nom","date_activation","date_desactivation","code_sil","secteur","methode","nature","urgence","bon","acte","delai_rendu","delai_analyse","delai_preana","recommandations","commentaire","indications","acheminement_nom","acheminement_temperature","soustraitance_soustraitant","soustraitance_transporteur","soustraitance_temperature","soustraitance_stockage","soustraitance_url","soustraitance_infos","preanalytique_centrifugation_vitesse","preanalytique_centrifugation_temperature","preanalytique_centrifugation_temps","preanalytique_aliquotage","preanalytique_stockage","preanalytique_analyseur"
"""
from ekz.models import * 
from django.core.exceptions import ObjectDoesNotExist

import sys,os, os.path
import csv

def run(*args):
    # Localisation du CSV
    csv_file= raw_input("Chemin vers le fichier ? ")
    if os.path.isfile(csv_file) == False:
        print "Je n'ai pas trouvé le fichier!"
        sys.exit()
    dataReader= csv.reader(open(csv_file), delimiter=",", quotechar='"')
    if "maj" in args:
        print "On ne peut pas faire de mise à jour en masse."
    elif Preanalytiques.objects.count()>0 and not "force" in args:
        print "La table des examens n'est pas vide ! ajoutez « --script-args=force » pour voir"
    else:
        print 'Insertions…'
        for row in dataReader:
            i=0
            while i <=29:
                if row[i] == "":
                    row[i] = None
                i = i+1
            if row[0] != 'nom':
                print "Examen : " + row[0]
                # On commence par les colonnes simples
                examen = Examens()
                examen.nom = row[0]
                if row[1] != None: examen.date_activation=row[1]
                if row[2] != None: examen.date_desactivation=row[2]
                examen.code_sil= row[3]
                examen.delai_rendu = row[10]
                examen.delai_analyse = row[11]
                examen.delai_preana = row[12]
                examen.commentaire = row[14]
                examen.indications = row[15]
                # Champs liés
                if row[4] != None:
                   secteur = Secteurs.objects.get(nom=row[4])
                   examen.secteur=secteur
                if row[5] != None:
                    methode = Methodes.objects.get(nom=row[5])
                    examen.methode=methode
                if row[6] != None:
                    nature, created = Natures.objects.get_or_create(nom=row[6])
                    examen.nature=nature
                if row[7] != None:
                    urgence = Urgences.objects.get(nom=row[7])
                    examen.urgence=urgence
                if row[8] != None:
                    bon = Bons.objects.get(nom=row[8])
                    examen.bon=bon
                if row[13] != None:
                    recom, created= Recommandations.objects.get_or_create(nom=row[13])
                    examen.recommandations=recom
                # Champs complexes
                if row[16] != None or row[17] != None:
                    #if row[17]=="":
                    #    row[17]=None
                    try:
                        acheminement = Acheminements.objects.get(nom=row[16], temperature__nom=row[17])
                    except ObjectDoesNotExist:
                        acheminement=Acheminements()
                        acheminement.nom=row[16]
                        #print row[17]
                        if row[17]!=None:
                            temperature=Temperatures.objects.get(nom=row[17])
                            acheminement.temperature=temperature
                        acheminement.save()
                    examen.acheminements=acheminement
                centri_exist = False
                preana_exist = False
                if row[24] != None or row[25] != None or row[26] != None:
                    # centri, centri_created = Centrifugations.objects.get_or_create(vitesse=row[24], temperature__nom=row[25], temps=row[26])
                    try:
                        centrifugation = Centrifugations.objects.get(vitesse=row[24], temperature__nom=row[25], temps=row[26])
                    except ObjectDoesNotExist:
                        #print "A"
                        if row[25] != None:
                            temperature = Temperatures.objects.get(nom=row[25])
                            centrifugation = Centrifugations()
                            centrifugation.vitesse=row[24]
                            centrifugation.temperature = temperature
                            centrifugation.temps=row[26]
                            centrifugation.save()
                            centri_exist = True
                    else:
                        centri_exist = True
                        #print "B"
                if row[27] != None or row[28] != None or row[29] != None:
                    if centri_exist == True:
                        try:
                            preana = Preanalytiques.objects.get(aliquotage=row[27], stockage__nom=row[28], analyseur__nom=row[29], centrifugation=centrifugation)
                        except ObjectDoesNotExist:
                            #print "C"
                            preana = Preanalytiques()
                            if row[28] != None:
                                stockage = Stockages.objects.get(nom=row[28])
                                preana.stockage=stockage
                            if row[29] != None:
                                analyseur = Analyseurs.objects.get(nom=row[29])
                                preana.analyseur=analyseur
                            preana.aliquotage=row[27]
                            preana.centrifugation=centrifugation
                            preana.save()
                            preana_exist = True
                        else:
                            #print "D"
                            preana_exist = True
                    else:
                        try:
                            preana = Preanalytiques.objects.get(aliquotage=row[27], stockage__nom=row[28], analyseur__nom=row[29])
                        except ObjectDoesNotExist:
                            #print "E"
                            preana = Preanalytiques()
                            if row[28] != None:
                                stockage = Stockages.objects.get(nom=row[28])
                                preana.stockage=stockage
                            if row[29] != None:
                                analyseur = Analyseurs.objects.get(nom=row[29])
                                preana.analyseur=analyseur
                            preana.aliquotage=row[27]
                            preana.save()
                            preana_exist = True
                        else:
                            #print "F"
                            preana_exist = True
                elif centri_exist == True:
                    preana = Preanalytiques()
                    preana.centrifugation=centri
                    preana.save()
                    preana_exist = True
                if preana_exist == True:
                    examen.preanalytique = preana
                if row[18] != None or row[19] != None or row[20] != None or row[21] != None or row[22] != None or row[23] != None:
                    try:
                        soustce = SousTraitance.objects.get(soustraitant__nom=row[18], transporteur__nom=row[19], temperature__nom=row[20], stockage__nom=row[21], url=row[22], infos=row[23])
                    except ObjectDoesNotExist:
                        soustce = SousTraitance()
                        if row[18] != None:
                            soustraitant = SousTraitants.objects.get(nom=row[18])
                            soustce.soustraitant=soustraitant
                        if row[19] != None:
                            transporteur = Transporteurs.objects.get(nom=row[19])
                            soustce.transporteur=transporteur
                        if row[20] != None:
                            temperature = Temperatures.objects.get(nom=row[20])
                            soustce.temperature=temperature
                        if row[21] != None:
                            stockage = Stockages.objects.get(nom=row[21])
                            soustce.stockage=stockage
                        soustce.url=row[22]
                        soustce.infos=row[23]
                        soustce.save()
                        examen.soustraitance=soustce
                    else:
                        examen.soustraitance=soustce
                    #print 'je suis dans la boucle Ss-traitance'
                examen.save()
                # Une fois que l'examen est ajouté en base, on peut lui associer les champs multiples
                if row[9] != None:
                    if ";" in row[9]:
                        for x in row[9].split(";"):
                            #print row[9]
                            #print x
                            code = Actes.objects.get(code=x)
                            Facturation(examen=examen, acte=code).save()
                    else:
                        code = Actes.objects.get(code=row[9])
                        Facturation(examen=examen, acte=code).save()

"""


                preanalytique=Preanalytiques()
                if row[0]  != None and row[1] != "":
                    centri=Centrifugations()
                    centri.vitesse=row[0]
                    centri.temps=row[1]
                    centri.save()
                    preanalytique.centrifugation=centri
                if row[2]  != None and row[3] != "" and row[4] != "":
                    stock=Stockages()
                    stock.nom=row[2]
                    stock.piece=row[3]
                    if row[4] != "":
                        temp= Temperatures.objects.get(nom=row[4])
                        stock.temperature =temp
                    stock.save()
                    preanalytique.stockage=stock
                preanalytique.aliquotage=row[5]
                if row[6] != "":
                    preanalytique.analyseur= Analyseurs.objects.get(nom=row[6])
                preanalytique.save()
"""
"""

                if row[24] != None or row[25] != None or row[26] != None or row[27] != None or row[28] != None or row[29] != None:
                    if row[24] != None or row[25] != None or row[26] != None:
                        print 'je suis dans la boucle Centri'
                        centri, centri_created = Centrifugations.objects.get_or_create(vitesse=row[24], temperature__nom=row[25], temps=row[26])
                    if row[27] != None or row[28] != None or row[29] != None:
                        if centri_created == True:
                            print 'je suis dans la boucle préana'
                            preana, created = Preanalytiques.objects.get_or_create(aliquotage=row[27], stockage__nom=row[28], analyseur__nom=row[29], centrifugation=centri)
                        else:
                            preana, created = Preanalytiques.objects.get_or_create(aliquotage=row[27], stockage__nom=row[28], analyseur__nom=row[29])
                    elif centri_created == True:
                            preana, created = Preanalytiques.objects.get_or_create(centrifugation=centri)
                    examen.preanalytique = preana
                    print 'je suis dans la boucle Centri/préana'
"""

