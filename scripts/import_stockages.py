# -*- coding: utf-8 -*-
# 
# Copyright (C) 2015  Nicolas Grandjean <nicolas@microniko.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
""" Importation du CSV des stockages
    Usage : python manage.py runscript import_stockages
    Utilise par défaut le format
    Pour forcer si la tables n'est pas vide (mais si des stockages existent déjà, ça ne fonctionnera pas) :
      python manage.py runscript import_stockages --script-args=force
    Pour mettre la table à jour :
      python manage.py runscript import_stockages --script-args=maj
    Format du fichier :
    "nom","temperature","piece"
"""


from ekz.models import Stockages, Temperatures 

import sys,os, os.path
import csv



def run(*args):
    # Localisation du CSV
    csv_file= raw_input("Chemin vers le fichier ? ")
    if os.path.isfile(csv_file) == False:
        print "Je n'ai pas trouvé le fichier!"
        sys.exit()
    dataReader= csv.reader(open(csv_file), delimiter=",", quotechar='"')
    if "maj" in args:
        if Stockages.objects.count()>0:
            for row in dataReader:
                if row[0] != "nom":
                    stockage=Stockages.objects.get(nom=row[0])
                    stockage.temperature=Temperatures.objects.get(nom=row[1])
                    stockage.piece=row[2]
                    stockage.save()
        else:
            print "C'est ennuyeux de mettre à jour quand la table est vide…"
    elif Stockages.objects.count()>0 and not "force" in args:
        print "La table des stockages n'est pas vide ! ajoutez « --script-args=force » pour voir"
    else:
        print 'Insertions…'
        for row in dataReader:
            if row[0] != 'nom':
                stockage=Stockages()
                stockage.nom=row[0]
                stockage.temperature=Temperatures.objects.get(nom=row[1])
                stockage.piece=row[2]
                stockage.save()


