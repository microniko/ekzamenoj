# -*- coding: utf-8 -*-
# 
# Copyright (C) 2015  Nicolas Grandjean <nicolas@microniko.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
""" Importation du CSV des utilisations des groupes de prélèvement
    Usage : python manage.py runscript import_utilisations
    Pour forcer si la tables n'est pas vide (non conseillé) :
      python manage.py runscript import_utilisations --script-args=force
    Pour mettre la table à jour :
      python manage.py runscript import_utilisations --script-args=maj
    Format du fichier :
    "examen","pourcentage","prelevement"
"""


from ekz.models import UtilisationGroupePrelevement, Examens, Prelevements

import sys,os, os.path
import csv



def run(*args):
    # Localisation du CSV
    csv_file= raw_input("Chemin vers le fichier ? ")
    if os.path.isfile(csv_file) == False:
        print "Je n'ai pas trouvé le fichier!"
        sys.exit()
    dataReader= csv.reader(open(csv_file), delimiter=",", quotechar='"')
    if "maj" in args:
        print "On ne peut pas faire de mise à jour en masse…"
    elif UtilisationGroupePrelevement.objects.count()>0 and not "force" in args:
        print "La table des utilisations n'est pas vide ! ajoutez « --script-args=force » pour voir"
    else:
        print 'Insertions…'
        for row in dataReader:
            if row[0] != 'examen':
                print 'Examen : ' + row[0]
                utilisation=UtilisationGroupePrelevement()
                examen=Examens.objects.get(nom=row[0])
                utilisation.examen=examen
                utilisation.pourcentage=row[1]
                prelevement=Prelevements.objects.get(nom=row[2])
                utilisation.prelevement=prelevement
                utilisation.save()

