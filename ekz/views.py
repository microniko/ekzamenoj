# -*- coding: utf-8 -*-
# 
# Copyright (C) 2014  Nicolas Grandjean <nicolas@microniko.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from django.shortcuts import render
from ekzamenoj import settings

# Create your views here.

from django.http import Http404
from django.shortcuts import render, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.views.generic import ListView, DetailView

from django.contrib.auth.models import Permission, User
from django.contrib.auth.decorators import permission_required
from django.utils.decorators import method_decorator

from forms import RechercheForm

#import ekz.constants

# Fusionner des résultats de plusieurs QuerySet (fonction rechercher)
from itertools import chain

# Classer le résultat de chain
from operator import attrgetter

# Librairie interne de traitement de chaînes
from tttstr import strip_diac, strip_tags_html

# Enlever les accents ==> Non utilisable :-(
# Calcul de dates
import datetime

from models import Examens, Synonymes, Prelevements,  Tubes, Bons


def Alphabet():
    """Liste des lettres de l'alphabet, seulement les lettres utilises
    dans la liste des examens. N'affiche que les examens actifs.
    Cette fonction doit être déclarée avant d'importer celles de modules externes (par exemple pages) sinon
    Django refuse d'importer Alphabet dans l'autre appli. Une explication du phénomène m'intéresse !
    """
    exam  = Examens.objects.filter(date_desactivation__gt=datetime.date.today(), date_activation__lte=datetime.date.today()).order_by('nom')
    syno = Synonymes.objects.order_by('nom')
    #l=['..','..']
    alphabet =[]
    for x in exam:
        if alphabet.count(strip_diac(strip_tags_html(x.nom)[0].upper())) == 0:
            alphabet.append(strip_diac(strip_tags_html(x.nom)[0].upper()))
    for y in syno:
            if alphabet.count(strip_diac(strip_tags_html(y.nom)[0].upper())) == 0 and (y.examen.date_desactivation > datetime.date.today() and y.examen.date_activation <= datetime.date.today() ):
                alphabet.append(strip_diac(strip_tags_html(y.nom)[0].upper()))
    alphabet.sort()
    return alphabet

# A mettre après les autres imports…
from pages.models import News
from pages.views import ListeDesPages

class ListViewPlus(ListView):
    """ Vue fille de vue générique, parente des autres vues listant des éléments.
        Permet de déclarer une seule fois pages et alphabet grâce à l'héritage """
    def get_context_data(self, **kwargs):
        context = super(ListViewPlus, self).get_context_data(**kwargs)
        context['pages'] = ListeDesPages()
        context['alphabet'] = Alphabet()
        return context


class ListeExamens(ListViewPlus):
    model = Examens
    context_object_name= "liste_examens" # ==> Ce que l'on retrouve dans le template'
    template_name = "liste_examens.html"

class ListeTubes(ListViewPlus):
    model = Tubes
    context_object_name= "liste_tubes" # ==> Ce que l'on retrouve dans le template'
    template_name = "liste_tubes.html"

class ListePrelevements(ListViewPlus):
    model = Prelevements
    context_object_name= "liste_prelevements" # ==> Ce que l'on retrouve dans le template'
    template_name = "liste_prelevements.html"

class ListeBons(ListViewPlus):
    model = Bons
    context_object_name= "liste_bons" # ==> Ce que l'on retrouve dans le template'
    template_name = "liste_bons.html"

class VoirExamen(DetailView): 
    """ Vue générique pour afficher un examen.
    Aucune solution trouvée pour gérer la la permission permettant de voir les examens inactifs.
    En attendant de faire mieux, c'est dans le gabarit (oui, on sait). 
    La nouvelle fonctionalité dans Django 1.9 https://docs.djangoproject.com/en/dev/topics/auth/default/#django.contrib.auth.mixins.AccessMixin
    ou https://docs.djangoproject.com/en/dev/topics/auth/default/#django.contrib.auth.mixins.LoginRequiredMixin
    pourrait-elle nous aidée ."""
    model = Examens
    context_object_name = "examen" # ==> Ce que l'on retrouve dans le template'
    template_name = "voir_examen.html"
    # https://docs.djangoproject.com/fr/1.7/topics/auth/default/#applying-permissions-to-generic-views
    #@permission_required('ekz.view_inactif',raise_exception=True)
    #def dispatch(self, *args, **kwargs):
    #    return super(VoirExamen, self).dispatch(*args, **kwargs)
    def get_context_data(self, **kwargs):
            context = super(VoirExamen, self).get_context_data(**kwargs)
            context['pages'] = ListeDesPages()
            context['alphabet'] = Alphabet()
            #examen = context['examen']
            #print(examen.is_actif) → pb car ne retourne pas True|False mais <bound method Examens.is_actif of <Examens: xxxxxx>> !!! 
            #print(self.request.user.has_perm('ekz.view_inactif')) → True|False (OK)
            #if (not examen.is_actif) and\
            #   (not self.request.user.has_perm('ekz.view_inactif')):
            #    raise PermissionDenied
            return context


def accueil(request):
    """La page d'accueil (index)'"""
    news = News.objects.filter(date_debut__lte = datetime.datetime.now(), date_fin__gte = datetime.datetime.now())
    pages = ListeDesPages()
    alphabet = Alphabet()
    return render(request, 'index.html', locals())


def liste_alpha_examens(request, lettre):
    """Liste des examens dans une lettre"""
    if request.user.has_perm('view_inactif'):
        examens = Examens.objects.filter(nom__regex=r'^('+lettre+'|'+lettre.lower()+')+') # Commencent par `lettre` mais avec une regexp pour pallier le pb de SQLite qui ne peut pas faire istartswith avec des caractères unicode (accents...)
        synonymes = Synonymes.objects.filter(nom__regex=r'^('+lettre+'|'+lettre.lower()+')+')
    else:
        examens = Examens.objects.filter(nom__regex=r'^('+lettre+'|'+lettre.lower()+')+', date_desactivation__gt=datetime.date.today(), date_activation__lte=datetime.date.today())
        synonymes = Synonymes.objects.filter(nom__regex=r'^('+lettre+'|'+lettre.lower()+')+', examen__date_desactivation__gt=datetime.date.today(), examen__date_activation__lte=datetime.date.today())  # Pareil avec les synonymes
    # On mélange tout ça et on tri par nom
    Tout = sorted(chain(synonymes, examens), key=attrgetter('nom'))
    pages = ListeDesPages()
    alphabet=Alphabet()
    return render(request, 'liste_alpha_examens.html', locals())


from django.views.decorators.csrf import csrf_exempt
@csrf_exempt
def recherche(request):
    ''' Moteur de recherche (basique) interne '''
    if len(request.GET) > 0:
       form = RechercheForm(request.GET)
       if form.is_valid():
          ChaineRecherche = form.cleaned_data['ChaineRecherche']
          if request.user.has_perm('view_inactif'):
             ResExamens = Examens.objects.filter(nom__icontains=ChaineRecherche).order_by('nom')
             ResSynonymes = Synonymes.objects.filter(nom__icontains=ChaineRecherche).order_by('nom')
          else:
             ResExamens = Examens.objects.filter(nom__icontains=ChaineRecherche, date_desactivation__gt=datetime.date.today(), date_activation__lte=datetime.date.today()).order_by('nom')
             ResSynonymes = Synonymes.objects.filter(nom__icontains=ChaineRecherche, examen__date_desactivation__gt=datetime.date.today(), examen__date_activation__lte=datetime.date.today()).order_by('nom')
          Resultats = sorted(chain(ResSynonymes, ResExamens), key=attrgetter('nom'))
    else:
       form = RechercheForm()
    pages = ListeDesPages()
    alphabet=Alphabet()
    return render(request, 'recherche.html', locals())
