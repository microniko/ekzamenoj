# -*- coding: utf-8 -*-
# 
# Copyright (C) 2014  Nicolas Grandjean <nicolas@microniko.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from django.contrib import admin

# Register your models here.

# Il faudra désactiver quelques vues :
# SousTraitanceAdmin

from models import Examens, Synonymes, SousTraitants, Acheminements
from models import Transporteurs, Actes, Methodes, Secteurs, Bons, Tubes
from models import Prelevements, UtilisationGroupePrelevement, SousTraitance, Temperatures
from models import Fichiers, Preanalytiques, Centrifugations, Lettres, Stockages
from models import Recommandations, Urgences, Natures
from models import Analyseurs
from models import Facturation

from django_summernote.admin import SummernoteModelAdmin
import datetime

# Import/export
#from import_export import resources
#from import_export.admin import ImportExportModelAdmin

#class ActesResource(resources.ModelResource):
#    model = Actes

#class ExamensResource(resources.ModelResource):
#    model = Examens

# Suite


class FacturationInline(admin.StackedInline):
    ''' Permet d'ajouter la facturation directement à la création d'un examen '''
    model = Facturation
    raw_id_fields = ("acte",)
    extra = 1 # Nombre de champs à afficher...
    verbose_name=u"Ajoutez autant d'actes que nécessaires"

class ExamensAdmin(SummernoteModelAdmin):
    """ Administration pour les examens """
    fieldsets = (
                  (u"Informations générales", {'fields': ('nom', ('date_activation', 'date_desactivation')) }),
                  (u"Prélèvement", {'fields' : ('nature','bon', 'recommandations', 'acheminements', 'fichier') }),
                  (u"Préanalytique", {'fields' : ('preanalytique','soustraitance') }),
                  (u"Analytique", {'fields' : ('code_sil','secteur','methode', 'urgence', ('delai_rendu', 'delai_recept', 'delai_preana') ) }),
                  (u"Informations complémentaires", {'fields' : ('commentaire','indications') }),
                )
    inlines = (FacturationInline,) # liaison m2m avec table intermédiaire
    list_display = ('nom', 'code_sil', 'nature', 'secteur', 'date_activation', 'date_desactivation')
    list_filter = ('secteur', 'date_desactivation', 'date_activation')
    date_hierarchy = 'date_modif'
    ordering = ('nom',)
    search_field = ('nom', 'description', 'code_sil', 'synonymes')
    filter_horizontal = ("fichier",)
    search_fields = ['nom']
    actions = [
        'make_activ_today', 'make_activ_tom',
        'make_desactiv_today', 'make_desactiv_tom',
    ]

    def make_activ_today(self,request, queryset):
        queryset.update(date_activation=datetime.datetime.now())
    make_activ_today.short_description = u"Fixer la date d'activation à aujourd'hui"
    def make_activ_tom(self,request, queryset):
        queryset.update(date_activation=datetime.datetime.now()+datetime.timedelta(days=1))
    make_activ_tom.short_description = u"Fixer la date d'activation à demain"

    def make_desactiv_today(self,request, queryset):
        queryset.update(date_desactivation=datetime.datetime.now())
    make_desactiv_today.short_description = u"Fixer la date de désactivation à aujourd'hui"

    def make_desactiv_tom(self,request, queryset):
        queryset.update(date_desactivation=datetime.datetime.now()+datetime.timedelta(days=1))
    make_desactiv_tom.short_description = u"Fixer la date de désactivation à demain"

#class ExamensAdmin(ImportExportModelAdmin):
#   resource_class = ExamensResource
#   pass


class SynonymesAdmin(admin.ModelAdmin):
    """Administration des synonymes"""
    list_display = ('nom', 'examen')
    list_filter = ('nom', 'examen')
    date_hierarchy = 'date_modif'
    ordering = ('nom',)
    search_fields = ['nom', 'examen']


class SousTraitantsAdmin(admin.ModelAdmin):
    """Administration des sous-traitants"""
    view_on_site = True
    list_display = ('nom','description', 'ville', 'voir')
    list_filter = ('ville',)
    date_hierarchy = 'date_modif'
    ordering = ('nom',)
    search_fields = ['nom']

    def voir(self, instance):
        #return "<a href=\""+str(instance.id)+"\">Voir</a>"
        return instance.get_absolute_url()
    voir.short_description="Sur le site public"

class TransporteursAdmin(admin.ModelAdmin):
    """Administration des transporteurs"""
    list_display = ('nom','description', 'ville')
    list_filter = ('ville',)
    date_hierarchy = 'date_modif'
    ordering = ('nom',)
    search_fields = ['nom']


class AcheminementsAdmin(admin.ModelAdmin):
    """Administration des modalités d'acheminement"""
    list_display = ('nom',)
    list_filter = ()
    date_hierarchy = 'date_modif'
    ordering = ('nom',)
    search_fields = ['nom']


class ActesAdmin(admin.ModelAdmin):
    """Administration des Actes"""
    list_display = ('code', 'libelle')
    list_filter = ()
    date_hierarchy = 'date_modif'
    ordering = ('code',)
    search_fields = ['code', 'libelle']

class MethodesAdmin(admin.ModelAdmin):
    """Administration des Méthodes"""
    list_display = ('nom', 'description')
    list_filter = ()
    date_hierarchy = 'date_modif'
    ordering = ('nom',)
    search_fields = ['nom']


class SecteursAdmin(admin.ModelAdmin):
    """Administration des Secteurs"""
    list_display = ('nom','description')
    list_filter = ()
    date_hierarchy = 'date_modif'
    ordering = ('nom',)
    search_fields = ['nom']


class BonsAdmin(admin.ModelAdmin):
    """Administration des Bons de demande"""
    list_display = ('nom',)
    list_filter = ()
    date_hierarchy = 'date_modif'
    ordering = ('nom',)
    search_fields = ['nom']


class TubesAdmin(admin.ModelAdmin):
    """Administration des Tubes ou flacons"""
    list_display = ('nom','ordre')
    list_filter = ()
    date_hierarchy = 'date_modif'
    ordering = ('ordre',)
    search_fields = ['nom']


class FichiersAdmin(admin.ModelAdmin):
    """Administration des fichiers"""
    list_display = ('nom','description','fichier')
    list_filter = ('nom',)
    date_hierarchy = 'date_modif'
    ordering = ('nom',)
    search_fields = ['nom', 'description']


class PrelevementsAdmin(admin.ModelAdmin):
    """Administration des Prélèvements"""
    list_display = ('nom','nombre','tube')
    list_filter = ()
    date_hierarchy = 'date_modif'
    ordering = ('nom',)
    search_fields = ['examen', 'tube']


class UtilisationGroupePrelevementAdmin(admin.ModelAdmin):
    """Administration des utilisations des groupes de prélèvements"""
    list_display = ('id','examen','pourcentage','prelevement',)
    list_filter  = ('examen','prelevement')
    #date_hierarchy = 'date_modif'
    ordering = ('id',)
    search_fields = ('prelevement', 'examen')

class TemperaturesAdmin(admin.ModelAdmin):
    """Administration des températures"""
    list_display = ('nom','description')
    list_filter  = ()
    date_hierarchy = 'date_modif'
    ordering = ('nom',)
    search_fields = ('nom', 'description')

class StockagesAdmin(admin.ModelAdmin):
    """Administration des lieux de stockages"""
    list_display = ('nom','piece', 'temperature')
    list_filter  = ()
    date_hierarchy = 'date_modif'
    orderings = ('nom',)
    search_field = ('nom', 'piece', 'temperature')

class PreanalytiquesAdmin(admin.ModelAdmin):
    """Administration des conditions préanalytiques"""
    list_display = ('centrifugation','aliquotage','stockage', 'analyseur')
    list_filter  = ('analyseur', 'stockage')
    date_hierarchy = 'date_modif'
    ordering = ('centrifugation',)
    search_fields = ('centrifugation','aliquotage','stockage')

class CentrifugationsAdmin(admin.ModelAdmin):
    """Administration des conditions de centrifugation"""
    list_display = ('vitesse','temps')
    list_filter  = ()
    date_hierarchy = 'date_modif'
    ordering = ('vitesse',)
    search_fields = ('vitesse', 'temps',)

class LettresAdmin(admin.ModelAdmin):
    """Administration des lettres pour la facturation"""
    list_display = ('lettre','prix')
    list_filter  = ()
    date_hierarchy = 'date_modif'
    ordering = ('lettre',)

class SousTraitanceAdmin(admin.ModelAdmin):
    """Administration de la sous-traitance"""
    list_display = ('soustraitant','transporteur','temperature', 'stockage','url')
    list_filter  = ('soustraitant','transporteur', 'stockage')
    date_hierarchy = 'date_modif'
    ordering = ('soustraitant',)
    search_fields = ('soustraitant', 'temperature', 'transporteur')

class UrgencesAdmin(admin.ModelAdmin):
    """Administration des niveaux d'urgence"""
    list_display = ('nom', 'classe')
    list_filter  = ('classe',)

class RecommandationsAdmin(admin.ModelAdmin):
    """Administration des recommandations pour le prélèvement"""
    list_display = ('nom', 'classe')
    list_filter  = ('classe',)

class NaturesAdmin(admin.ModelAdmin):
    """Administration des natures de prélèvement"""

class AnalyseursAdmin(admin.ModelAdmin):
    """Administration des analyseurs"""
    list_display = ('nom', 'description')

class FacturationAdmin(admin.ModelAdmin):
    """Administration de la facturation"""
    raw_id_fields = ("acte",)


admin.site.register(Examens, ExamensAdmin)
admin.site.register(Synonymes, SynonymesAdmin)
admin.site.register(SousTraitants, SousTraitantsAdmin)
admin.site.register(Transporteurs, TransporteursAdmin)
admin.site.register(Acheminements, AcheminementsAdmin)
admin.site.register(Actes, ActesAdmin)
admin.site.register(Methodes, MethodesAdmin)
admin.site.register(Secteurs, SecteursAdmin)
admin.site.register(Fichiers, FichiersAdmin)
admin.site.register(Bons, BonsAdmin)
admin.site.register(Tubes, TubesAdmin)
admin.site.register(Prelevements, PrelevementsAdmin)
admin.site.register(UtilisationGroupePrelevement, UtilisationGroupePrelevementAdmin)
admin.site.register(Temperatures, TemperaturesAdmin)
admin.site.register(Stockages, StockagesAdmin)
admin.site.register(SousTraitance, SousTraitanceAdmin)
admin.site.register(Preanalytiques, PreanalytiquesAdmin)
admin.site.register(Centrifugations, CentrifugationsAdmin)
admin.site.register(Lettres, LettresAdmin)
admin.site.register(Urgences,UrgencesAdmin)
admin.site.register(Recommandations,RecommandationsAdmin)
admin.site.register(Natures,NaturesAdmin)
admin.site.register(Analyseurs, AnalyseursAdmin)
admin.site.register(Facturation, FacturationAdmin)
