# -*- coding: utf-8 -*-
# 
# Copyright (C) 2014  Nicolas Grandjean <nicolas@microniko.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from django.contrib import admin

# Register your ekz.models here.

# Il faudra désactiver quelques vues :
# SousTraitanceAdmin

from ekz.models import Examens, Synonymes, SousTraitants, Acheminements
from ekz.models import Transporteurs, Actes, Methodes, Secteurs, Bons, Tubes
from ekz.models import Prelevements, UtilisationGroupePrelevement, SousTraitance, Temperatures
from ekz.models import Fichiers, Preanalytiques, Centrifugations, Lettres, Stockages
from ekz.models import Recommandations, Urgences, Natures
from ekz.models import Analyseurs
from ekz.models import Facturation
from ekz.models import Bilans

from django_summernote.admin import SummernoteModelAdmin
import datetime


class FacturationInline(admin.StackedInline):
    ''' Permet d'ajouter la facturation directement à la création d'un examen '''
    model = Facturation
    raw_id_fields = ("acte",)
    extra = 1 # Nombre de champs à afficher...
    verbose_name=u"Ajoutez autant d'actes que nécessaire"

class SynonymesInline(admin.StackedInline):
    ''' Permet d'ajouter les synonymes directement à la création d'un examen '''
    model = Synonymes
    extra = 1 # Nombre de champs supplémentaires à afficher...
    verbose_name=u"Synonyme"
    verbose_name_plural=u"Synonymes"

class ExamensAdmin(SummernoteModelAdmin):
    """ Administration pour les examens """
    view_on_site = True
    save_on_top = True
    save_as = True
    fieldsets = (
                  (u"Informations générales", {'fields': ('nom', ('date_activation', 'date_desactivation')) }),
                  (u"Prélèvement", {'fields' : ('nature','bon', 'recommandations', 'acheminements', 'fichier') }),
                  (u"Préanalytique", {'fields' : ('preanalytique','soustraitance') }),
                  (u"Analytique", {'fields' : ('code_sil','secteur','methode', 'urgence', ('delai_rendu', 'delai_recept', 'delai_preana') ) }),
                  (u"Informations complémentaires", {'fields' : ('commentaire','indications') }),
                )
    inlines = (FacturationInline,SynonymesInline) # liaison m2m avec table intermédiaire
    list_display = ('nom', 'code_sil', 'nature', 'secteur', 'date_activation', 'date_desactivation')
    list_filter = ('secteur', 'date_desactivation', 'date_activation')
    date_hierarchy = 'date_modif'
    ordering = ('nom',)
    search_field = ('nom', 'description', 'code_sil', 'synonymes')
    filter_horizontal = ("fichier",)
    search_fields = ['nom']
    actions = [
        'make_activ_today', 'make_activ_tom',
        'make_desactiv_today', 'make_desactiv_tom',
    ]
    def make_activ_today(self,request, queryset):
        queryset.update(date_activation=datetime.datetime.now())
    make_activ_today.short_description = u"Fixer la date d'activation à aujourd'hui"

    def make_activ_tom(self,request, queryset):
        queryset.update(date_activation=datetime.datetime.now()+datetime.timedelta(days=1))
    make_activ_tom.short_description = u"Fixer la date d'activation à demain"

    def make_desactiv_today(self,request, queryset):
        queryset.update(date_desactivation=datetime.datetime.now())
    make_desactiv_today.short_description = u"Fixer la date de désactivation à aujourd'hui"

    def make_desactiv_tom(self,request, queryset):
        queryset.update(date_desactivation=datetime.datetime.now()+datetime.timedelta(days=1))
    make_desactiv_tom.short_description = u"Fixer la date de désactivation à demain"

admin.site.register(Examens, ExamensAdmin)

class SynonymesAdmin(admin.ModelAdmin):
    """Administration des synonymes"""
    list_display = ('nom', 'examen')
    list_filter = ('nom', 'examen')
    date_hierarchy = 'date_modif'
    ordering = ('nom',)
    search_fields = ['nom', 'examen']

admin.site.register(Synonymes, SynonymesAdmin)

class SousTraitantsAdmin(admin.ModelAdmin):
    """Administration des sous-traitants"""
    list_display = ('nom','description', 'ville')
    list_filter = ('ville',)
    date_hierarchy = 'date_modif'
    ordering = ('nom',)
    search_fields = ['nom']

admin.site.register(SousTraitants, SousTraitantsAdmin)

class TransporteursAdmin(admin.ModelAdmin):
    """Administration des transporteurs"""
    list_display = ('nom','description', 'ville')
    list_filter = ('ville',)
    date_hierarchy = 'date_modif'
    ordering = ('nom',)
    search_fields = ['nom']

admin.site.register(Transporteurs, TransporteursAdmin)

class AcheminementsAdmin(admin.ModelAdmin):
    """Administration des modalités d'acheminement"""
    list_display = ('nom',)
    list_filter = ()
    date_hierarchy = 'date_modif'
    ordering = ('nom',)
    search_fields = ['nom']

admin.site.register(Acheminements, AcheminementsAdmin)

class ActesAdmin(admin.ModelAdmin):
    """Administration des Actes"""
    list_display = ('code', 'libelle')
    list_filter = ()
    date_hierarchy = 'date_modif'
    ordering = ('code',)
    search_fields = ['code', 'libelle']

admin.site.register(Actes, ActesAdmin)

class MethodesAdmin(admin.ModelAdmin):
    """Administration des Méthodes"""
    list_display = ('nom', 'description')
    list_filter = ()
    date_hierarchy = 'date_modif'
    ordering = ('nom',)
    search_fields = ['nom']

admin.site.register(Methodes, MethodesAdmin)

class SecteursAdmin(admin.ModelAdmin):
    """Administration des Secteurs"""
    list_display = ('nom','description')
    list_filter = ()
    date_hierarchy = 'date_modif'
    ordering = ('nom',)
    search_fields = ['nom']

admin.site.register(Secteurs, SecteursAdmin)

class BonsAdmin(admin.ModelAdmin):
    """Administration des Bons de demande"""
    list_display = ('nom',)
    list_filter = ()
    date_hierarchy = 'date_modif'
    ordering = ('nom',)
    search_fields = ['nom']

admin.site.register(Bons, BonsAdmin)

class TubesAdmin(admin.ModelAdmin):
    """Administration des Tubes ou flacons"""
    list_display = ('nom','ordre')
    list_filter = ()
    date_hierarchy = 'date_modif'
    ordering = ('ordre',)
    search_fields = ['nom']

admin.site.register(Tubes, TubesAdmin)

class FichiersAdmin(admin.ModelAdmin):
    """Administration des fichiers"""
    list_display = ('nom','description','fichier')
    list_filter = ('nom',)
    date_hierarchy = 'date_modif'
    ordering = ('nom',)
    search_fields = ['nom', 'description']

admin.site.register(Fichiers, FichiersAdmin)

class PrelevementsAdmin(admin.ModelAdmin):
    """Administration des Prélèvements"""
    list_display = ('nom','nombre','tube')
    list_filter = ()
    date_hierarchy = 'date_modif'
    ordering = ('nom',)
    search_fields = ['examen', 'tube']

admin.site.register(Prelevements, PrelevementsAdmin)

class UtilisationGroupePrelevementAdmin(admin.ModelAdmin):
    """Administration des utilisations des groupes de prélèvements"""
    list_display = ('id','examen','pourcentage','prelevement',)
    list_filter  = ('examen','prelevement')
    ordering = ('id',)
    search_fields = ('prelevement', 'examen')

admin.site.register(UtilisationGroupePrelevement, UtilisationGroupePrelevementAdmin)

class TemperaturesAdmin(admin.ModelAdmin):
    """Administration des températures"""
    list_display = ('nom','description')
    list_filter  = ()
    date_hierarchy = 'date_modif'
    ordering = ('nom',)
    search_fields = ('nom', 'description')

admin.site.register(Temperatures, TemperaturesAdmin)

class StockagesAdmin(admin.ModelAdmin):
    """Administration des lieux de stockages"""
    list_display = ('nom','piece', 'temperature')
    list_filter  = ()
    date_hierarchy = 'date_modif'
    orderings = ('nom',)
    search_field = ('nom', 'piece', 'temperature')

admin.site.register(Stockages, StockagesAdmin)

class PreanalytiquesAdmin(admin.ModelAdmin):
    """Administration des conditions préanalytiques"""
    list_display = ('centrifugation','aliquotage','stockage', 'analyseur')
    list_filter  = ('analyseur', 'stockage')
    date_hierarchy = 'date_modif'
    ordering = ('centrifugation',)
    search_fields = ('centrifugation','aliquotage','stockage')

admin.site.register(Preanalytiques, PreanalytiquesAdmin)

class CentrifugationsAdmin(admin.ModelAdmin):
    """Administration des conditions de centrifugation"""
    list_display = ('vitesse','temps')
    list_filter  = ()
    date_hierarchy = 'date_modif'
    ordering = ('vitesse',)
    search_fields = ('vitesse', 'temps',)

admin.site.register(Centrifugations, CentrifugationsAdmin)

class LettresAdmin(admin.ModelAdmin):
    """Administration des lettres pour la facturation"""
    list_display = ('lettre','prix')
    list_filter  = ()
    date_hierarchy = 'date_modif'
    ordering = ('lettre',)

admin.site.register(Lettres, LettresAdmin)

class SousTraitanceAdmin(admin.ModelAdmin):
    """Administration de la sous-traitance"""
    list_display = ('soustraitant','transporteur','temperature', 'stockage','url')
    list_filter  = ('soustraitant','transporteur', 'stockage')
    date_hierarchy = 'date_modif'
    ordering = ('soustraitant',)
    search_fields = ('soustraitant', 'temperature', 'transporteur')

admin.site.register(SousTraitance, SousTraitanceAdmin)

class UrgencesAdmin(admin.ModelAdmin):
    """Administration des niveaux d'urgence"""
    list_display = ('nom', 'classe')
    list_filter  = ('classe',)

admin.site.register(Urgences,UrgencesAdmin)

class RecommandationsAdmin(admin.ModelAdmin):
    """Administration des recommandations pour le prélèvement"""
    list_display = ('nom', 'classe')
    list_filter  = ('classe',)

admin.site.register(Recommandations,RecommandationsAdmin)

class NaturesAdmin(admin.ModelAdmin):
    """Administration des natures de prélèvement"""

admin.site.register(Natures,NaturesAdmin)

class AnalyseursAdmin(admin.ModelAdmin):
    """Administration des analyseurs"""
    list_display = ('nom', 'description')

admin.site.register(Analyseurs, AnalyseursAdmin)

class FacturationAdmin(admin.ModelAdmin):
    """Administration de la facturation"""
    raw_id_fields = ("acte",)

admin.site.register(Facturation, FacturationAdmin)

class BilansAdmin(admin.ModelAdmin):
    """ Administration des bilans """
    filter_horizontal = ("examen",)

admin.site.register(Bilans, BilansAdmin)

