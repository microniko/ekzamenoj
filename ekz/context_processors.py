# -*- coding: utf-8 -*-
# 
# Copyright (C) 2014  Nicolas Grandjean <nicolas@microniko.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# ----------------------------
# Variables de configuration
# Grâce à Chris Kief (http://chriskief.com/2013/09/19/access-django-constants-from-settings-py-in-a-template/)
# ----------------------------

from ekzamenoj import configlocale

def configuration(request):
    '''Fonction retournant l'ensemble du contenu de la configuration locale
    (dans configlocale.py : CONFIG_LOCALE) dans un dictionnaire de façon
     à ce que l'ensemble des valeurs soient directement disponible sans avoir
    besoin de les déclarer à chaque fois ici
    Cette fonction permet de rendre les valeurs disponibles dans les templates'''
    foo = dict()   # Déclaration du dictionnaire
    for nom, valeur in configlocale.CONFIG_LOCALE.items():   # Pour chaque valeurs de CONFIG_LOCALE
        foo[nom]=valeur   # On ajoute la valeur au dictionnaire
    return foo     # Et on retourne notre dico

# ----------------------------
# Version de Git
# https://mespotesgeek.fr/fr/django-template-variable-debug-et-git-version/
# ----------------------------

from django.conf import settings
import subprocess

def informations(request):
    command = 'cd ' + settings.BASE_DIR + ';'
    command += 'git describe --tags;'
    p = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    version_out, err = p.communicate()
    if request.user.is_staff:
        apps = settings.INSTALLED_APPS
        python, err =subprocess.Popen('python --version', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT).communicate()
        #django, err =subprocess.Popen('cd ' + settings.BASE_DIR + '; python manage.py --version', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT).communicate()
        pip, err =subprocess.Popen('pip freeze', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT).communicate()
        return {'version': version_out, 'apps': apps, 'python': python, 'pip': pip}
    else:
        return {'version': version_out}

