# -*- coding: utf-8 -*-
# 
# Copyright (C) 2014  Nicolas Grandjean <nicolas@microniko.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# ----------------------------
# Variables de configuration
# Grâce à Chris Kief (http://chriskief.com/2013/09/19/access-django-constants-from-settings-py-in-a-template/)
# ----------------------------

from ekzamenoj import localsettings

def configuration(request):
    '''Fonction retournant l'ensemble du contenu de la configuration locale
    (dans localsettings.py : CONFIG_LOCALE) dans un dictionnaire de façon
     à ce que l'ensemble des valeurs soient directement disponible sans avoir
    besoin de les déclarer à chaque fois ici
    Cette fonction permet de rendre les valeurs disponibles dans les templates'''
    foo = dict()   # Déclaration du dictionnaire
    for nom, valeur in localsettings.CONFIG_LOCALE.items():   # Pour chaque valeurs de CONFIG_LOCALE
        foo[nom]=valeur   # On ajoute la valeur au dictionnaire
    return foo     # Et on retourne notre dico
