# -*- coding: utf-8 -*-
# 
# Copyright (C) 2014  Nicolas Grandjean <nicolas@microniko.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

''' Quelques tags pour les templates en plus'''

# Permet de...
from django import template
register = template.Library()

@register.simple_tag
def get_verbose_name(object):
    '''Retourne le verbose_name d'un modèle
    p. ex : {% get_verbose_name examen.secteur %}
            {% get_verbose_name examen %}
    '''
    return object._meta.verbose_name

@register.simple_tag
def get_verbose_field_name(instance, field_name):
    ''' Retourne le verbose_name d'un champ dans un modèle
        p. ex : {% get_verbose_field_name examen.secteur "telephone" %}
                {% get_verbose_field_name examen "code_sil" %}
        '''
    return instance._meta.get_field(field_name).verbose_name



