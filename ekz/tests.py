# -*- coding: utf-8 -*-
# 
# Copyright (C) 2014  Nicolas Grandjean <nicolas@microniko.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.test import TestCase, Client

# Create your tests here.

from models import *


def lipsum(long=10):
   """ Une bête fonction pour retourner du Lorem Ipsum facilement dans une longueure prédéfinie"""
   lip=u"Lorem ipsum dolor sit amet, consecteteur adipiscing. Duis. Mi, placerat. Integer at iaculis quam etiam volutpat ante, senectus maecenas primis semper fames ligula ad nisl mattis sit. Eros ve nulla praesent cras dis eni. Diam duis blandit ac etiam litora. Enim. Varius. Mus class euismod sociosqu velit. Iaculis libero. Pharetra, euismod metus orci. Amet eni vitae vel diam arcu massa ve sollicitudin rhoncus sem."
   #print(len(lip))
   return lip[0:long]

class StatusTest(TestCase):
    """ Classe permettant de tester les URLs """
    def setUp(self):
        self.client = Client()

    def test_public(self):
        """ Test des adresses publiques 
        FIXME → il faudra prendre en compte la variable permettant de limiter l'accès à certaines pages dans la config 
              → Il manque des URL avec des id → /examen/<id>
                                              → /fichiers/<path>
                                              → /lettre_<lettre>/
                                              → /page/<pk>
                                              → /prescription/<id>
                                              → /prescriptions/
                                              → /recherche/
              → AttributeError: 'TemplateResponse' object has no attribute 'template' (commenté) """
        urls = [
                {'url':'/aide_au_prelevement/',
                 'template': 'ekz/liste_prelevements.html',
                 'status': 200
                },
                {'url':'/formulaires_de_prescription/',
                 'template': 'ekz/liste_bons.html',
                 'status': 200
                },
                {'url':'/login/',
                 'template': 'ekz/login.html',
                 'status': 200
                },
                {'url':'/recherche/',
                 'template': 'ekz/recherche.html',
                 'status': 200
                },
                {'url':'/recueils/',
                 'template': 'ekz/liste_tubes.html',
                 'status': 200
                },
                #{'url':'/...',
                # 'template': '....html',
                # 'status': 200
                #},
               ]
        for elem in urls:
            reponse = self.client.get(elem['url'])
            self.assertEqual(reponse.status_code, elem['status']) # On vérifie que l'on nous répond bien le bon code
            # → FIXME ne fonctionne pas !
            #reponse = self.client.get(elem['url'], follow=True)
            #self.assertEqual(reponse.template.name, elem['template']) # on vérifie que l'on est bien redirigé vers le template

#

class TestPrelevements(TestCase):
    """ Une classe globale pour tester tout ce qui touche un prélèvement.
    Ce n'est peut-être pas très judicieux mais j'ai choisi de la séparer des tests sur les examens
    ce qui impose de créer une définir une fonction pour tester la création d'un examen... """
    def test_tube_create(self):
        """Pour tester la création d'un tube """
        tube = Tubes(nom=u"Tube de test", ordre=250, image="image_test.png")
        tube.save()
        # On vérifie que le tube est bien créé
        tb = Tubes.objects.get(ordre=250)
        self.assertEqual(tb.nom, u"Tube de test")

    def test_examen_create_dans_prel(self):
        """ Pour tester la création d'un examen basique car seulement pour tester les prélèvements"""
        exam = Examens(nom=u"Examen pour tester les prélèvements")
        exam.save()
        # Vérification
        ex = Examens.objects.get(id=1)
        self.assertEqual(ex.nom,u"Examen pour tester les prélèvements")

    def test_prel_create(self):
        """ Pour tester la création des prélèvements sans mettre d'examen """
        self.test_tube_create()
        t = Tubes.objects.get(ordre=250)
        prl = Prelevements(nom=u"Prélèvement test", nombre=25, tube=t)
        prl.save()
        # On vérifie
        prel = Prelevements.objects.get(nom=u"Prélèvement test")
        self.assertEqual(prel.nombre,25)

    def test_utilisation_gpr_prel(self):
        """ Pour tester la création d'une utilisation de groupe de prélèvement : càd un prélèvement en mettant un examen """
        self.test_examen_create_dans_prel()
        exam = Examens.objects.get(id=1)
        self.test_prel_create()
        prl = Prelevements.objects.get(nom=u"Prélèvement test")
        ugp = UtilisationGroupePrelevement(pourcentage=15, prelevement=prl, examen=exam)
        ugp.save()
        # On vérifie
        util = UtilisationGroupePrelevement.objects.get(id=1)
        self.assertEqual(util.examen.nom,u"Examen pour tester les prélèvements")
        self.assertEqual(util.pourcentage,15)
        self.assertEqual(util.prelevement.nombre,25)

class TestFacturation(TestCase):
    """ Une classe globale pour tester tout ce qui à un rapport avec la facturation. De même que pour les
    prélèvements, on propose un test de création d'examen basique utilisé dans les autres tests. """
    def test_lettre_create(self):
        """ Pour tester la création d'une lettre """
        lt = Lettres(lettre=u"€", prix=1)
        lt.save()
        # Vérification
        let = Lettres.objects.get(prix=1)
        self.assertEqual(let.lettre,u"€")

    def test_examen_create_dans_fac(self):
        """ Pour tester la création d'un examen basique car seulement pour tester la facturation"""
        exam = Examens(nom=u"Examen pour tester la facturation")
        exam.save()
        # Vérification
        ex = Examens.objects.get(id=1)
        self.assertEqual(ex.nom,u"Examen pour tester la facturation")

    def test_acte_create(self):
        """ Pour tester la création d'un acte, on récupère la lettre concernée. """
        self.test_lettre_create()
        lt= Lettres.objects.get(prix=1)
        act = Actes(code=u"X256F", libelle=u"Un acte juste pour tester", nombre=23, lettre=lt)
        act.save()
        # Vérification
        a = Actes.objects.get(code=u"X256F")
        self.assertEqual(a.libelle,u"Un acte juste pour tester")
        self.assertEqual(a.nombre,23)
        self.assertEqual(a.lettre.lettre,u"€")
        self.assertEqual(a.lettre.prix,1)

    def test_facturation_create(self):
        """ Pour tester la facturation des examens. """
        self.test_examen_create_dans_fac()
        exam = Examens.objects.get(id=1)
        self.test_acte_create()
        act = Actes.objects.get(code=u"X256F")
        fac = Facturation(examen=exam, acte=act)
        fac.save()
        # Vérificatoions
        factu = Facturation.objects.get(examen=1)
        self.assertEqual(factu.examen.nom,u"Examen pour tester la facturation")
        self.assertEqual(factu.acte.libelle,u"Un acte juste pour tester")
        self.assertEqual(factu.acte.nombre,23)
        self.assertEqual(factu.acte.lettre.lettre,u"€")



class TestSoustraitance(TestCase):
    """ Classe globale pour tester tout ce qui est lié à la sous-traitance ainsi qu'aux étapes préanalytiques (les modèles étant très liés)
    FIXME → IL faut tester la création d'un examen dans tout ça !"""
    def test_sstt_create(self):
      """Pour tester la création d'un sous-traitant """
      sstt = SousTraitants(nom = u"Sous Traitant pour tester", description=lipsum(250), ville=u"Ville de test", cp=u"33500", telephone=u"+33 1 02 03 55 22", courriel=u"test@perdu.com", url="www.perdu.com")
      sstt.save()
      # Vérification
      ssttant = SousTraitants.objects.get(nom=u"Sous Traitant pour tester")
      self.assertEqual(ssttant.cp, u"33500")
      self.assertEqual(ssttant.description, lipsum(250))
      self.assertEqual(ssttant.ville, u"Ville de test")
      self.assertEqual(ssttant.telephone, u"+33 1 02 03 55 22")
      self.assertEqual(ssttant.courriel, u"test@perdu.com")
      self.assertEqual(ssttant.url, u"www.perdu.com")


    def test_transporteur_create(self):
      """Pour tester la création d'un transporteur (proche du sous-traitant : mêmes champs """
      transpt = Transporteurs(nom = u"Transporteur pour tester", description=lipsum(250), ville=u"Ville de test", cp=u"33500", telephone=u"+33 1 02 03 55 22", courriel=u"test@perdu.com", url="www.perdu.com")
      transpt.save()
      # Vérification
      tpt = Transporteurs.objects.get(nom=u"Transporteur pour tester")
      self.assertEqual(tpt.cp, u"33500")
      self.assertEqual(tpt.description, lipsum(250))
      self.assertEqual(tpt.ville, u"Ville de test")
      self.assertEqual(tpt.telephone, u"+33 1 02 03 55 22")
      self.assertEqual(tpt.courriel, u"test@perdu.com")
      self.assertEqual(tpt.url, u"www.perdu.com")

    def test_ssttce_create(self):
      """ Pour tester la création d'une sous-traitance Penser à intégrer la température et le stockage"""
      self.test_transporteur_create()
      self.test_sstt_create()
      tsp = Transporteurs.objects.get(nom=u"Transporteur pour tester")
      sstt= SousTraitants.objects.get(nom=u"Sous Traitant pour tester")
      sstce = SousTraitance(soustraitant=sstt, transporteur=tsp, url="www.perdu.com", infos=u"<h1>" + lipsum(15) + "<h1><p>" + lipsum(300) + "</p>")
      sstce.save()
      # Vérification
      test= SousTraitance.objects.get(soustraitant=sstt, transporteur=tsp)
      self.assertEqual(test.url, "www.perdu.com")

    def test_centrif_create(self):
      """ Tester la création d'une centrifugation """
      centrif = Centrifugations(vitesse=u"350 tours/min", temps=u"35min")
      centrif.save()
      # Vérification
      centri = Centrifugations.objects.get(vitesse=u"350 tours/min")
      self.assertEqual(centri.temps, u"35min")

    def test_temp_create(self):
        """ Teste de la création de la température """
        t = Temperatures(nom=u"Température de test", description=lipsum(50))
        t.save()
        # Vérification
        temp = Temperatures.objects.get(nom=u"Température de test")
        self.assertEqual(temp.description, lipsum(50))

    def test_stock_create(self):
        """ Test la création du stockage """
        self.test_temp_create()
        temp = Temperatures.objects.get(nom=u"Température de test")
        stk = Stockages(nom=u"Réfrigérateur numéro 2750", piece=u"Pièce 350", temperature=temp)
        stk.save()
        # Vérification
        stock = Stockages.objects.get(nom=u"Réfrigérateur numéro 2750")
        self.assertEqual(stock.piece, u"Pièce 350")

    def test_acheminement_create(self):
        """ test de la création d'un acheminement (lié à une température -ou pas) """
        self.test_temp_create()
        temp = Temperatures.objects.get(nom=u"Température de test")
        # D'abord avec une température
        ach1 = Acheminements(nom=u"Une température avec une température", temperature=temp)
        ach1.save()
        # Puis sans température
        ach2 = Acheminements(nom=u"Une température sans température")
        ach2.save()
        # Vérifications
        achem1 = Acheminements.objects.get(nom=u"Une température avec une température")
        self.assertEqual(achem1.id,1)
        achem2 = Acheminements.objects.get(nom=u"Une température sans température")
        self.assertEqual(achem2.id,2)


    def test_analyseur_create(self):
        """ Test de la création d'un analyseur"""
        a = Analyseurs(nom=u"Analyseur de test", image=u"automate.png", description=lipsum(255))
        a.save()
        # Vérification
        analy = Analyseurs.objects.get(nom=u"Analyseur de test")
        self.assertEqual(analy.image, u"automate.png")

    def test_preana_create(self):
        """ Pour tester la création d'une condition préana"""
        self.test_centrif_create()
        centri = Centrifugations.objects.get(vitesse=u"350 tours/min")
        self.test_stock_create()
        stock = Stockages.objects.get(nom=u"Réfrigérateur numéro 2750")
        self.test_analyseur_create()
        analy = Analyseurs.objects.get(nom=u"Analyseur de test")
        preana = Preanalytiques(centrifugation=centri, stockage=stock, aliquotage=u"Texte libre dans l'aliquotage", analyseur=analy)
        preana.save()
        # Vérification
        pre = Preanalytiques.objects.get(stockage=stock)
        self.assertEqual(pre.aliquotage,u"Texte libre dans l'aliquotage")


class TestExamens(TestCase):
    """ Classe générique pour tester les examens et ce qui y est lié (fichiers, bons, méthodes, secteurs, synonymes) """
    def test_urg_create(self):
        """ Tester la création """
        urg = Urgences(nom=u"Urgence de test", image=u"ambulance.png", classe=u"urgence1")
        urg.save()
        # Vérification
        u=Urgences.objects.get(classe=u"urgence1")
        self.assertEqual(u.nom,u"Urgence de test")

    def test_nat_create(self):
        """ Tester la création """
        nat = Natures(nom=lipsum(255))
        nat.save()
        # Vérification
        n = Natures.objects.get(nom=lipsum(255))
        self.assertEqual(n.nom, lipsum(255))

    def test_recom_create(self):
        """ Tester la création """
        recom = Recommandations(nom=u"Recommandation de test", image=u"info.png", classe=u"recom2")
        recom.save()
        # Vérification
        r=Recommandations.objects.get(classe=u"recom2")
        self.assertEqual(r.nom,u"Recommandation de test")

    def test_fichier_create(self):
        """ Tester la création d'un fichier. """
        fich = Fichiers(nom=u"Un fichier de test", description=lipsum(255), fichier="fichier.pdf")
        fich.save()
        # Vérification
        f = Fichiers.objects.get(fichier="fichier.pdf")
        self.assertEqual(f.description,lipsum(255))
        self.assertEqual(f.nom,u"Un fichier de test")

    def test_bon_create(self):
        """ Tester la création d'un bon """
        bon = Bons(nom=u"Un formulaire de demande", image="bon.png", fichier="bon.pdf")
        bon.save()
        #vérification
        b = Bons.objects.get(image="bon.png")
        self.assertEqual(b.fichier,"bon.pdf")
        self.assertEqual(b.nom,u"Un formulaire de demande")


    def test_secteur_create(self):
        """ Tester la création d'un secteur """
        sect = Secteurs(nom=u"Un secteur de test", description=lipsum(255), telephone=u"+ 33 1 22 33 44 55", courriel=u"test@mail.com")
        sect.save()
        # Vérification
        s = Secteurs.objects.get(nom=u"Un secteur de test")
        self.assertEqual(s.telephone,u"+ 33 1 22 33 44 55")
        self.assertEqual(s.courriel,u"test@mail.com")
        self.assertEqual(s.description,lipsum(255))

    def test_methode_create(self):
        """ Tester la création d'une méthode. """
        meth = Methodes(nom=u"Méthode de test", description=lipsum(255))
        meth.save()
        # Vérification
        m = Methodes.objects.get(nom=u"Méthode de test")
        self.assertEqual(m.description,lipsum(255))

    def test_examen_create(self):
        """ Tester la création d'un examen.
        FIXME → Il faut trouver une solution pour le m2m (fichiers)"""
        # On récupère tous les champs nécessaires
        self.test_secteur_create()
        s = Secteurs.objects.get(nom=u"Un secteur de test")
        self.test_methode_create()
        m = Methodes.objects.get(nom=u"Méthode de test")
        self.test_nat_create()
        n = Natures.objects.get(nom=lipsum(255))
        self.test_fichier_create()
        f = Fichiers.objects.get(fichier="fichier.pdf")
        self.test_urg_create()
        u=Urgences.objects.get(classe=u"urgence1")
        self.test_bon_create()
        b = Bons.objects.get(image="bon.png")
        self.test_recom_create()
        r=Recommandations.objects.get(classe=u"recom2")
        # On créé notre examen de test
        exam = Examens(nom=u"Examen pour tester (beaucoup)",
                      code_sil=lipsum(40),
                      secteur=s,
                      methode=m,
                      nature=n,
         #             fichier=f,
                      urgence=u,
                      bon=b,
                      delai_rendu=5,
                      delai_analyse=24,
                      recommandations=r,
                      commentaire= u"<p> "+ lipsum(400) + lipsum(200) + "</p",
                      indications = u"<p> "+ lipsum(400) + lipsum(400) + "</p>")
        exam.save()
        # Vérification
        x = Examens.objects.get(delai_rendu=5)
        self.assertEqual(x.secteur.courriel,u"test@mail.com")
        self.assertEqual(x.methode.description,lipsum(255))
        #self.assertEqual(x.fichier.nom,u"Un fichier de test")
        self.assertEqual(x.urgence.image,u"ambulance.png")
        self.assertEqual(x.recommandations.image,u"info.png")
        #self.assertEqual(x.,)
        # les dates aussi 
        self.assertEqual(x.date_activation, datetime.date.today())
        ddes = x.date_activation+datetime.timedelta(days=110000)
        self.assertEqual(x.date_desactivation.strftime('%Y-%m-%d'),ddes.strftime('%Y-%m-%d'))

    def test_synonyme_create(self):
        """ Tester la création d'un synonyme """
        self.test_examen_create()
        x = Examens.objects.get(delai_rendu=5)
        syn = Synonymes(nom=u"Un synonyme de test", examen=x)
        syn.save()
        # Vérification
        syno = Synonymes.objects.get(nom=u"Un synonyme de test")
        self.assertEqual(syno.examen.nom,u"Examen pour tester (beaucoup)")

    def test_recherche(self):
        """ Test pour formulaire de recherche. Doit trouver l'examen par son synonyme et par son nom 
        FIXME → Ne fonctionne pas !"""
        #self.test_examen_create()
        #self.test_synonyme_create()
        #response = self.client.get('/recherche/', {'ChaineRecherche':'synonyme'}, follow=True)
        #self.assertEqual(response.template.name, 'ekz/recherche.html')
