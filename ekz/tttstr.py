# -*- coding: utf-8 -*-
# 
# Copyright (C) 2015  Nicolas Grandjean <nicolas@microniko.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
""" Quelques fonctions permettant de traiter des chaînes de caractères. 
"""


import unicodedata
#from unidecode import unidecode
def strip_diac(str):
    """ Surpprime les caractères diacrtitiques.
    Non utilisable pour l'instant. On retourne la chaîne telle quelle pour
    faire des tests plus tard """
    #return unicodedata.normalize('NFKD',str).encode('ascii','ignore')
    return str
    #return unidecode(str)

# Enlever les tags HTML
import re # Regexp
def strip_tags_html(str):
    """ Ôte les balises HTML (ce qu'il y a entre < et >).
    """
    str = re.sub("<.*?>","",str)
    # str = re.sub('<[^<]+?>', '', str)
    return str


