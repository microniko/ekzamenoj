# -*- coding: utf-8 -*-
# 
# Copyright (C) 2015  Nicolas Grandjean <nicolas@microniko.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from django.conf.urls import patterns, include, url

from views import ListeExamens, VoirExamen, liste_alpha_examens, accueil, ListeTubes, ListeBons, ListePrelevements, recherche

from django.contrib.auth.decorators import  permission_required


from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.static import static
from django.conf import settings

from django.views.generic.base import TemplateView


urlpatterns = patterns('',
    # La liste des examens (tous)
    url(r'^$', ListeExamens.as_view(paginate_by=10), name='liste_examens'),

    # La liste des tubes (tous)
    url(r'^recueils/$', ListeTubes.as_view(), name='liste_tubes'),

    # La liste des bons (tous)
    url(r'^formulaires_de_prescription/$', ListeBons.as_view(), name='liste_bons'),

    # La liste des prelevements (tous)
    url(r'^aide_au_prelevement/$', ListePrelevements.as_view(), name='liste_prelevements'),

    # Page d'un examen
    #url(r'^voir/(?P<id>\d+)$', voir_examen, name='voir_examen'),
    #url(r'^voir/(?P<pk>\d+)$', permission_required('ekz.view_inactif',raise_exception=True)(VoirExamen.as_view()),name='voir_examen'),
    url(r'^voir/(?P<pk>\d+)$', VoirExamen.as_view(),name='voir_examen'),

    # Liste alphabétique par lettre
    url(r'^commencent_par_(?P<lettre>.+)/$', liste_alpha_examens, name='liste_alpha_examens'),   # def liste_alpha_examens dans views.py

    # Page de recherche
    url(r'^rechercher/$', recherche, name='recherche'),
    # OpenSearch.xml 
    url('^opensearch.xml$', TemplateView.as_view(template_name='opensearch.xml'), name='opensearch'),

)

