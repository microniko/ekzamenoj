# -*- coding: utf-8 -*-
# 
# Copyright (C) 2014  Nicolas Grandjean <nicolas@microniko.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from django.db import models
#import ekzamenoj.settings

import datetime

from ekzamenoj import localsettings
# Create your models here.

from tttstr import strip_diac

class Tubes(models.Model):
    """ La table Tubes contient l'ensemble des éléments (tubes, flacons...) pouvant être utilisés pour les prélèvements.
    Le champ « ordre de prélèvement » permet de classer les prélèvements dans l'ordre, c'est surtout intéressant pour le prélèvement sanguin.
    La valeur peut-être comprise entre -32768 et 32767, n'hésitez pas à utiliser des valeurs espacées afin de pouvoir intercaler
    facilement un nouveau tube sans avoir à changer tous les nombres.
     """
    nom = models.CharField(verbose_name=u'Nom', max_length=255)
    ordre = models.SmallIntegerField(verbose_name=u'Ordre de prélèvement')
    image = models.ImageField(verbose_name=u"Image/Logo", upload_to="images/tubes/", blank=True)
    date_modif = models.DateTimeField(verbose_name=u"Date de la dernière modification", auto_now=True,)
    def __unicode__ (self):
        return self.nom
    class Meta:
        verbose_name=u"Tube ou flacon pour le prélèvement"
        verbose_name_plural=u"Tubes ou flacons pour le prélèvement"
        ordering = ['ordre']


class SousTraitants(models.Model):
    """ La table SousTraitants contient l'ensemble des sous-traitants utilisés en liens avec la sous-traitance """
    nom         = models.CharField(verbose_name=u'Nom du sous-traitant', max_length=255)
    description = models.CharField(verbose_name=u'Description', max_length=255, blank=True)
    ville       = models.CharField(verbose_name=u'Ville', max_length=15, blank=True)
    cp          = models.CharField(verbose_name=u'Code postal', max_length=15, blank=True)
    telephone   = models.CharField(verbose_name=u'Numéro de téléphone', max_length=100, blank=True)
    courriel    = models.EmailField(verbose_name=u'Adresse de courriel', blank=True)
    url         = models.URLField(u'Site Internet', blank=True)
    date_modif = models.DateTimeField(verbose_name=u"Date de la dernière modification", auto_now=True,)
    def __unicode__ (self):
        return self.nom
    class Meta:
        verbose_name=u"Sous-traitant"
        verbose_name_plural=u"Sous-traitants"

class Transporteurs(models.Model):
    """ La table Transporteurs contient l'ensemble des intermédiaires à la sous-traitance : les transporteurs """
    nom         = models.CharField(verbose_name=u'Nom du transporteur', max_length=255)
    description = models.CharField(verbose_name=u'Description', max_length=255, blank=True)
    ville       = models.CharField(verbose_name=u'Ville', max_length=255, blank=True)
    cp          = models.CharField(verbose_name=u'Code postal', max_length=255, blank=True)
    telephone   = models.CharField(verbose_name=u'Numéro de téléphone', max_length=100, blank=True)
    courriel    = models.EmailField(verbose_name=u'Adresse de courriel', blank=True)
    url         = models.URLField(u'Site Internet', blank=True)
    date_modif = models.DateTimeField(verbose_name=u"Date de la dernière modification", auto_now=True,)
    def __unicode__ (self):
        return self.nom
    class Meta:
        verbose_name=u"Transporteur"
        verbose_name_plural=u"Transporteurs"


class SousTraitance(models.Model):
    """ La table SousTraitance permet de résumer les conditions de sous-traitance.
    Elle est liée aux tables SousTraitants, Transpoteurs, Temperatures et Stockages.
    Un seul examen est lié à un seul mode de soustraitance, les infos sont  donc spécifiques """
    soustraitant = models.ForeignKey(SousTraitants, verbose_name=u"Sous-traitant", on_delete=models.PROTECT)
    transporteur = models.ForeignKey(Transporteurs, verbose_name=u"Transporteur", blank=True, null=True, on_delete=models.PROTECT)
    temperature  = models.ForeignKey('Temperatures', blank=True, null=True, on_delete=models.PROTECT)
    stockage     = models.ForeignKey('Stockages', blank=True, null=True, on_delete=models.PROTECT)
    url          = models.URLField(verbose_name=u'Liens vers infos externes', blank=True, null=True)
    infos        = models.TextField(verbose_name=u"Informations complémentaires", blank=True, null=True)
    date_modif = models.DateTimeField(verbose_name=u"Date de la dernière modification", auto_now=True,)
    def __unicode__(self):
            return u"Envoyé par {0} vers {1} ({2})".format(self.transporteur,self.soustraitant,self.temperature)
    class Meta:
        verbose_name=u"Sous-traitance"
        verbose_name_plural=u"Sous-traitances"
        permissions = (
            ("view_soustraitance", u"Peut voir la sous-traitance"),
            ("view_soustraitance_details", u"Peut voir les détails de la sous-traitance"),
            )


class Urgences(models.Model):
    """ La table Urgences contient la liste des niveaux d'urgence. Quatre classes CSS sont disponibles pour les illustrer """
    CSS = (
       ('urgence1',u'Urgence 1'),
       ('urgence2',u'Urgence 2'),
       ('urgence3',u'Urgence 3'),
       ('urgence4',u'Urgence 4'),
    )
    nom = models.CharField(verbose_name=u"Nom", max_length=255)
    image = models.ImageField(verbose_name=u"Image/Logo", upload_to="images/urgences/", blank=True)
    classe = models.CharField(verbose_name=u'Classe (CSS)', max_length=100, blank=True, choices=CSS)
    date_modif = models.DateTimeField(verbose_name=u"Date de la dernière modification", auto_now=True,)
    def __unicode__ (self):
        return self.nom
    class Meta:
        verbose_name=u"Niveau d'urgence"
        verbose_name_plural=u"Niveaux d'urgence"


class Natures(models.Model):
    """ La table Natures liste les matrices (nature des spécimens à analyser) """
    nom = models.CharField(verbose_name=u"Nom", max_length=255)
    date_modif = models.DateTimeField(verbose_name=u"Date de la dernière modification", auto_now=True,)
    def __unicode__ (self):
        return self.nom
    class Meta:
        verbose_name=u"Nature de spécimen (matrice)"
        verbose_name_plural=u"Natures de spécimen (matrices)"


class Recommandations(models.Model):
    """ La table Recommandations contient les recommandations utiles pour le prélèvement.
    Quatre classes CSS sont proposées pour la mise en forme."""
    CSS = (
       ('recom1',u'recom 1'),
       ('recom2',u'recom 2'),
       ('recom3',u'recom 3'),
       ('recom4',u'recom 4'),
    )
    nom = models.CharField(verbose_name=u"Nom", max_length=255)
    image = models.ImageField(verbose_name=u"Image/Logo", upload_to="images/recommandations/", blank=True)
    classe = models.CharField(verbose_name=u'Classe (CSS)', max_length=100, blank=True, choices=CSS, default='recom1')
    date_modif = models.DateTimeField(verbose_name=u"Date de la dernière modification", auto_now=True,)
    def __unicode__ (self):
        return self.nom
    class Meta:
        verbose_name=u"Recommandation pour le prélèvement"
        verbose_name_plural=u"Recommandations pour le prélèvement"



class Examens(models.Model):
    """ La table Examens est la table centrale du système, elle regroupe les examens disponibles """
    nom          = models.CharField(verbose_name=u"Nom de l'examen", unique=True, null=False, max_length=255)
    date_activation = models.DateField(verbose_name=u"Date d'activation",default=lambda: datetime.datetime.now())
    date_desactivation = models.DateField(verbose_name=u"Date de désactivation",default=lambda: datetime.datetime.now()+datetime.timedelta(days=110000))
    code_sil     = models.CharField(verbose_name=u"Code SIL", max_length=255, blank=True, null=True)
    secteur      = models.ForeignKey('Secteurs', blank=True, null=True, on_delete=models.PROTECT)
    methode      = models.ForeignKey('Methodes', blank=True, null=True, on_delete=models.PROTECT)
    nature       = models.ForeignKey('Natures', blank=True, null=True, verbose_name=u"Nature du spécimen (matrice)")
    fichier      = models.ManyToManyField('Fichiers', verbose_name=u"Fichier(s) joint(s)", blank=True)
    urgence      = models.ForeignKey('Urgences',verbose_name=u"Peut-être demandé en urgence", blank=True, null=True)
    bon          = models.ForeignKey('Bons', blank=True, null=True, on_delete=models.PROTECT)
    actes        = models.ManyToManyField('Actes', through='Facturation', verbose_name=u'Actes', blank=True)
    delai_rendu  = models.SmallIntegerField(verbose_name=u"Délai (en heures) de rendu du résultat", blank=True, null=True)
    delai_recept = models.SmallIntegerField(verbose_name=u"Délai (en heures) après le prélèvement", blank=True, null=True)
    delai_preana = models.SmallIntegerField(verbose_name=u"Délai (en heures) après le préanalytique", blank=True, null=True)
    recommandations  = models.ForeignKey(Recommandations, verbose_name=u"Recommandations pour le prélèvement", blank=True, null=True)
    commentaire  = models.TextField(verbose_name=u"Commentaire", blank=True, null=True)
    indications  = models.TextField(verbose_name=u"Indications de l'examen", blank=True, null=True)
    acheminements = models.ForeignKey('Acheminements', blank=True, null=True, on_delete=models.PROTECT)
    soustraitance = models.OneToOneField(SousTraitance, verbose_name=u"Sous-traitance", blank=True, null=True)
    preanalytique   = models.ForeignKey('Preanalytiques', blank=True, null=True, on_delete=models.PROTECT)
    date_modif   = models.DateTimeField(verbose_name=u"Dernière mise à jour", auto_now=True)
    def all_synonymes(self):
        """ Retrouve la liste des synonymes associées """
        return self.synonymes_set.all()
    def all_prelevements(self):
        """ Retrouve à quels prelèvements sont associés l'examen """
        return self.prelevements_set.all()
    def is_actif(self):
        """ Retourne True si l'examen est actif.
            La gestion de la permission view_inactif est géré au niveau des URL """
        if (self.date_activation < datetime.date.today() and self.date_desactivation <= datetime.date.today()) or (self.date_activation > datetime.date.today() and self.date_desactivation >= datetime.date.today()):
            return False
        return True
    def __unicode__ (self):
        if self.date_desactivation < datetime.date.today():
            return u'{0} »»»» DÉSACTIVÉ'.format(self.nom)
        elif self.date_activation > datetime.date.today():
            return u'{0} »»»» DÉSACTIVÉ'.format(self.nom)
        else:
            return self.nom
    class Meta:
        verbose_name=u"Examen"
        verbose_name_plural=u"Examens"
        permissions = (
            ("view_code_sil", u"Peut voir le code SIL"),
            ("download_bon", u"Peut télécharger un formulaire de prescription"),
            ("view_inactif", u"Peut voir les examens inactifs"),
            )

class Centrifugations(models.Model):
    """ Dans la table Centrifugation, les modes de centrifugation sont listés """
    vitesse = models.CharField(verbose_name=u"Vitesse", max_length=100)
    temps = models.CharField(verbose_name=u"Temps", max_length=100)
    temperature = models.ForeignKey('Temperatures', blank=True, null=True, on_delete=models.PROTECT)
    date_modif = models.DateTimeField(verbose_name=u"Date de la dernière modification", auto_now=True,)
    def __unicode__ (self):
        return u"Centrifugation à {0} pendant {1}".format(self.vitesse,self.temps)
    class Meta:
        verbose_name=u"Centrifugation"
        verbose_name_plural=u"Centrifugations"


class Preanalytiques(models.Model):
    """ La table Preanalytiques définit les conditions préanalytiques. Elle est liée aux tables Centrifugations, Stockages
    et Analyseurs. Le champs aliquotage est une chaîne de caractères """
    centrifugation = models.ForeignKey('Centrifugations', blank=True, null=True, on_delete=models.PROTECT)
    aliquotage     = models.CharField(verbose_name=u"Aliquotage", max_length=255, blank=True, null=True)
    stockage       = models.ForeignKey('Stockages', blank=True, null=True, on_delete=models.PROTECT)
    analyseur      = models.ForeignKey('Analyseurs', blank=True, null=True, on_delete=models.PROTECT)
    date_modif = models.DateTimeField(verbose_name=u"Date de la dernière modification", auto_now=True,)
    def __unicode__ (self):
        if self.centrifugation == None:
            txt_cen = u'Pas de centrifugation'
        else:
            txt_cen = str(self.centrifugation).decode("utf-8")
        if self.aliquotage == '':
            txt_ali = u"pas d'aliquotage"
        else:
            #txt_ali = self.aliquotage
            txt_ali =str(self.aliquotage).decode("utf-8")
        if self.stockage == None:
            txt_sto = u"traitement immédiat"
        else:
            txt_sto = str(self.stockage).decode("utf-8")
        if self.analyseur != None:
            foo = txt_cen + u" puis " + txt_ali + u" puis " + txt_sto + u" puis " + str(self.analyseur).decode("utf-8")
        else:
            foo = txt_cen + u" puis " + txt_ali + u" puis " + txt_sto
        return foo
    class Meta:
        verbose_name=u"Preanalytique"
        verbose_name_plural=u"Préanalytiques"
        permissions = (
            ("view_preanalytique", u"Peut voir les conditions préanalytiques"),
            )



class Synonymes(models.Model):
    """ La table Synonymes permet de lié un examen à un ou plusieurs synonymes """
    nom        = models.CharField(verbose_name=u'Nom du synonyme', max_length=255)
    examen     = models.ForeignKey('Examens', blank=False, null=False, on_delete=models.PROTECT)
    date_modif = models.DateTimeField(verbose_name=u"Date de la dernière modification", auto_now=True,)
    def __unicode__ (self):
        return self.nom
    class Meta:
        verbose_name=u"Synonyme"
        verbose_name_plural=u"Synonymes"



class Temperatures(models.Model):
    """ Les températures ..."""
    nom        = models.CharField(verbose_name=u'Température', max_length=255)
    description= models.CharField(verbose_name=u'Description', max_length=255, blank=True)
    date_modif = models.DateTimeField(verbose_name=u"Date de la dernière modification", auto_now=True,)
    def __unicode__ (self):
        return self.nom
    class Meta:
        verbose_name=u"Température"
        verbose_name_plural=u"Températures"

class Stockages(models.Model):
    """ La table Stockages contient tous les lieux de stockage et détaille les conditions (liés à la table des températures). """
    nom       = models.CharField(verbose_name=u"Lieu (frigo, congelateur...)", max_length=255)
    piece      = models.CharField(verbose_name=u'Pièce', max_length=255, blank=True, null=True)
    temperature = models.ForeignKey('Temperatures', blank=True, null=True, on_delete=models.PROTECT)
    date_modif = models.DateTimeField(verbose_name=u"Date de la dernière modification", auto_now=True,)
    def __unicode__ (self):
        if self.piece == u'':
            txt_piece = u''
        elif self.piece == None :
            txt_piece = u''
        else:
            txt_piece = u'(pièce ' + self.piece + u')'
        if self.temperature != None:
            txt_temp = u'à ' + str(self.temperature).decode("utf-8")
        else:
            txt_temp = u''
        return u"{0} {1} {2}".format(self.nom,txt_temp,txt_piece)

    class Meta:
        verbose_name=u"Lieu de stockage"
        verbose_name_plural=u"Lieux de stockage"

class Acheminements(models.Model):
    """ La table Acheminements permet de définir les modalités d'acheminement du prélèvement, lié à une température (facultatif) """
    nom         = models.CharField(verbose_name=u'Libellé', max_length=255)
    date_modif = models.DateTimeField(verbose_name=u"Date de la dernière modification", auto_now=True,)
    temperature  = models.ForeignKey('Temperatures', blank=True, null=True, on_delete=models.PROTECT)
    def __unicode__ (self):
        return u"{0} ({1})".format(self.nom,self.temperature)
    class Meta:
        verbose_name=u"Modalité d'acheminement du prélèvement"
        verbose_name_plural=u"Modalités d'acheminement du prélèvement"

class Lettres(models.Model):
    """ La table Lettres permet de définir autant de lettres (typiquement B, BHN et pourquoi pas €) que nécessaire et son prix en €uros 
    """
    lettre = models.CharField(verbose_name=u"Lettre", max_length=10)
    prix = models.FloatField(verbose_name=u"Prix (type float) de la lettre")
    date_modif = models.DateTimeField(verbose_name=u"Date de la dernière modification", auto_now=True,)
    def __unicode__ (self):
        return u"{0}, {1} {2}".format(self.lettre,self.prix, localsettings.CONFIG_LOCALE['MONNAIE_LOCALE'])
    class Meta:
        verbose_name=u"Lettre"
        verbose_name_plural=u"Lettres"


class Actes(models.Model):
    """ La table Actes répertorie l'ensemble des actes de la nommenclature (ou non) afin de calculer le prix de chaque examens """
    code = models.CharField(verbose_name=u'Code acte', max_length=10, unique=True)
    libelle = models.CharField(verbose_name=u'Libellé', max_length=150, blank=True)
    nombre = models.FloatField(verbose_name=u'Prix (en nombre de lettre', blank=True)
    lettre = models.ForeignKey('Lettres', blank=True, null=True, on_delete=models.PROTECT)
    date_modif = models.DateTimeField(verbose_name=u"Date de la dernière modification", auto_now=True,)
    def _get_prix(self):
        total = int(self.nombre) * self.lettre.prix
        return total
    prix = property(_get_prix)
    def __unicode__ (self):
        total = int(self.nombre) * self.lettre.prix
        return u"{0} - {1}  ({2}{3} soit {4}{5})".format(self.code,self.libelle,self.nombre, self.lettre.lettre, total, localsettings.CONFIG_LOCALE['MONNAIE_LOCALE'])
    class Meta:
        verbose_name=u"Acte"
        verbose_name_plural=u"Actes"

class Facturation(models.Model):
    '''Table de liaison perso entre Examens ↔ Actes
    '''
    examen = models.ForeignKey('Examens', verbose_name=u'Examen')
    acte   = models.ForeignKey('Actes', to_field='code', verbose_name=u'Code acte')
    date_modif = models.DateTimeField(verbose_name=u"Date de la dernière modification", auto_now=True)
    def __unicode__ (self):
        return u"{0} : {1}".format(self.examen,self.acte)
    class Meta:
        verbose_name=u"Facturation : liaison Examens/Actes"
        verbose_name_plural=u"Facturation : liaisons Examens/Actes"

class Secteurs(models.Model):
    """ La table Secteurs contient les secteurs d'exécution des examens """
    nom         = models.CharField(verbose_name=u'Nom du secteur', max_length=255)
    description = models.CharField(verbose_name=u'Description', max_length=255, blank=True)
    telephone   = models.CharField(verbose_name=u'Numéro de téléphone', max_length=100, blank=True)
    courriel    = models.EmailField(verbose_name=u'Adresse de courriel', blank=True)
    date_modif = models.DateTimeField(verbose_name=u"Date de la dernière modification", auto_now=True,)
    def __unicode__ (self):
        return self.nom
    class Meta:
        verbose_name=u"Secteur d'exécution"
        verbose_name_plural=u"Secteurs d'exécution"


class Methodes(models.Model):
    """ La table Methodes contient l'ensemble des méthodes d'analyses des examens """
    nom         = models.CharField(verbose_name=u'Nom', max_length=255)
    description = models.CharField(verbose_name=u'Description', max_length=255, blank=True)
    date_modif = models.DateTimeField(verbose_name=u"Date de la dernière modification", auto_now=True,)
    def __unicode__ (self):
        return self.nom
    class Meta:
        verbose_name=u"Méthode d'analyse"
        verbose_name_plural=u"Méthodes d'analyse"

class Bons(models.Model):
    """ La table Bons rassemble les formulaires de demande des examens """
    nom = models.CharField(verbose_name=u"Nom", max_length=255)
    image = models.ImageField(verbose_name=u"Image/Logo", upload_to="images/bons/", blank=True)
    fichier = models.FileField(verbose_name=u"Fichier", upload_to="fichiers/bons/", blank=True)
    date_modif = models.DateTimeField(verbose_name=u"Date de la dernière modification", auto_now=True,)
    def __unicode__ (self):
        return self.nom
    class Meta:
        verbose_name=u"Bon de demande"
        verbose_name_plural=u"Bons de demande"


class Fichiers(models.Model):
    """ La table Fichier permet de lier à certains examens des fichiers joints (formulaires, explications...) """
    nom = models.CharField(verbose_name=u"Nom", max_length=255)
    description = models.CharField(verbose_name=u'Description', max_length=255, blank=True)
    fichier = models.FileField(verbose_name=u"Fichier", upload_to="fichiers/fichiers/", blank=True)
    #typemime = models.CharField(verbose_name=u'Type', max_length=30, blank=True)
    date_modif = models.DateTimeField(verbose_name=u"Date de la dernière modification", auto_now=True,)
    def __unicode__ (self):
        return self.nom
    class Meta:
        verbose_name=u"Fichier joint"
        verbose_name_plural=u"Fichiers joints"


class Prelevements(models.Model):
    """Regroupe les examens communs au même couple nombre de tube/nature de tube"""
    nom = models.CharField(verbose_name=u"Nom du groupe de prélèvement", max_length=255)
    nombre = models.SmallIntegerField(verbose_name=u'Nombre de tube(s) ou flacon(s)')
    tube   = models.ForeignKey(Tubes)
    examen = models.ManyToManyField('Examens', verbose_name=u"Examens en communs", through="UtilisationGroupePrelevement")
    date_modif = models.DateTimeField(verbose_name=u"Date de la dernière modification", auto_now=True)
    def ImgTubes(self):
        i=1
        html = u''
        if self.nombre > 5:
            html += str(self.nombre) + " x "
            nb = 1
        else:
            nb = self.nombre
        while i <= nb:
            html += u'<img src="/fichiers/' + str(self.tube.image).decode('utf-8') + u'" alt="' + self.tube.nom + '" title="' + self.tube.nom +'">'
            i = i+1
        return html
    def machin(self):
        return u'Coucou coucou !' + str(self.nombre)
    def __unicode__(self):
            return u"{0} ({1} {2})".format(self.nom,self.nombre,self.tube)
    class Meta:
        verbose_name=u"Groupe de prélèvement"
        verbose_name_plural=u"Groupes de prélèvements"
        ordering = ['tube']

class UtilisationGroupePrelevement(models.Model):
    """Utilisation du groupe de prélèvement par examen"""
    pourcentage = models.FloatField(verbose_name=u"Pourcentage du tube utilisé", max_length=3)
    prelevement = models.ForeignKey('Prelevements', verbose_name=u'Groupe de prélèvement')
    examen = models.ForeignKey('Examens', verbose_name=u'Examen')
    date_modif = models.DateTimeField(verbose_name=u"Date de la dernière modification", auto_now=True,)
    def __unicode__(self):
            return u"{0} utilise {1} % de {2}".format(self.examen,self.pourcentage,self.prelevement)
    class Meta:
        verbose_name=u"Utilisation du groupe de prélèvement"
        verbose_name_plural=u"Utilisation des groupes de prélèvements"

class Analyseurs(models.Model):
    """Liste des analyseurs"""
    nom = models.CharField(verbose_name=u"Nom", max_length=255)
    image = models.ImageField(verbose_name=u"Image/Logo", upload_to="images/analyseurs/", blank=True)
    description = models.CharField(verbose_name=u'Description', max_length=255, blank=True)
    date_modif = models.DateTimeField(verbose_name=u"Date de la dernière modification", auto_now=True,)
    def __unicode__(self):
            return self.nom
    class Meta:
        verbose_name = u'Analyseur'
        verbose_name_plural = u'Analyseurs'

