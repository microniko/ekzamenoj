Méthode pour remplir à la main la base de données
=================================================

Ce qui suit ce passe dans l'interface d'administration des examens (admin/ekz)

Principes généraux
------------------
La logique d'Ekzamenoj veut que l'on commence toujours par l'ajout
d'une utilisation de tube, un examen étant lié à un prélèvement
par l'utilisation d'un tube.

On appele « groupe de prélèvement » un ensemble de tubes ou flacons pouvant être utiliser pour réaliser un ou plusieurs examens.

Les synonymes sont ajoutés en dernier.

Dans les formulaires de créations des listes déroulantes (ou des listes FIXEME !!!!) vous permettent de choisir des éléments liés à celui que vous êtes en train de créer. Dans tous les cas, un signe « + » (vert) ouvre un formulaire d'ajout d'un nouvel élément afin de rendre la saisie plus simple et plus rapide.


Étapes préliminaires
--------------------
Créer les lettres disponibles (habituellement B, BHN, éventuellement €) et leur coût.

Créer (ou importer) les actes de la nommenclature. Le système présentera le coût en euros de chaque examens pour lesquels les actes sont renseignés.

Les autres tables pourront être complétées au fur et à mesure de la saisie des examens.



Ajouter une utilisation du groupe de prélèvement
------------------------------------------------
Prennons trois exemples simples :

Exempe 1 : On considère que l'on peut réaliser un hémogramme sur un tube EDTA et que ce tube peut également servir pour une quantification de la population lymphocytaire (T4T8). Une fois ces examens réalisés, il ne sera plus possible d'utiliser le tube pour autre chose.
Exemmple 2  : Pour rercher la mutation du gène X, il faut 3 tubes EDTA et un tube hépariné. La recherche de la mutation du gène Y peut-être faite sur les mêmes tubes.
Exemple 3 : La mesure du TP nécessite un tube citraté et le dossage du facteur VIII aussi mais si les deux examens sont demandés en même temps, il faudra deux tubes.


Pour le premier exemple, il faudra créer deux utilisations de prélèvement. La première, utilisant 50 % du groupe de prélèvement « hématologie » (comprennant 1 tube EDTA)  pour l'examen hémogramme. La seconde, utilisant 50 % du groupe de prélèvement « hématologie » pour l'examen T4T8. Nous pourrions indiquer respectivement 30 % et 70 % d'utilisation, dans cette exemple, celà n'a pas d'importance (ça en aurait si d'autres examens utiliseraient le groupe « hématologie »).

Pour le second exemple, il faudra aussi créer deux utilisations de prélèvement. En créant la première, (utilisant 50 % du groupe) on créé le groupe de prélèvement « génétique 1 » comprenant 3 tubes EDTA. On créé également l'examen mutation du gène X. Ensuite, en créant une nouvelle utilisation de prélèvement, il faut créer un nouveau groupe de prélèvement « génétique 2 » utilisant 50 % du tube hépariné lié à l'examen mutation du gène X (sélectionner celui-ci dans la liste). Ajouter ensuite une troisième et une quatrième utilisation de prélèvement pour l'examen mutation du gène Y (sur le même principe que X) en reprenant les groupes de prélèvements créé précédemment.

Pour le troisième et dernier exemple, il faudra créer une première utilisation de prélèvement utilisant 10 % du groupe de prélèvement « hemostase » (le reste pouvant être utilisé pour d'autres examens créés plus tard) comprennant 1 tube citraté. L'examen TP est créé lors de la création de l'utilisation du groupe de prélèvement. L'examen dosage du facteur VIII est créé au moment de créer une nouvelle utilisation de groupe de prélèvement utilisant 100 % du groupe « hémostase ».

Remarquez que pour ce dernier exemple, il y a plusieurs solutions possible, celle proposée ici permet de présenter les possibilités du système.


Ajouter des synonymes
---------------------
Vous pouvez ajouter autant de synonymes que nécessaire par examen. Dans les exemples précédents :
- Facteur anti-hémophilique A est synonyme de dosage du facteur VIII
- Facteur 8 est synonymes de dosage du facteur VIII.
- Maladie M est synonyme de mutation du gène X et du gène Y.
- Taux de prothrombine est synonmye de TP.
- Temps de Quick est synonmye de TP.
- Numération globulaire est synonyme d'hémogramme.
- Etc.

Ces synonymes apparaissent dans la liste des examens par lettre.


Éléments liés aux groupes de prélèvements
-----------------------------------------

Lorsque vous créez un nouveau groupe de prélèvement, outre le nom, vous devez préciser le nombre de tubes concernés et leur nature. Il peut s'agir de tubes mais aussi de flacon, seringue ou n'importe quel contenant. Concernant les tubes (ou flacon), vous devrez préciser l'ordre de prélèvement. Il est vivement conseillé de choisir des valeurs éloignées afin de pouvoir intercaller des tubes à l'avenir. La valeur peut être comprise entre -32768 et 32767. L'ajout d'une image du tube est facultative.


Champs des examens
------------------

Les seules données obligatoires sont le nom de l'examen et les dates d'activation et de désactivation (l'examen figurera dans le site public entre ces dates).


Éléments liés au prélèvement
-----------------------------

- Nature du spécimen : correspond à la matrice (sang total, sérum, plasma, urine,…).
- Bon de demande d'examen : seul le nom est obligatoire, vous pouvez lier une image (un logo) et permettre de télécharger une version imprimable.
- Recommendations pour le prélèvement : figure en dessous des informations relative aux tubes.
- Fichiers joints : vous pouvez lier autant de fichiers que nécessaires.


Préanalytique
-------------

- Centrifugation (vitesse/temps).
- Aliquotage (champ libre).
- Stockage (avant analyse) : lieu et pièces sont des champ texte libre, le champ température (avec température -p.ex. réfrigéré- et sa description -p.ex. entre 2 et 6°C).
- Analyseur.


Sous-traitance
--------------
Un item de sous-traitance est lié à un seul examen (même dans les mêmes conditions). Vous pourrez ainsi détailler spécifiquement les conditions (avec un lien externe éventuellement et un champ de description).


Analytique
----------
- Code SIL (limité à 255 caractères, vous pouvez en indiquer plusieurs si besoin).
- Secteur : exécution de l'analyse (avec une description, numéro de téléphone, adresse de courriel).
- Méthode d'analyse.
- Délai de rendu (en nombre de jours).
- Délai après prélèvement.


Informations complémentaires
----------------------------
Le commentaire s'affiche sur la page de description de l'examen. La description est masquée (un lien de « démasquage » est proposé).


Facturation
-----------
Ajouter autant d'actes liés à une nommenclature que nécessaire en cliquant sur le « + ». Si vous ne connaissez pas le code acte, cliquez sur la loupe pour ouvrir une fenêtre de recherche.



Ajouter une page
----------------
Les pages sont des textes plutôt long dont le lien a vocation à rester longtemps visible dans le menu. Vous definissez une période d'affichage de la page.

Ajouter une nouvelle
--------------------
Les nouvelles permettent d'jouter des informations courtes visibles en page d'accueil du site. Là aussi, vous définissez une période d'affichage de la nouvelle.

