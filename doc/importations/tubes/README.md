Comment générer pleins de bouchons avec des couleurs différentes
================================================================


Commande inkscape pour l'exportation en PNG
-------------------------------------------


 inkscape -e essai1.png -w 25 modele_de_bouchon_tube.svg

fichier essai1.png de 25 px de large à partir du SVG.


Le fichier SVG modele_de_bouchon_tube.svg
-----------------------------------------

__Il faudra le nettoyer de tous les trucs inutiles d'Inkscape__

# Couleur du bouchon
  <path
     id="couleur-bouchon"
     style="fill:#00ffff;fill-opacity:1;stroke:#000000;stroke-width:1.39706445;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-opacity:1;stroke-dashoffset:0">
  </path>


# Couleur de la colerette
  <path
     id="Colerette"
     style="fill:#808000;stroke:#000000;stroke-width:1.39706445000000001;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-opacity:1;stroke-dashoffset:0;fill-opacity:1">
  </path>

Il faut maintenant trouver le moyen de modifier dans l'arbre XML le _fill_ du _style_ du _path.id=couleur-bouchon_ 
et                                                               le _fill_ du _style_ du _path.id=Colerette_ 


Algorithme
----------

Saisir la couleur, trouver la valeur hexa correspondante pour le bouchon
puis
Saisir la couleur, trouver la valeur hexa correspondante pour la colertte
puis
Saisir la taille
Générer le PNG avec le nom des couleurs.


Code
----

Python :
 import re

f = open("file","r")
f2 = open("file2","w")

repl = { 'str1': 'repl1', 'str2': 'repl2' }

for l in f.readlines():
       for str in repl:
               l = re.sub( str, repl[str], l )
       f2.write( l )

f.close()
f2.close()

Bash :

 sed -i -e 's/chaine1/chaine5/g|sed s/chaine2/chaine6/g' -e 's/chaine3/chaine7/g' myfic

Ce qui donne : 

sed -i -e 's/COULEUR-BOUCHON/#xxxx/g' modele_de_bouchon_tube.svg
sed -i -e 's/COULEUR-COLERETTE/#xxxx/g' modele_de_bouchon_tube.svg


Le script fonctionne, il faudrait voir pour la taille maintenant et voir si on pourrait avoir une option pour les bouchons sans colerette... OK.
