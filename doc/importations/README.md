Importation de données au format CSV
====================================
__Ne chercher pas un import Excel®, c'est inutile.__

Module CVSImport
----------------
Installer ce module [https://github.com/edcrewe/django-csvimport] à l'aide de PyPi (en root) :
#  pip django-csvimport

Installation automatique avec les requirements.


Principe d'importation des données
----------------------------------
L'import à lieu dans l'interface d'administration (http://le_site/admin/csvimport/csvimport/). Il faudra ajouter un contenu par table à injecter.

Dans tous les cas, il faut que les informations liées soient renseignées dans les tables sinon l'importation échouera.

Le format de la liste des champs (field list) correspond à :
    columnX = nom du champ dans la table importée
        Avec X >= 1 (pas de zéro !!)
Pour les tables liées, vous devez indiqué le champ correspondant à la description dans la table externe (un coup d'œil sur le schéma relationnel de la base de données vous donnera plus d'indications) :
    columnY = nom du champ dans la table importée(Table externe|champ lié)

L'ordre des champs qui est donné ici correspond à l'ordre dans la base de données (et dans les formulaires). L'important étant d'avoir la correspondance entre le numéro de la colonne et le code du champ de la table.

Si vous ne renseignez rien dans la zone fields list, le module considèrera la première colonne comme le nom du champ dans la table et recherchera sa correspondance.

Il est possible d'ajouter un enregistrement à une table externe si les autres champs (de cette table externe) ne sont pas obligaoires.

Utilisation de LibreOffice
--------------------------
Exporter les feuilles après modifications au format CSV :
- Jeu de caractère : UTF-8.
- Séparateur de champ : , (virgule).
- Séparateur de texte : " (guilemets).
- Cocher _mettre entre guilement toutes les cellules de texte_.

Visiblement, il n'y a pas besoin de séparateur de texte, ça fonctionne très bien sans.

Utilisation de Gnumeric
-----------------------

À compléter. L'export est plus pratique car il ne modifie pas le document de travail contrairement à LibreOffice. Utiliser  Export > Format texte paramétrable...



Importation des données au format CSV
-------------------------------------
Consultez la doc de CVS-Import pour plus de détails.
/usr/local/lib/python2.7/dist-packages/csvimport/models.py => Modifier la classe CSVImport : field_list = models.TextField(blank=True,

L'importation devrait se faire dans l'ordre suivant.

### Avant tout
Créer une entrée dans la table des lettres (au moins une : le B) avant d'importer les codes de facturation


### Codes de facturation  => Table ekz.Actes
##### Format du fichier CSV
Utiliser le fichier NABM.csv proposé ou le créer (cf. recommandations ci-dessous dans l'exportation au format CSV). 
"CODE","LIBELLE","COEFFICIENT B","lettre","prix"
4084,"DETERMINATION PRENATALE DU SEXE FOETAL SANG MATERNEL",500,"B",0.27

ATTENTION aux zéros devant les codes actes (4 zéros non significatifs) !
Je pense que la colonne prix est inutile.
Il faut que les lettres injectées soient connues (cf. _Avant tout_)
Attention, le fichier BHN Montpellier est un peu mal foutu, il faut le retravailler en triant les colonnes après avoir scinder les colonnes et vérifier qu'il n'y a pas de lettre mal formées ... (B30 au lieu de B ...)

#### Field list 
column1=code, column2=libelle, column3=nombre, column4=lettre(Lettres|lettre)

### Table des méthodes d'analyses => Table ekz.Methodes ###

Inutile de préciser la correspondance colonne/champ, la première ligne correspond au nom des champs.

Lors de l'import des examens, si l'entrée correspondant à la méthode est trouvée dans la table Methodes, il est utilisé. Sinon, il est créé.
Les mises à jour (par exemple pour mettre les descriptions après) ne fonctionnent pas.

### Table des sous-traitants => Table ekz.SousTraitants ###

Inutile de préciser la correspondance colonne/champ, la première ligne correspond au nom des champs.


### Table des transporteurs => Table ekz.Transporteurs ###

Inutile de préciser la correspondance colonne/champ, la première ligne correspond au nom des champs.


### Table des températures => Table ekz.Temperatures ###

Inutile de préciser la correspondance colonne/champ, la première ligne correspond au nom des champs.


### Table des lieux de stockage => Table ekz.Stockages ###

Inutile de préciser la correspondance colonne/champ, la première ligne correspond au nom des champs.


### Table de la sous-traitance => Table ekz.SousTraitance ###

Liste des champs :
column1=soustraitant(SousTraitants|nom),
column2=stockage(Stockages|nom),
column3=temperature(Temperatures|nom),
column4=transporteur(Transporteurs|nom),
column5=infos,
column6=url

### Table des centrifugations => Table ekz.Centrifugations ###

Inutile de préciser la correspondance colonne/champ, la première ligne correspond au nom des champs.


### Table des analyseurs => Table ekz.Analyseurs ###

Inutile de préciser la correspondance colonne/champ, la première ligne correspond au nom des champs.
Si vous souhaiter lier des images, vous devrez indiquer dans la colonne image le chemin vers l'image de l'analyseur (de la même manière que les tubes mais dans images/analyseurs).

### Tables des conditions préanalytiques => Table ekz.Preanalytiques ###

Liste des champs :
column1=centrifugation(Centrifugations|id),
column2=stockage(Stockages|id),
column3=aliquotage,
column4=analyseur(Analyseurs|nom)

Il y a un problème pour les centrifugations, stockages et analyseurs. Le système créé des entrée dans les tables liées si les cases sont vides. Si on met 0, il le cré quand-même avec l'ID = 0... S'il n'y a pas moyen de s'en sortir, une requète en dur dans la base de données sur la table en remplaçant 0 par rien pourrait résoudre le problème.
On a mis 0 et ça passe, voir avec la requête... après...
À CONFIRMER !!!


### Table des secteurs => Table ekz.Secteurs ###

Inutile de préciser la correspondance colonne/champ, la première ligne correspond au nom des champs.


### Table des examens => Table ekz.examens

##### Important #####

Il faudra peut-être faire l'importation en plusieurs fois. En effet, les champs soustraitance et preanalytique ne peuvent pas être vides s'ils sont déclarés dans la liste des champs importés (le module essaye de créer des entrées mais échoue car les champs obligatoires ne sont pas renseignés.
Il y a deux tableaux dans modele_import.odt

##### Format du fichier CSV
Utilisez le fichier ODT proposé et exporter la feuille concernée au format CSV (cf. recommandations ci-dessous dans l'exportation au format CSV). 
Ne laissez-pas vide les champs delai_rendu et delai_analyse, si vous ne souhaiez pas que ces renseignements apparaissent, mettre 0 (zéro).
Pour les dates, le format correct est AAAA-MM-JJ (format ISO 8601) et doit être renseigné obligatoirement.
- Les fichiers joints devront être liés manuellement.
- Les recommandations devront être liés manuellement.

##### Field list

Ceci fonctionne : 
column1=nom,
column2=date_activation,
column3=date_desactivation,
column4=code_sil,
column5=secteur(Secteurs|nom),
column6=methode(Methodes|nom),
column7=nature(Natures|nom),
column9=urgence(Urgences|nom),
column10=bon(Bons|nom),
column12=delai_rendu,
column13=delai_analyse,
column14=recommandations(Recommandations|nom),
column15=commentaire,
column16=indications,
column17=acheminements(Acheminements|nom),

Suivant le choix :

column18=soustraitance(SousTraitance|id),
column19=preanalytique(Preanalytiques|id)



Il faudra voir pour les liaison avec les actes après.
                                avec les fichiers

Les dates ne peuvent être vides

Tout a été vérifié. C'est Ok. Même les tables liées, les enregistrements ne sont pas recréés s'ils sont trouvés.

Mises à jour impossible par cette manière.

Reste à faire : les autres champs.
- Recommandations => Pas ok tout à fait, ne retrouve pas ce qui existe déjà dans la table ?! ==> Si, si, c'est ok.
- Acheminement == OK, remplir la table avant.

Essayer l'export avec Gnumeric ... Export > Format texte paramétrable => Plus pratique

Pour les fichiers joints, il n'est pas possible d'importer directement. Mais vu que c'est un champ Many2Many, je suppose qu'il faille passer par la table intermédiaire.

##### Examens sous-traités #####

Procéder de la même manière en ajoutant à field list :
column18=soustraitance(SousTraitance|id),

ATTENTION : le lien entre SousTraitance et Examens est de type __un vers un__. C'est à dire qu'il n'est pas possible de lier la même sous-traitance à plusieurs examens !


À revoir !!!! Le fichier d'exemple ne fonctionne pas : 'NoneType' object has no attribute 'get_internal_type' !!!!


##### Conditions préanalytiques #####
Même principe que la sous-traitance mais plusieurs examens peuvent avoir les mêmes conditions préanalytiques :
column19=preanalytique(Preanalytiques|id)


### Tubes ==> Table ekz.Tubes

#### Générer les images des bouchons des tubes

Le dossier /doc/importations/tubes contient :
- Un fichier contenant une liste au format CSV des couleurs et leur correspondance au format hexadécimal (fichier couleurs_hexa.csv).
- Le modèle de bouchon au format SVG (fichier modele_de_bouchon_tube.svg)
- Le script de génération des images (fichier :  GenererTubes)

Le script GenererTubes est écrit en Bash. Il utilise GNU sed et inkscape (en mode ligne de commande). Ces deux logiciels doivent donc être installés pour que le script fonctionne correctement. Les paramètres du script sont documentées dans sa sources et accessible au lancement du script sans paramètres.

Si besoin, modifier le fichier listant les couleurs à générer ainsi que le modèle SVG.

Générer les images à l'aide du script (exemples) :

- Générer des bouchons unis de 25 px dans /tmp :
 $ bash GenererTubes x x 25 /tmp

- Générer des bouchons unis de 35 px dans fichiers/images/tubes :
 $ bash GenererTubes x x 25 fichiers/images/tubes

- Générer des bouchons de 25 px avec une collerette jaune dans fichiers/images/tubes :
 $ bash GenererTubes jaune ffff00 25 fichiers/images/tubes

- Générer des bouchons de 25 px avec une collerette rouge dans fichiers/images/tubes :
 $ bash GenererTubes rouge ff0000 25 fichiers/images/tubes

#### Autres modes de receuil

#### Importation CSV

1. Le chemin par défaut (fichiers/images/tubes) peut-être changé à condition de changer le chemin dans le fichier CSV... ce n'est pas conseillé).
2. Utiliser le fichier CSV pour l'importation dans la base de données. Le chemin de l'image est générer grâce à une concaténation de chaînes de caractères.


###### Field list

Liste des champs :
column1=nom,
column2=ordre,
column3=image


### Synonymes

Les examens doivent être importés.

La colonne examen doit correspondre exactement au champ nom de l'examen.

#### Field list :

column1=nom,
column2=examen(Examens|nom)



### Table des groupes de prélèvements => Table ekz.prelevements ###

Liste des champs :
column1=nom,
column2=tube(Tubes|nom),
column3=nombre



### Table de l'utilisation des groupes de prélèvements => Table ekz.UtilisationGroupePrelevement ###

Inutile de préciser la correspondance colonne/champ, la première ligne correspond au nom des champs.


## Ce qu'il faudra faire à la main ##

__ À moins que vous ne fassiez un CSV __ 

- Les bons de demande.

