Importation de données au format CSV
====================================
__Ne chercher pas un import Excel®, c'est inutile.__

Principe d'importation des données
----------------------------------

On utilise les scripts import_*.py présents dans /scripts.

Ces scripts permettent :
- Un import en masse des données ;
- Une mise à jour des données (pour certains).

Une matrice de modèles pour créer des fichiers CSV est donné à titre d'exemple dans le fichier doc/importations/modeles_import.ods. Il s'agit d'un document OpenDocument, vous êtes libres de l'éditer avec le logiciel qui vous conviendra. Chacun des onglets correspond à un script d'importation.

Chacun des scripts peut être éxécuté par la commande :
 $ python manage.py runscript import_*table* [--script-args=]

Avec *table* : la table à importer (parfois plusieurs tables en même temps…)
     --script-args=maj : pour procéder à une mise à jour (pour les scripts qui le permettent)
     --script-args=force : pour forcer l'insertion d'enregistrement si la table n'est pas vide (pas forcément une bonne idée dans certains cas)

Dans tous les cas, il vous sera demandé d'indiquer la localisation du fichier CSV.

Lorsqu'il s'agit d'importer des fichiers (images, documents…), le script créé une copie du fichier au même endroit qu'un ajout manuel via l'interface d'administration l'aurait faite.

Il va sans dire qu'une sauvegarde *intégrale* des données avant l'importation est plus que conseillée !

Utilisation de LibreOffice
--------------------------
Exporter les feuilles après modifications au format CSV :
- Jeu de caractère : UTF-8.
- Séparateur de champ : , (virgule).
- Séparateur de texte : " (guilemets).
- Cocher _mettre entre guilement toutes les cellules de texte_.

Utilisation de Gnumeric
-----------------------
L'export est plus pratique car il ne modifie pas le document de travail contrairement à LibreOffice. Utiliser  Export > Format texte paramétrable...

Importation des données au format CSV
-------------------------------------
L'importation devrait suivre l'ordre suivant.

### Lettres clé ⇒ Table ekz.Lettres → script import_lettres
Le prix doit être dans le format anglo-saxon (séparateur des décimales : le point « . »)

### Codes de facturation → Table ekz.Actes → script import_actes
Utiliser le fichier NABM.csv proposé ou le créer (cf. recommandations ci-dessous dans l'exportation au format CSV).

Le format doit correspondre à l'ordre : code,libelle,nombre,lettre

ATTENTION aux zéros devant les codes actes (4 zéros non significatifs) !

L'importation du fichier BHN dit « du CHU de Montpellier » peut poser problème. Bien tester avant mise en production. En effet, il arrive que le fichier soit un peu mal foutu, il faut le retravailler en triant les colonnes après avoir scinder les colonnes et vérifier qu'il n'y a pas de lettre mal formées ... (B30 au lieu de B ...)

### Table des méthodes d'analyses → Table ekz.Methodes → script import_methodes

### Table des sous-traitants → Table ekz.SousTraitants → script import_sous-traitants

### Table des transporteurs → Table ekz.Transporteurs → script import_transporteurs

### Table des températures → Table ekz.Temperatures → script import_temperatures

### Table des lieux de stockage → Table ekz.Stockages → script import_stockages

### Table de la sous-traitance → Table ekz.SousTraitance → script import_sous-traitances
Ne pas utiliser, la sous-traitance sera importée dans la table correspondante avec les examens.

### Table des centrifugations → Table ekz.Centrifugations → script import_centrifugations
Ne pas utiliser, les centrifugations seront importées dans la table correspondante avec les examens.

### Table des analyseurs → Table ekz.Analyseurs ⇒ script import_analyseurs

### Tables des conditions préanalytiques → Table ekz.Preanalytiques → script import_preanalytiques
Ne pas utiliser, les conditions préanalytiques seront importées dans la table correspondante avec les examens.

### Table des secteurs → Table ekz.Secteurs ⇒ script import_secteurs

### Table des bons → Table ekz.Bons ⇒ script import_bons

### Table des urgences → Table ekz.Urgences ⇒ script import_urgences

### Table des recommandations pour le prélèvement → Table ekz.Recommandations ⇒ script import_recommandations

### Table des examens → Table ekz.examens → script import_examens

Pour les dates, le format correct est AAAA-MM-JJ (format ISO 8601) et peut-être laissé vide (par défaut : date d'activation = maintenant ; date de désactivation = maintenant + 110 000 jours)
Concernant les actes liés, s'il y en plusieurs, les séparer par un point-virgule (;). Attention, certains tableurs ont la fâcheuse tendance à ajouter des espace là où on en voudrait pas… La colonne correspondante, dans le fichier fourni a le format fixé (texte) afin d'éviter qu'un zéro en début de chaîne soit ignoré. Les actes devront bien évidemment exister !

### Tubes → Table ekz.Tubes ⇒ script import_tubes

#### Générer les images des bouchons des tubes

Le dossier /doc/importations/tubes contient :
- Un fichier contenant une liste au format CSV des couleurs et leur correspondance au format hexadécimal (fichier couleurs_hexa.csv).
- Le modèle de bouchon au format SVG (fichier modele_de_bouchon_tube.svg)
- Le script de génération des images (fichier :  GenererTubes)

Le script GenererTubes est écrit en Bash. Il utilise GNU sed et inkscape (en mode ligne de commande). Ces deux logiciels doivent donc être installés pour que le script fonctionne correctement. Les paramètres du script sont documentées dans sa sources et accessible au lancement du script sans paramètres.

Si besoin, modifier le fichier listant les couleurs à générer ainsi que le modèle SVG.

Générer les images à l'aide du script (exemples) :

- Générer des bouchons unis de 25 px dans /tmp :
 $ bash GenererTubes x x 25 /tmp

- Générer des bouchons unis de 35 px dans fichiers/images/tubes :
 $ bash GenererTubes x x 25 fichiers/images/tubes

- Générer des bouchons de 25 px avec une collerette jaune dans fichiers/images/tubes :
 $ bash GenererTubes jaune ffff00 25 fichiers/images/tubes

- Générer des bouchons de 25 px avec une collerette rouge dans fichiers/images/tubes :
 $ bash GenererTubes rouge ff0000 25 fichiers/images/tubes

#### Autres modes de receuil
Les placer à l'endroit où le script d'importation devra aller les chercher

#### Importation CSV
1. Le chemin par défaut (fichiers/images/tubes) peut-être changé à condition de changer le chemin dans le fichier CSV... ce n'est pas conseillé).
2. Utiliser le fichier CSV pour l'importation dans la base de données. Le chemin de l'image est généré grâce à une concaténation de chaînes de caractères.

### Synonymes ⇒ Table ekz.Synonymes → script import_synonymes
Les examens doivent être importés.
La colonne examen doit correspondre exactement au champ nom de l'examen.

### Table des groupes de prélèvements → Table ekz.prelevements ⇒ import_prelevements

### Table de l'utilisation des groupes de prélèvements → Table ekz.UtilisationGroupePrelevement → script import_utilisations

