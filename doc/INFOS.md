PERMISSIONS PERSO
-----------------
Elles sont déclarées dans models.py puis retrouvées dans le template et dans les permissions.
Il faut juste faire ensuite :
   python manage.py update_permissions

Modèle relationnel de données
-----------------------------

Il faut django-extensions et le paquet pygraphviz

python manage.py graph_models ekz pages -g -o mrd.png


À propos de la configuration locale
-----------------------------------

Le fichier ekzamenoj/localsettings.py permet de personnaliser le comportement de l'application.
Les valeurs du dictionnaire CONFIG_LOCALE sont disponibles dans les templates (grâce à ekz/context_processors.py dans les TEMPLATE_CONTEXT_PROCESSORS du settings.py) et dans l'ensemble du code python.


Générer le fichier des données par défaut (peut-être utilisé pour une sauvegarde)
---------------------------------------------------------------------------------

  python manage.py dumpdata --indent=4 -e sessions -e admin -e contenttypes -e auth.Permission  > doc/default_data.json
  python manage.py dumpdata --indent=4 -e sessions -e admin -e contenttypes -e auth.User


Il est mieux de générer des fichiers séparés par applications :

	python manage.py dumpdata --indent=4 ekz > default_data/ekz.json
	python manage.py dumpdata --indent=4 pages > default_data/pages.json
	python manage.py dumpdata --indent=4 presc > default_data/presc.json
	python manage.py dumpdata --indent=4 patients > default_data/patients.json

Il faut les utilisateurs car ils sont liés aux prescriptions :

	python manage.py dumpdata --indent=4 auth.user > default_data/user.json


Générer un script Python pour les données
-----------------------------------------

  python manage.py dumpscript > dump.py

Générer un dump avec PostgreSQL
-------------------------------

    postgres@krypton:~$ pg_dump -h localhost -U ekzamenoj -c -d ekzamenoj > ekzamenoj.sql


Vider la base PostgreSQL
-----------------------

    postgres@krypton:~$ psql
    postgres=# drop database ekzamenoj; CREATE DATABASE ekzamenoj WITH ENCODING 'UTF-8' OWNER "ekzamenoj"; GRANT ALL PRIVILEGES ON DATABASE ekzamenoj TO ekzamenoj;

Générer le requirements.txt
---------------------------

  pip freeze > requirements.txt


Mettre un paquet à jour
-----------------------

  pip install xxxx --upgrade

Installation dans un environement virtuel
-----------------------------------------

Créer l'environnement :

  mkvirtualenv --python=/usr/bin/python3 -a Documents/django/ekzamenoj ekzamenoj

Travailler dedans :

  workon ekzamenoj

Instaler ce qu'il faut :

  pip install -r requirements.txt

Initialiser la base de données :

  python manage.py migrate

Si vous utilisez une base de données PostgreSQL, il faudra probablement initialiser la base de données en deux fois :

  python manage.py migrate auth & python manage.py migrate

Pour importer les données par défaut (facultatif ; il faudra décompresser l'archive des fichiers correspondants) *ATTENTION : l'utilisateur admin a les plein pouvoir, son mot de passe est admin, pensez à le changer !* :

  python manage.py loaddata default_data/*.json

Pour créer un administrateur :

  python manage.py createsuperuser

Il ne reste plus qu'à lancer le serveur :

  python manage.py runserver

Et voilà !

Générer des migrations propres
------------------------------

   $ python manage.py  makemigrations --dry-run -n flow presc


Avec `--dry-run` pour ne rien faire pour l'instant
     `-n flow` le nom du changement
  et `presc` le nom du module

À propos de colorbox
--------------------

La page du projet est ici, on y trouvera la doc http://www.jacklmoore.com/colorbox/
On utilise plus Colorbox


À propos de la prescription
---------------------------
Ce système m'a été inspiré par le méchanisme des boutiques en ligne.
Une partie du code de ce système est fortement inspiré par « django-ecommerce » (sous licence AGPL 3) dont l'auteur est Jean Traullé.
