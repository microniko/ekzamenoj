PERMISSIONS PERSO
-----------------
Elles sont déclarées dans models.py puis retrouvées dans le template et dans les permissions.
Il faut juste faire un syncdb

Modèle relationnel de données
-----------------------------

Il faut django-extensions et le paquet pygraphviz

python manage.py graph_models ekz pages -g -o mrd.png


À propos de la configuration locale
-----------------------------------

Le fichier ekzamenoj/localsettings.py permet de personnaliser le comportement de l'application.
Les valeurs du dictionnaire CONFIG_LOCALE sont disponibles dans les templates (grâce à ekz/context_processors.py dans les TEMPLATE_CONTEXT_PROCESSORS du settings.py) et dans l'ensemble du code python.


Générer le fichier des données par défaut (peut-être utilisé pour une sauvegarde)
---------------------------------------------------------------------------------

  python manage.py dumpdata --indent 1 > doc/default_data.json


Générer le requirements.txt
---------------------------

  pip freeze > requirements.txt


Installation dans un environement virtuel 
-----------------------------------------

Créer l'environnement :

  mkvirtualenv -a Documents/django/ekzamenoj ekzamenoj

Travailler dedans :

  workon ekzamenoj

Instaler ce qu'il faut :

  pip install -r requirements.txt


__Il manque les infos sur la base de données__

Il ne reste plus qu'à lancer le serveur :

  python manage.py runserver

Et voilà !


À propos de colorbox
--------------------

La page du projet est ici, on y trouvera la doc http://www.jacklmoore.com/colorbox/

Il y a 5 mises en forme de la colorbox. Il suffit de modifier le lien symbolique dans static/colorbox :
  - Pour la feuille de styles : colorbox.css -> example5/colorbox.css
  - Pour les images           : images -> example1/images

