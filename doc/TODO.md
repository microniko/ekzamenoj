TODO LIST
========

PENSER À GIT !!!!!!

- Écrire des tests unitaires.
- Renommer ekz en examens ? et d'autres : pages en infos
- Refaire les environnements virtuels : le copier ne convient pas (ne change pas le chemin de l'envvirt... !)
- Écrire des tests unitaires.
- Doc dans l'admin (admin/doc) il faut installer docutils de Python. Ok pour ça mais il n'aime pas les accents dans les descriptions des modèles. Ok pour les accents mais il faudrait centraliser plus ou moins la doc…
- Dans l'OpenSearch remplacer le vrai nom par https://docs.djangoproject.com/en/1.7/ref/request-response/#django.http.HttpRequest.META (je crois que dans FJ on l'a déjà fait ! Je n'ai pas reussit à transmettre request.META[…] au template à partir de {'adresse', …} dans l'url.py ! J'ai trouvé dans  https://docs.djangoproject.com/fr/1.7/topics/auth/default/ {{ domain }}, ça serait trop simple pour marcher ! Effectivement, {{ domain }} ne fonctionne pas…
- Simplifier Alphabet() (dans ekz/views.py) et utiliser is_actif si possible. Visiblement pas possible, on ne peut pas requêter contre une méthode de modèle. Fait en partie pour les synos.
- Créer une fonction pour enlever les accents → ça marche mais du coup, la liste_alpha_examens ne trouve pas les examens avec accents. Il faudrait presque que dans le lien il nous mette par exemple examens/commencent_par_EouÉ/
- Passer le validateur HTML5
- Intégrer le type mime pour mettre une belle image aux docs joints.
- Tester avec Sentry (http://sametmax.com/traquez-les-erreurs-de-vos-apps-django-avec-sentry/) pas très libre mais on ne sait jamais.
- Améliorer la traçabilité (voir dans django.contrib.admin → LogEntry).
- Un examen = plusieurs analyses élémentaires…
- Tester une mise à jour de nommenclature.
- Lire la doc sur la migration…
- Dans les liens pour éditer quelque chose, si on ajoute « ?_popup=1 » à l'URL, ça permet de n'afficher que le formulaire d'admin. On aurait pu utiliser ça pour mettre dans une colorbox. Mais oon obtient une page blanche… Erreur 405 !
- Favicon ?
- Ajouter un champ slug aux pages.
- Pour PostgreSQL problème dans les données par défaut → alter table ekz_actes alter column libelle type varying; (taille par défaut : 150…)
                                                       → Le smalint n'est pas assez grand pour les delai_*
- Penser aux mise à jour de JQuery et Boostrap (pour SummerNote)
- Normalement, c'est bon pour les migrations… Il faudra refaire les données par défaut (il y avait un pb de droit/groupes…).
- Mauvais calcul des tubes depuis le passage à Django 1.9 ! Rustine mise en place sur la page de la prescription et  la génération d'étiquettes. Voir si on ne peut pas obtimiser…
- Ça n'a pas encore été avec la migration des données… Grrr. Il faut repartir
  tout à zéro. À mon avis, il faudrait repartir de zéro avec des données
initiales propres. Note : pour générer les migrations, il faut forcer le nom de
l'application à makemigrations. On a toujours le soucis de smallint :
django.db.utils.DataError: Problem installing fixture
'/var/www/microniko.net/demo-ekzamenoj1.0/default_data-20160404.json': Could not
load ekz.Examens(pk=14): ERREUR:  smallint en dehors des limites
De même pour varying:
django.db.utils.DataError: Problem installing fixture
'/var/www/microniko.net/demo-ekzamenoj1.0/default_data-20160404.json': Could not
load ekz.Actes(pk=40): ERREUR:  valeur trop longue pour le type character
varying(150)
 a résolu le soucis : alter table ekz_actes alter column libelle type varchar;
 Mais il faudra regarder pourquoi Django merdouille à créer les tables dans
PgSQL !
SQLite est visiblement moins regardant sur la longueur des champs. Le champ description de la RHN peut compter jusque 540 caractères… On augmente la taille dans la model.py de Actes !
Problème avec delai_preana de ekz
Les données par défaut posent problème : lignes 139 140 141 8 9 et 24 (auth.group des id qui auraient été supprimés ?)

Le bootstrap de l'admin n'est pas hébergé localement !

Ce qui est en cours
===================


Historique des trucs faits
==========================
- Créer un droit « voir inactif » et une classe grisée afin de montrer (en gris) ou pas les examens inactifs. ==> OK.
- Séparer News & Pages d'Ekz ? ==> OUI, OK
- Convertir les fonctions ListeDesPages() & Alphabet() en classes. Ça permettra des les importer dans les views => Fait autrement : OK
- Convertir les champs demandé en urgence et urgences en ForeignKey => OK
- Sous-traitance lié à un seul examen. ==> OK
- Régler cette histoire de constante/variable et le prix ... ==> AYÉ :)
- Revoir l'histoire de la variable de prix car c'est dans localsettings et ekz/context_processors.py :) ==> OK
- Pourquoi le commentaire n'apparaît pas ?  ==> Ben, si ?
- Faire une boucle dans ekz/context_processors.py pour ne pas avoir à déclarer tout le temps les localsettings ! ==> OK
- Actes : il faudrait que les examens soient liés au code nomenclature (pour éviter les pb lors des mises à jour) et probablement, il faudrait plusieurs actes possibles. ==> OK
- Revoir l'interface d'admin :
  - Faire des fieldsets pour regrouper des éléments: ModelAdmin.fieldsets ==> OK
- Voir le through, il devrait être possible de mettre direct dans la saisie (exemple de Actes à travers Facturation dans Examens) => ModelAdmin.formfield_for_manytomany ? Mouais... Pas si sûr... C'est possible !  ==> OK :D
- Générer un requirements.txt avec pip freeze > requirements.txt ==> OK
- Mettre la connexion dans une colorbox ==> Non satisfaisant, en cas d'erreur de login, ne reste pas dans la colorbox.
- Mettre la recherche aussi => Non satifaisant.
- Connexion & Recherche : dans un élément AJAX. ==> >C'est pas très pratique, ça casse le django. Du coup, seulement le login dans une animation CSS3 pure...
- En admin, quand on ajoute un stockage, il ne peut y avoir de caractères accentués dans le champ « pièce » ?!  ==> OK
- Pareil dans le préana : champ aliquotage  ==> OK
- Mettre CSVimport à jour et le tester => Ok 
- Revoir l'organisation des applications interne. Ça serait bien de séparer page pour une éventuelle réutilisation mais ça va nous poser un problème à cause du menu....    → OK
- Trouver pourquoi l'on ne peut pas importer deux fonctions ≠ de deux applis ≠ croisées :   → OK
        from pages.views import ListeDesPages   dans    ekz/views.py                          (problème d'ordre de déclaration des fonctions)
        from ekz.views import Alphabet          dans    pages/views.py
- J'ai modifié la fonction qui liste l'alphabet avec la gestion des examens désactivés. Normalement, les synonymes doivent aussi gérer leurs examens actifs ou non. C'est à vérifier. ==> Problème avec le reques. À revoir à l'occasion... → Vérifié, ok (mais je ne sais pas ce qu'est reques…)
- Tous les docs ne s'affichent pas dans le template d'affichage d'un examen. → Ben, si … ?
- Le commentaire n'apparaît pas. → OK
- Revoir auto_now     ==> Dernière modif   \_ Désuet ? Il paraît (irc) mais pour l'utilisation ici, ça va. À condition de remplacer auto_now_add par auto_now => OK
         auto_now_add ==> Création         /
- Trouver le moyen de générer du XML pour faire un fichier opensearch.xml automatique. → Trouvé : http://www.elfsternberg.com/2010/07/15/adding-opensearch-django/ → OK
- On devrait maintenant pouvoir améliorer l'affichage des tubes (prélèvements) et ne pas avoir une sale succession de if. Une bonne fonction dans un des modèle devrait être bien plus élégante. → Tests en cours ; penser à comparer le nombre de requêtes. Fonction ImgTubes dans le modèle Prelevements On a un soucis avec les noms des images avec des accents ! Pour les accents, c'est réglé. Voir si on peut améliorer la chose car on peut afficher un grand nombre de tube ! Nous devrons compter le nombre de rqt. Je pense que l'on pourrait utiliser la vue générique DetailView pour Examens en mettant prelevement et synonymes dans les définissant dans le modèle approprié (Examens). Ça semblait fonctionner (¿) mais ça ne fonctionne plus... Il faudrait y arriver (c'est bon).  Faudra compter le nombre de rqt aussi (résultat : ça change pas grand chose !).   Vue générique utilisée fonctionnelle avec méthodes internes pour simplifier le code (https://docs.djangoproject.com/fr/1.7/topics/db/queries/#backwards-related-objects). La vue générique fonctionne. Il faudra améliorer la gestion des permission : dans le gabarit c'est moche.
- Définir une fonction dans models/Examen pour savoir si un examen est désactivé. Du coup, on devrait pouvoir définir si un doit afficher un examen en fonction de la permission et ceci dans le modèle directement. →  Fait ↑ 
- Réorganiser les applications : mettre les templates dans un dossier templates par applications → Presque fini. Revoir avec le bouquin. → Les gabarits sont classés par appli et c'est plus joli comme ça!
- Faire hériter les ListView d'une vue parente avec get_context_datapour simplifier l'écriture. → Ok.
- Faire une classe dans pages qui listerait les pages dans le menu et faire hériter les classes. Faire une classe globale « liste des pages » + « alphabet » pour faire hériter les classes dans ekz/views → OK ↑
- Mettre un lien d'édition dans le site public vers l'admin si on a le droit. → OK
- Le logout d'un utilisateur n'ayant pas le statut équipe ne peut pas se déloguer ! → OK
- Il faudra faire en sorte que les URL coupées par l'utilisateur (genre /examens/voir/ ou /examens/) puisse retourner quelque chose. →
- Ça serait pratique de pouvoir faire une recherche d'un acte dans l'administration...
- Faire un include pour ne pas répéter le formulaire de login. → OK
- Changer d'éditeur de texte enrichi car redactor n'est pas libre. Éditeurs Wysiwyg libre : TinyMCE (LGPL), CKEditor (GPL, LGPL, MPL), Summernote (MIT), Aloha Editor (GPLv2). On a choisi Summernote → OK
- Voir du côté de Django-secure https://pypi.python.org/pypi/django-secure (une partie des fonctionnalités sera intégrées dans Django 1.8).
- Je pense qu'il y a des [doub|trip]lons avec le système ekzamenoj/localsettings.py et ekz/contants.py et ekz/settings.py" La config est récupérée par TEMPLATE_CONTEXT_PROCESSORS : ekz.context_processors.configuration → OK
- L'image des recommandations pour le prélèvement ne s'importe pas ! → ok
- Voir image des bons → ok
- Détails des sous-traitants et transporteurs → ok
- Bouton d'édition sur les pages et les news → OK
- Mettre des champs sur la même ligne (exemple : fields = ((’url’, ’title’), ’content’)) → OK
- Bootstrap et Summernote à mettre en interne.
- Mettre des dates/heures par défaut dans les pages et news.
- Cosmétique du moteur de recherche
- La page /examens/ est bizarre…
- Les synonymes en inline dans les examens
- Il manque la température pour la centrifugation…
- Import CSV maison
- Revoir le __unicode__(self) de Preanalytiques !
- Ajouter liens pour éditer les synonymes ?
- Lien vers détails de modifs examens
- Amélioration de l'admin. Trouver pourquoi « view_on_site  » ne souhaite pas fonctionner. Trouver un moyen pour utilisr AdminSite.site_title
  Il fallait simplement définir get_absolute_url
- Cosmétique des colorbox (sous-traitance, secteur d'ex.)
- Mettre des choses par défaut dans la base de données. Possible de façon automatique ?
                                                       Avec [une|des] commandes ?
                                                           :    python manage.py syncdb --noinput
                                                           puis python manage.py loaddata doc/importations/donnees.json   <= Fichier généré
- Le groupe d'examen par prélèvement faisant parti intégrante de la prescription, ne faudrait il pas le séparer pour le mettre dans presc ? NON !!!! Sinon, le système d'ajout d'examen par prélèvement s'effondre.
- Comprendre pourquoi dans la vue VoirExamen() examen.is_actif retourne ce …⋅?!%—$ de <bound method Examens.is_actif....> !!! Si on résolvait ça, on pourrait éviter de gérér la permission view_inactif dans le gabarit ! ⇒ Résolu, il fallait passé par une property !!!! → c'est bon pour la permission du coup :)
- Voir les histoires de suppressions dans les objets liés (on_delete=models.PROTECT) « the on_delete argument of ForeignKey and OneToOneField will be required in Django 2.0. » → ok
- Revoir cette histoire de Jquery. On dirait qu'elle est plusieurs fois (cf settings.py) et en plus on a un module django-jquery... => ??
- Quand la version de Django 1.9 sera utilisable en production, revoir la classe VoirExamen (ekz/views.py) pour laquelle on avait des soucis pour gérer une permission. https://docs.djangoproject.com/en/dev/topics/auth/default/#django.contrib.auth.mixins.AccessMixin ou https://docs.djangoproject.com/en/dev/topics/auth/default/#django.contrib.auth.mixins.LoginRequiredMixin. Ça permet de requéter sur une méthode de modèles ?!
- Nettoyage de code pour la permission view_inactif dans le gabarit voir_examen (et ailleurs ?) depuis que is_actif est fonctionnel. Nettoyer aussi ekz.views.VoirExamen
- Presc : bilans
- Revoir les permissions/toussa pour patients/presc à revoir, mis dans les url…
- On peut ajouter deux fois le même bilan à une prescription…
- Personnaliser les erreurs 403 et 404.
- Saisir aussi l'heure lors de la validation d'une prescription
- Le résultat de la recherche présente les examens inactifs mais le ne précise pas…
- Délai ajout d'analyses : avant/après prétraitement…
- Liste des patients
- Supprimer un patient de la prescription en cours.
- Valider une prescription avec le patient.
- Cosmétique sur détail examen
- Cosmétique de la liste de tous les examens
- Ordonner les news
- Trouver comment surcharger le debug de TEMPLATES pour le mettre à True
- Ce n'est pas bien dans le template de mettre le chemin complet vers les images
  dans \includegraphics ! ⇒ OK
- Commentaire dans la prescription
- Il y a un lien foireux (voir en bas, la date) sur la page patient/
- Voir ceci : http://www.trucsweb.com/tutoriels/css/css-modal/#fermer
- Le système se vautre lamentablement si l'on prescrit un examen qui n'est pas
  attaché à une utilisation de groupe de prélèvement
