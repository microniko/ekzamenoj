/*
 * 
 * Copyright (C) 2015  Nicolas Grandjean <nicolas@microniko.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


function details(bouton, id) {
  var div = document.getElementById(id);
  if(div.style.display=="none") {
    div.style.display = "block";
    bouton.innerHTML = "Masquer";
  } else { // S'il est visible...
    div.style.display = "none";
    bouton.innerHTML = "Voir";
  }
}
/* Permettre aux navigateurs nazes d'afficher « place holder »
 * A jQuery based placeholder polyfill
 * Source : http://www.jacklmoore.com/notes/form-placeholder-text/
 *                                                                  */


$(document).ready(function(){
  function add() {
    if($(this).val() === ''){
      $(this).val($(this).attr('placeholder')).addClass('placeholder');
    }
  }

  function remove() {
    if($(this).val() === $(this).attr('placeholder')){
      $(this).val('').removeClass('placeholder');
    }
  }

  // Create a dummy element for feature detection
  if (!('placeholder' in $('<input>')[0])) {

    // Select the elements that have a placeholder attribute
    $('input[placeholder], textarea[placeholder]').blur(add).focus(remove).each(add);

    // Remove the placeholder text before the form is submitted
    $('form').submit(function(){
      $(this).find('input[placeholder], textarea[placeholder]').each(remove);
    });
  }
});
