> Copyright (C) 2015  Nicolas Grandjean <nicolas@microniko.net>
> 
> This program is free software: you can redistribute it and/or modify
> it under the terms of the GNU General Public License as published by
> the Free Software Foundation, either version 3 of the License, or
> (at your option) any later version.
> 
> This program is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
> GNU General Public License for more details.
> 
> You should have received a copy of the GNU General Public License
> along with this program.  If not, see <http://www.gnu.org/licenses/>.

Les images sont originaires du thème pour Gnome : RAVE-X Colors Icon Theme 

« Designed and Developed by: The RAVEfinity "Open Source Design Team" in California USA.
(With contributions world wide)  http://www.ravefinity.com/ »
Credits :

Faenza Icons.
(C) Tiheum
http://tiheum.deviantart.com/art/Faenza-Icons-173323228

Elementary Icons, Folders & Theme.
Daniel Foré <Daniel.p.ForeATgmailDoTcom> , http://elementaryos.org/

(C) Johnathan Linux Mint Theme Devloper & Clem.
www.linuxmint.com

(C) 2014 New Colors And Theme Built By RAVEfinity, Jared sot <ravefinity@gmail.com>

RAXE-X-Full-Dark-Brown/actions/24
